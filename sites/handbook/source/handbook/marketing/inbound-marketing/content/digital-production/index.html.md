---
layout: handbook-page-toc
title: "Digital Production"
description: GitLab Digital Production
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page
{:.no_toc}

- TOC
{:toc}

## General SLAs for video production:

- All video requests must be made using the video-request issue template.
- Requests must be made no less than ten weeks in advance for event videography.
- Requests must be made no less than eight weeks in advance for a customer or user story.
- Requests for on-site livestreams that are to be produced by GitLab's digital production team must be made at least 12 weeks in advance.
- Requests for animated videos must be made no less than eight weeks in advance of expected delivery.
- You must have approval of all customers, users and other external partners that are to be videographed prior to making a production request.
- You must have approval from the event venue that videography and photography is permitted on the premises.
- You must have any and all required documentation, e.g. location agreement, liability agreement, photography/videography agreement, from the venue approved and signed at the time of request.
- If production or additional liability insurance is required by the venue, please acquire that policy. If you have questions on how to acquire production insurance, please contact the digital production team *and* make a request for assistance in the video-request issue you created for this event.
- You must provide all suggested interview questions two weeks prior to the shoot date.
- Contribute & Commit - No video requests will be accepted on site.
- Contribute & Commit - Please open an interview request using the video-request issue template. You must use the production schedule to request an interview timeslot. Please be flexible because some intvs can run long and impact the day's production schedule. If you are late or fail to appear for your scheduled interview time, you may lose the chance to have your requested interview filmed.  


## SLAs and procedures for videography at GitLab Connect events:

- Please open a request using the video-request issue template **as soon as your date and venue have been locked in.** You **do not** have to wait until a speaker has agreed to be recorded; this is the only general digital production SLA that does not apply for Connect events. 
- As you work through the contracting process for the venue rental, please let them know you plan to have a small production crew come in to record portions of the event. Please get all required documentation, e.g. location agreement, liability agreement, photography/videography agreement, signed. 
- Please find out if production or additional liability insurance is required by the venue. If so, please acquire that policy. If you have questions on acquiring production insurance, please contact the digital production team *and* make a request for assistance in the video-request issue you created for this event.
- All video production costs for Connect events comes from the Corporate Marketing budget.

## Related links and handbooks

1. **[GitLab Equipment List](https://about.gitlab.com/handbook/marketing/inbound-marketing/content/digital-production/digital-production-equipment-list/)**
     1. [GitLab Equipment Setup and Operation](https://about.gitlab.com/handbook/marketing/inbound-marketing/content/digital-production/digital-production-equipment-list/black-magic-pocket-cinema-camera/)
1. [GitLab.tv Handbook](/handbook/marketing/inbound-marketing/content/digital-production/gitlab-tv/)

----

Return to the main [Inbound Marketing Handbook](/handbook/marketing/inbound-marketing/).
