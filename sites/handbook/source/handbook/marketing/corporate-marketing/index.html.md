---
layout: handbook-page-toc
title: Corporate Marketing at GitLab
description: 'For strategies and administration of corporate marketing at GitLab'
twitter_image:
twitter_image_alt: GitLab's Corporate Marketing Team Handbook
twitter_site: 'gitlab'
twitter_creator: 'gitlab'

---
## Welcome to the Corporate Marketing Handbook
{:.no_toc}

The Corporate Marketing team includes Corporate Events, Corporate Communications (PR, Executive Communications, and Social Media), All-Remote Marketing, and the Community Team. Corporate Marketing is ultimately responsible for creating awareness and driving top of funnel interest in GitLab. We do this by driving conversations in the communications channels that accelerate our objectives, and by developing an integrated communication strategy that is executed globally.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How We Work

### Project Management

There is an initiative underway to simplify Project Management across all of Marketing.

In the meantime, we have implemented a few practices to help us get organized:

- Add weekly milestones to issues based on the week you expect to work on the issue (format is `Fri: Apr 17, 2020`)
- Move issues forward into future milestones, or place them in the `backlog` milestone if they're no longer planned
- We close out the week's milestone each week, and issues will be moved forward if they are not closed out
- You can use labels however you like, they are a tool for you and your team to filter/sort issues

## Brand personality

GitLab's brand has a personality that is reflected in everything we do. It doesn't matter if we are hosting a fancy dinner with fortune 500 CIOs, at a hackathon, or telling our story on about.gitlab.com...across all our communication methods, and all our audiences, GitLab has a personality that shows up in how we communicate.

Our personality is built around four main characteristics.

1. Human: We write like we talk. We avoid buzzwords and jargon, and instead communicate simply, clearly, and sincerely. We treat people with kindness.
1. Competent: We are highly accomplished, and we communicate with conviction. We are efficient at everything we do.
1. Quirky: We embrace diversity of opinion. We embrace new ideas based on their merit, even if they defy commonly held norms.
1. Humble: We care about helping those around us achieve great things more than we care about our personal accomplishments.

These four characteristics work together to form a personality that is authentic to GitLab team-members, community, and relatable to our audience. If we were `quirky` without being `human` we could come across as eccentric. If we were `competent` without being `humble` we could come across as arrogant.

GitLab has a [higher purpose](/company/strategy/#mission). We want to inspire a sense of adventure in those around us so that they join us in contributing to making that mission a reality.

## Tone of voice (TOV)

The following guide outlines the set of standards used for all written company
communications to ensure consistency in voice, style, and personality, across all
of GitLab's public communications.

The tone of voice we use when speaking as GitLab should always be informed by
our [mission & vision](https://about.gitlab.com/company/strategy/#mission). We use [inclusive, gender-neutral language](https://litreactor.com/columns/5-easy-ways-to-make-your-writing-gender-neutral).
Most importantly, we see our audience as co-conspirators, working together to
define and create the next generation of software development practices. The below
presentation should help to clarify further:

<figure class="video_container"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQnRSaLsmAyGxcDHj3a_uh5UT2h45WUKqF0UGwtedOVGUK0iCcC134czSZ_Hv7cOvLF9BpKM0_frZMS/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></figure>

Watch a walkthrough of the TOV project and guidelines - [GitLab's Tone-of-voice: Seat at the Table](https://youtu.be/j-viaR6qhXM) 

See [the Blog Editorial Style Guide](/handbook/marketing/inbound-marketing/content/editorial-team/) for more.

### About

#### GitLab the community

GitLab is an [open source project](https://gitlab.com/gitlab-org/gitlab-ce/)
with a large community of contributors. Over 3,000 people worldwide have
contributed to GitLab's source code.

#### GitLab the company

GitLab Inc. is a company based on the GitLab open source project. GitLab Inc. is
an active participant in our community (see our [stewardship of GitLab CE](/company/stewardship/)
for more information), as well as offering GitLab, a product (see below).

#### GitLab the product

GitLab is a complete DevOps platform, delivered as a single application. See the
[product elevator pitch](/handbook/marketing/strategic-marketing/messaging/)
for additional messaging. 

## Updating the press page

### Adding a new press release

1. Create a new merge request and branch in www-gitlab-com.
1. On your branch, navigate to `source` then `press` and click on the [`releases` folder](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/press/releases).
1. Add a new file using the following format `YYYY-MM-DD-title-of-press-release.html.md`.
1. Add the following to the beginning of your document:

```
---
layout: markdown_page
title: "Title of press release"
description: "short sentence overview of press release'
twitter_image: "/location/of/image.png"
twitter_creator: "@gitlab"
twitter_site: "@gitlab"
twitter_image_alt: "Celebrating GitLab's press release about xx with fun emojis"
---
```

`description`, `twitter_image`, and `twitter_image_alt` would be the three, net-new tags needed to be custom to every single release. `twitter_creator` and `twitter_site` would be static with the value "@gitlab".

1. Add the content of the press release to the file and save. Make sure to include any links. It is important to not have any extra spaces after sentences that end a paragraph or your pipeline will break. You must also not have extra empty lines at the end of your doc. So make sure to check that when copying and pasting a press release from a google doc.
1. Assign @wspillane to the MR to add the social sharing image to the `twitter_image:` section above and to merge the release. If the team has previously discussed the work, the social team may have already created a sharing image. If not, they will need to create it from this mention. The social team will add the data necessary to the MR for social sharing and merge the press release.

### Updating the `/press/#press-releases` page

When you have added a press release, be sure to update the index page too so that it is linked to from [/press/#press-releases](/press/#press-releases).

1. On the same branch, navigate to `data` then to the [`press_releases.yml` file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/press_releases.yml).
1. Scroll down to `press_releases:`, then scroll to the most recent dated press release.
1. Underneath, add another entry for your new press release using the same format as the others, ensuring that your alignment is correct and that dashes and words begin in the same columns.
1. The URL for your press release will follow the format of your filename for it: `/press/releases/YYYY-MM-DD-title-of-press-release.html`.

### Updating the recent news section

1. Every Friday the PR agency will send a digest of top articles.
1. Product marketing will update the `Recent News` section with the most recent listed at the top. Display 10 articles at a time. To avoid formatting mistakes, copy and paste a previous entry on the page, and edit with the details of the new coverage. You may need to search online for a thumbnail to upload to `images/press`, if coverage from that publication is not already listed on the page. If you upload a new image, make sure to change the path listed next to `image_tag`.

- - -

## Design

Read more about our Brand Guidelines in the [Brand Activation](/handbook/marketing/corporate-marketing/brand-activation/).

- - -

## Speakers

For resources for GitLab team members who are planning on attending events or speaking at conferences, see [Speaker Resources](/handbook/marketing/corporate-marketing/speaking-resources/).  To join the [GitLab Speakers Bureau](/speakers/), see the [Developer Evangelist page on the Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/).

- - -

## Corporate Events

### Mission Statement

- The mission of the Corporate Events Team is to:
    - Showcase the value and strengths of GitLab on all fronts
    - Deliver creative solutions to problems
    - Provide exceptional service
    - Build lasting and trusting vendor and internal relationships
    - Treat everyone like they are our most valued customer, including fellow GitLab team-members

### What does the corporate Events team handle?

- **Sponsored events** (events with global audience of 5000+ attendees for NA, 3000+ for other territories (50% or more of audience is national or global). There are some exceptions. There are handful smaller events that we handle due to the nature of the audience, product specific, and the awareness and thought leadership positions we are trying to build out as a company). The primary goal is always driving brand awareness but that cannot be the only result.
- **Owned events**
    - [GitLab Commit](/events/commit/), our User Conference
    - GitLab Culture Open House Events- by invite only
- **Internal events** (Contribute-sized events)
    - [GitLab Contribute](/events/gitlab-contribute/), our internal company and core community event
    - We also serve as DRI for all internal Sales events- [SKO's](/events/sko21/), Force Management planning, Rewards Travel, SQS, QBR's. Must be above 25 people attending for corp events involvement.
    Please review our events decision tree to ensure Corporate Marketing is the appropriate owner for an event. If it is not clear who should own an event based on the [decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=sharing), please email events@gitlab.com.
- **Speaker management and session production** for internal and external GitLab speakers. This includes management of the GitLab-owned call for proposal (CFP) process, agenda-building sessions, coordinating speaker communications, and establishing work-back and event-day run of show schedules.

### Corporate Events Strategy / Goals

- **Brand**
    - For Sponsored Events: Get the GitLab brand in front of 15% of the event audience. 40,000 person event we would hope to get 4,000+ leads (10%) and 5% general awareness and visibility with additional branding and activities surrounding participation.
    - Human touches- Tracked by leads collected, social interactions, number of opportunities created, referrals, current customers met, and quality time spent on each interaction.
    - Audience Minimum Requirements- volume, relevance (our buyer persona, thought leaders, contributors), reach (thought leaders?), and duration of user/ buyer journey considered.
- **ROI**
    - Work closely with demand gen campaigns and field marketing to ensure events are driving results and touching the right audience.
    - Exceed minimum threshold of ROI for any events that also have a demand gen or field component- 5 to 1 pipe to spend within a 1-year horizon.
    - Aim to keep the cost per lead for a live event around $100.
    - [ROI Calculator](https://docs.google.com/spreadsheets/d/1SAYGXysUHGXPKrTDFf9yRcQrh9TYNxR9_Ts6H9dq8JY/edit?usp=sharing) we aim to make 5x ROI on pipeline focused events but this can be used to estimate what return we might get on an event.
- **Thought Leadership and Education**

<details markdown="1">

<summary>Meet the Corporate Events team</summary>

## Meet the Corporate Events team

[**Emily Kyle**](https://about.gitlab.com/company/team/#emily)

* Title: Manager, Corporate Events and Brand
* GitLab handle: @emily
* Slack handle: @emily

[**Lauren Conway**](https://about.gitlab.com/company/team/#Lconway)

* Title: Senior Corporate Events Manager
* GitLab handle: @lconway
* Slack handle: @Lauren Conway

[**Emily Chin**](https://about.gitlab.com/company/team/#echin)

* Title: Corporate Events Production Manager
* GitLab handle: @echin
* Slack handle: @Emily Chin

[**Teagan Shurtleff**](https://about.gitlab.com/company/team/#tshurtleff)

* Title: Corporate Events Manager
* GitLab handle: @tshurtleff
* Slack handle: @Teagan Shurtleff

</details>

### GitLab Commit User Conferences

- [Link to 2020 Slack Channel](https://gitlab.slack.com/archives/CK8HV2A10/)
- Commit Virtual 2020- [Planning Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/915)
- Goal Call for proposals (CFP) and agenda-setting process:
    - 9+ months pre-event: Open an internal call for track issue (GitLab team members)
    - 7+ months pre-event: Select Commit track(s)
    - 6+ months pre-event: Open a public call for proposals (CFP) form and invite contributions
        - Track manager trainings and establishment of deadlines and workflows
    - 5+ months pre-event: Convene the CFP selection committee
    - 4+ months pre-event: Early session acceptances begin
    - 3+ months pre-event: CFP closes; Next round of acceptances sent
    - 2+ months pre-event: Final acceptances sent; sponsorships close
        - Any submissions not selected are thanked and – should the proposal be a potential fit for a future event – invited to stay in touch for participation at a later date
        - Draft agenda is reviewed by stakeholders for strength and diversity of content and speakers
    - 1+ month pre-event: Agenda is final and publishable
- GitLab's event experience is unique for speakers in the hands-on approach that the Corporate Events team takes to ensure that speakers feel supported and prepared. A typical Commit speaker experience will include:
    - Access to a speaker page with all relevant deadlines, resources, and contact information
    - One-on-one access to a presentation support team, including the session track manager and production manager
    - Dedicated working document or email thread between the speaker, track manager, and production manager to work through final session title, description, and slide content
    - 2-3 Zoom meetings to discuss session flow and practice for timing, pace, and polish
    - Weekly communication and updates
    - Optional training and slide creation support
- Commit Virtual for GitLab team members:
    - We need support the day of the event with such tasks as chat moderator, booth staffing, social monitoring support...
- Onsite at Commit for GitLab team members:
    - You will be assigned one or multiple onsite tasks. It is critical you show up for your set duty and communicate any changes in your plans. Clean your schedule on the day of the event, as it will be a full day commitment.
        - Tasks include:
            - Track Scanning- it is essential you show up and stay for this if you are assigned. Our partners have paid to get leads form their talks and it is our promise to provide said leads. All talk attendees must be scanned for this purpose.
            - Check in Support
            - Swag table
            - Questions/ help desk
            - Booth Duty (Hiring, UX, Support, Security, Demo..) - do not leave the booth unstaffed. We have back up. Ask for help on coverage if you need it.
    - Dress code: casual to business casual. Wear what you feel comfortable in. No open toed shoes for safety reasons.
    - Team Travel
        - Team members may come in the day before the event and stay the night of the event. No additional days will be covered unless you have arranged a special circumstance with the Commit planning team.
        - We can only provide Visa support for speakers and external attendees for this event series.
        - If you live within 60 miles of the event you will be asked to commute to the event unless you have a specific arrangement with the Commit planning team.

### RFP Process

- See finance handbook for when you need to go through RFP process
- Use RFP Templates for uniform evaluation of vendors
    - [Questionnaire](https://docs.google.com/spreadsheets/d/154wEnFfBBOq0l_dq23LfGBWI1JHPwL1U_ERDZNjF67k/edit?usp=sharing) to send to vendors bidding
    - [DMC RFP Template](https://docs.google.com/document/d/1dGEffBse3FMnsM3YSVDUs848AU7Rn4DJRfn2l_nTcTQ/edit?usp=sharing)
    - Questions on using the templates ask them in the corporate-events-team Slack channel.

### Event Execution

For event execution instructions, please see the [Marketing Events page](/handbook/marketing/events/#event-execution) for detail instruction and the criteria used to determine what type of events are supported.

### Best Practices on site at a GitLab event

- [Employee Booth Guidelines](/handbook/marketing/events/#employee-booth-guidelines)
- [Scanning Best Practices](/handbook/marketing/events/#scanning-best-practices)

## Accessibility at GitLab Hosted Events

### In advance of the event, we promise that we will take into account the following:

- Accessibility will not be an afterthought.
- Events will be inclusive and accessible.
- Venues will meet International Accessibility Standards guidelines.
- There will be seating available and accessible seating made available upon request
- There will be gender neutral pronouns in event communication.
- General dress guidance will not include male/female binary descriptors, and attendees can decide for themselves what works best for them.
- Feedback will be collected during and after the event to gauge accessibility and comfort levels.
- Designated confidential resources will be available for team members.
- Name badges will have write-in areas, printed or stickers for preferred pronouns.
- Large group meals will have ingredients and allergens listed. If you have communicated your needs with the event staff and the kitchen cannot provide a suitable meal for you, there will be a designated amount you can expense per meal on site.
- The Events team will commit to the idea that no detail is too small
- For all GitLab hosted events of 300+ we hire security that is also certified in first aid.

### Communicating Accessibility

- Gathering attendee needs during registration.
    - Dietary restrictions: If you have any questions, please follow up with the individual privately. Determine the specific restrictions and provide information on how ingredients will be provided. Offer solutions in case there is an issue onsite so that the attendee is prepared.
    - Additional needs: Include an open text area in which attendees can list specific needs to help ensure full guest participation. Example requests include interpreting services, assistive listening devices, accessible parking, accessible hotel rooms, captioning, Reserved front row seat, wheelchair access to working tables throughout the room, Lactation room, or seating for in-person events. The team will make all good faith efforts to carry out requests, or will open lines of discussion for options when not available.
    - Preferred pronouns: Aim to avoid a closed field for gender and instead provide a blank write-in field or inclusive dropdown options so attendees can select/type their preference pending on the badging system. This information will be displayed on name badges, or available to add with stickers etc.
    Email and landing page communication
- Put accessibility information in the event page footer or have a page dedicated to accessibility.
- Send out email on accessibility specifics and resources in advance of the event. Examples of what to communicate in that email:
    - Use of flash photography
    - Any sort of strobe lights or flashing images that may cause seizures
    - Distinctly amplified sounds/music
    - The use of fog machines/any other chemicals or smells that may make your space inaccessible to individuals with Multiple Chemical Sensitivity (MCS) or Idiopathic Environmental Intolerances (IEI)
    - Whether interpreting services will be provided for various speakers, panels, talks, etc.
    - Whether assistive listening devices will be provided.
    - All optional parts of your event, including off-site social activities, that may not be fully accessible.
    - Information about meals and dietary restrictions.
    - Accessible transportation options
    - First Aid/Medical Assistance options
    - Info on how to get in touch with questions or concerns.

### For speakers

- Speak clearly (ideally facing forward without covering your mouth)
- Avoid acronyms and colloquialisms as much as possible
- When addressing someone specifically, ask for his/her/their name and confirm pronouns
- Specify when you’re finished speaking
- For interpreters, always look at and address the participating attendee
- Repeat questions posted by the audience before responding, especially if there is not a roving microphone available.

### Coming soon

- **Supplier/ Vendor diversity list that encourages the use of LGBTQ-, woman-, veteran-, or minority-owned businesses.**
- **Venue Sourcing Accessibility Checklist**

- - -

## Swag/ [Merchandise Handling](https://about.gitlab.com/handbook/marketing/corporate-marketing/merchandise-handling/)
For merch team, see more details on handling and fulfillment specific on the [merchandise handling page](https://about.gitlab.com/handbook/marketing/corporate-marketing/merchandise-handling/)
All swag requests, creation and vendor selection is handled by the Corporate Events Marketing team.

- We aim to have our swag delight and/or be useful. We want to create swag that is versatile, easy to store and transport.
- As a remote company with team members in over 60 countries - our swag often has to go on miraculous journeys.
- With this in mind we try to ship things that are durable, light and that will unlikely get stuck in customs.
- We strive to make small batch, limited edition and themed swag for the community to collect.
- Larger corporate events will have custom tanuki stickers in small runs, only available at the specific event.
- Region specific sticker designs are produced quarterly.
- Our goal is to make swag self-serve => [web shop](https://gitlab.myshopify.com/). Please note our swag store is not intended for ordering and expensing back items purchased, see below on how to order swag if you are internal to GitLab.



- - -

## Social Marketing and Social Media

Please consult the [Social Marketing Handbook](/handbook/marketing/corporate-marketing/social-marketing/).

## All-Remote Marketing

Please consult the [All-Remote Marketing Handbook](/handbook/marketing/corporate-marketing/all-remote/).

## Corporate Communications and PR (Public Relations)

### Mission Statement

The mission of GitLab’s Corporate Communications team is to amplify GitLab's product, people and partner integrations in the media. This team is responsible for global PR (public relations).

### Our audience

The audience we aim to reach is external press/media. This includes business, lifestyle, workplace, finance, and beyond, using mediums such as print, digital, video, events, podcasts, etc.

### GitLab Master Messaging Document

GitLab's corporate communications team is the [DRI](/handbook/people-group/directly-responsible-individuals/) (directly responsible individual) for the GitLab Master Messaging Document. If this document is needed, please request access in the `#external-comms` Slack channel.

### Objectives and goals

As detailed in GitLab’s public [CMO OKRs](/company/okrs/), GitLab’s corporate communications team seeks to elevate the profile of GitLab in the media and investor circles, positioning it as a pioneer of remote work, increasing share of voice against competitors, and pulling through key messages in feature articles.

- Leverage events to generate media interest in GitLab's people and products
- Form and foster relationships with key reporters and publications
- Position GitLab executives and subject matter experts as thought leaders in their areas of expertise
- Increase GitLab's presence in media awards and accolades
- Increase GitLab's contributed content
- Work with social media team to cross-promote and amplify GitLab's media inclusions

### Contacting GitLab's PR team

##### For [Incident Communications Plans, please check out this handbook page](/handbook/marketing/corporate-marketing/incident-communications-plan/).

For external parties, please visit our [Get In Touch page](/press/#get-in-touch).

For GitLab team members, please use the `#external-comms` Slack channel and please follow the [Communication Activation Tree](https://docs.google.com/document/d/1qos3kjM_yIhS8-syey7WfR22SzmnHYFSCZpnFnBv7Rw/edit).

### Requests for Announcements

This process allows us to decide on the best channel to communicate different types of updates and news to our users, customers, and community.

Please follow the instructions below to request a formalized announcement around any of the following:

- A new product feature and capabilities
- A partner integration
- A significant milestone achieved
- A new initiative
- A customer case study
- Inclusion in an analyst report
- A breaking change
- A deprecation
- A change in policy or pricing
- A product promotion (launching or ending)
- Something else? If you're not sure if your case applies, please follow the directions below anyway, so the team can assess how best to proceed.

Please submit a request via an `announcement` [issue template in the Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=announcement) before opening an issue for a blog post, for example. In the issue template, you will be able to provide additional information on the proposed announcement. As a general guide, below the team has outlined three levels for announcements based on the type of announcements and suggested communications activities associated with each tier. The PR team will assess your request in the issue and determine how to proceed. If you are requesting a joint announcement and you are not part of the [Partner Marketing team](/handbook/marketing/strategic-marketing/partner-marketing/), please ensure you ping them on your issue.

- **Level 1** - A level 1 announcement will be announced via a press release and amplified with social media and an optional blog post. The execution of a blog post will be determined by the blog editorial team on a case by case basis. If the editorial team agrees to have a blog, then the social amplification will be the blog link as it includes assets that are helpful in the link cards across social channels. If there isn't an associated blog post, the social amplification can be for the press release link or relevant news coverage of the announcement. (In this case, the DRI needs to ensure there is an associated image to use with the release link or would make the decision of which news outlet link to select for social amplification.) Example announcements and news include but are not limited to: major GitLab company news around funding, earnings, executive new hires, analyst firm industry awards, acquisitions/mergers, Commit announcements, major joint partner news (ex. a partner such as AWS or Google) and major customer announcement (ex. enterprise or government agencies).
- **Level 2** - A level 2 announcement will be announced via a blog post and amplified with social media. The DRI/SME of the announcement will be responsible for working with the blog editorial team on creating the content and MR for the blog post (please see [the blog handbook](/handbook/marketing/blog/index.html#process-for-time-sensitive-posts) for more on this process). Example announcements and news include but are not limited to: partner integrations, new feature/capability highlights from the monthly release cycles (ex. Windows Shared Runners), customer case study announcement (not household names).
- **Level 3** - A level 3 announcement will be announced and promoted via GitLab’s social media channels. Example announcements and news include but are not limited to: awards from media publications (ex. DEVIES), speaking opps that GitLab employees are participating in (drive attendees/awareness) and ecosystem partner integrations.
- **Other** - In some cases, the following communications channels may be more appropriate:
    - A targeted email to affected users
    - Including an item in an upcoming release post (where the announcement is specifically tied to a release, does not require communication in advance of the release, and is not a sensitive topic)
    - A public issue (see [below](#using-public-issues-to-communicate-with-users))

### Press Releases

GitLab's corporate communications team is the [DRI](/handbook/people-group/directly-responsible-individuals/) (directly responsible individual) for writing all press releases that are issued by the company and routing through the appropriate approval process. The team has developed an [issue template to help make the press release development and approval process](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=press-release-template) more streamlined and consistent.  If you have any questions on the press release process or how to make an announcement request, please reach out via the `#external-comms` Slack channel or submit an `announcement` [issue template in the Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=announcement) (see Requests for Announcements section above).
#### Best Practices for an Announcement and Understanding Media Embargoes

GitLab team members will find [embargo and announcement guidelines in a confidential issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3348) (_must be logged in to access_).

This answers the following questions.

1. What is an embargo?
1. Why do we set embargoes?
1. Why don’t we talk about embargoed news prior to the announcement?
1. Who do we share embargoed news with?
1. Who is it okay to discuss this news with?
1. When can I talk about news freely with anyone?

### Using public issues to communicate with users

In some cases it may be more appropriate and [efficient](/handbook/values/#boring-solutions) to communicate with users in a public issue. Below are some examples of the types of communications that may suit an issue as opposed to a blog post, press release, or email, for example:

- Soliciting feedback (e.g. gathering community input on a proposal)
- Explaining upcoming or ongoing changes to GitLab (**not** breaking changes, changes which require users to take action, or otherwise sensitive)
- Sharing updates on a live, ongoing incident (this would be owned by the [Communications Manager on Call](/handbook/engineering/infrastructure/incident-management/#communications-manager-on-call-cmoc-responsibilities))
- See [below](#creating-your-communication-issue) for some examples

Using a public issue has a few advantages:

- In cases where you are seeking feedback or input from the community, your issue can serve as the single source of truth for communication on the subject, rather than spreading communication across a blog post and another page.
- You can easily add related epics and issues so that users can browse all the relevant information and contribute to the discussion. This also keeps the conversation all on GitLab, rather than spreading it across different channels.
- There is one fewer step/link to click on for community members to share their feedback.
- When seeking feedback, keeping things in an issue reinforces that a final decision has not yet been made. A blog post can give the impression of being a formal announcement rather than a proposal, which can have a negative impact on brand sentiment.
- You can get your message out there more quickly, because for most team members creating an issue is more familiar than writing and formatting a blog post. There's also no need to coordinate with the Editorial teams. However, please follow the guidelines in the [PR review and media guidelines](#public-gitlab-issues) section for review from the Corp Comms team.

#### Creating your communication issue

Below are some examples of using an issue to communicate something with our audience. Feel free to use these as a template for your issue. See below [PR review and media guidelines section](#public-gitlab-issues) for more details on the review process.

- [Feedback for ending support for Internet Explorer 11](https://gitlab.com/gitlab-org/gitlab/issues/197987)
- [Recent changes in GitLab routing](https://gitlab.com/gitlab-org/gitlab/-/issues/214217)
- [Color updates in GitLab to establish an accessible baseline](https://gitlab.com/gitlab-org/gitlab/-/issues/212881)

At a minimum, your issue should:

- **Be created in the most relevant GitLab project.** This would likely be the project where most of the discussion or work on your initiative is being done.
- **Have the `user communication` label applied.**
- **Include context for your message.** What background information might someone need to know to understand what you are communicating or asking? What relevant issues or epics can you add to help people understand?
- **Use subheadings for structure.** This makes your issue easy for readers to skim and pick out the most relevant information to them.
- **Include any key dates.** Be sure to call out if there is a deadline for submitting feedback.

#### Promoting your public issue

We can spread the word about a public issue in the same way that we would promote a blog post:

- You can [request promotion of your item on GitLab's social channels](/handbook/marketing/corporate-marketing/social-marketing/#social-medias-place-in-an-integrated-marketing-and-communications-strategy).
- You can request that your item be included in an upcoming newsletter by leaving a comment on the appropriate [newsletter issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues?label_name%5B%5D=Newsletter).
- Ask in #marketing on Slack if you would like to promote in some other way.

We can promote other items this way as well: for example surveys, landing pages, or handbook pages.
### PR review and media guidelines

Speaking on behalf of GitLab via a public channel such as a media interview (in-person or via phone), a podcast, a public issue on GitLab, a forum, a conference/event (live or virtual), a blog or an external platform is a significant responsibility. If you are unsure whether or not you should accept a speaking opportunity, create a public issue to gather feedback/communicate a message, or provide comment representing GitLab to a member of the media or influencer, please see below for guidance.

#### Communications trainings available to all team members
The communications team develops trainings and certifications for GitLab team members to up their practices when representing the company with the media, at an event, or on social media. [Consider taking a communication training to better align with the opportunities listed below.](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications-resourses-trainings/)

#### Speaking opportunities

If you are asked to speak on behalf of GitLab, consider reaching out to the PR and Developer Evangelist teams to ensure that the opportunity aligns with GitLab objectives. Inquiries should be initiated in the `#external-comms` Slack channel.

#### Research-oriented requests

For analyst research-oriented requests, please consult the [Analyst Relations](/handbook/marketing/strategic-marketing/analyst-relations/) handbook section and direct questions to the `#analyst-relations` Slack channel.

For press/media inquiries which are more research oriented, please share the request and associated context in the `#external-comms` Slack channel. An example of a research-oriented request is an invitation to participate in an interview to share more about one's day-to-day role at GitLab, specific tools teams use on a daily basis, how a team interacts and collaborates with other teams, specific products that are most helpful to them in their workflow, etc. The corporate marketing team will evaluate for reach, awareness, merit, and potential [conflicts of interest](/handbook/people-group/code-of-conduct/#avoiding-conflicts-of-interest), and advise on a case-by-case basis.

#### Honorariums

If the a research-oriented request or speaking opportunity has an honorarium offered (for example, a gift card or cash amount awarded for participation), please go through our [Approvals for Outside Projects & Activities process](/handbook/people-group/contracts-probation-periods/#approval-for-outside-projects--activities-aka-exceptions-to-piaa) to ensure a [conflict of interest is not present](/handbook/people-group/code-of-conduct/#avoiding-conflicts-of-interest).

#### Media mentions and interviews

If you are asked to be quoted or to provide commentary on any matter as a spokesperson of GitLab, please provide detail of the opportunity to the PR team in the `#external-comms` Slack channel.

In the event that a media member, editor, or publisher offers a draft or preview of an article where you are quoted, please allow the PR team to review by posting in the `#external-comms` Slack channel. The PR team will ensure that the appropriate GitLab team member(s) review and approve in a timely manner.

#### Public GitLab Issues

If you would like to [use a GitLab public issue to communicate an update](#using-public-issues-to-communicate-with-users) to customers/users or gather feedback from customers/users, please complete the `announcement` [issue template](/handbook/marketing/corporate-marketing/#requests-for-announcements) with the details of the specific request and draft of the issue announcement copy. The PR team will review the request details and issue copy, as well as recommend other relevant teams (product, product marketing, legal, etc) to loop in for review before posting.

#### Social media

Please consult the [Social Marketing Handbook](/handbook/marketing/corporate-marketing/social-marketing/). If you are contacted on a social media platform and asked to share/retweet or provide commentary as a spokesperson of GitLab, feel welcome to provide detail of the opportunity to the social team in the `#social-media` Slack channel.

#### Writing about GitLab on your personal blog or for external platforms

You are welcome to write about your experience as a GitLab team member on your personal blog or for other publications and you do not need permission to do so. If you would like someone to check your draft before submitting, you can share it with the PR team who will be happy to review. Please post it in the `#external-comms` Slack channel with a short summary of what your blog post is about.

- - -

Return to the main [GitLab Marketing Handbook](/handbook/marketing/).
