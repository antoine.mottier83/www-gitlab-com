---
layout: handbook-page-toc
title: "Developer Evangelism Community Response Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to engage Developer Evangelism in community response

Given the Developer Evangelism team's familiarity with our community and broad knowledge of GitLab, we regularly engage in managing situations that require GitLab to address urgent and important concerns of our community members.

### Notification

* **Upcoming announcement:** To notify the Developer Evangelism team of an upcoming announcement, product change, or other news event that may elicit a response from our community, the DRI should comment to `@johncoghlan` in a relevant issue or notify the Developer Evangelism team in the [#developer-evangelism Slack channel](https://gitlab.slack.com/archives/CMELFQS4B). 
* **In-progess situation:** To notify the Developer Evangelism team of an urgent situation that is in-progress, please tag the `@dev-evangelism` User Group in a Slack message in a Slack thread or channel where the situation is being discussed or the [#developer-evangelism Slack channel](https://gitlab.slack.com/archives/CMELFQS4B). 
* Please give the Developer Evangelism team as much notice as possible so we can help influence the messaging of the announcement, prepare responses for the community, and schedule coverage. See **Scheduling** below for more details.

### Preparation 

* To prepare for a community response situation, the Developer Evangelism team will typically create a list of expected questions/concerns from the community and draft responses. 
* For high-priority announcements and news, we will conduct a practice exercise to test our preparation. 
* In many cases, we will want to direct the community to share their feedback in the GitLab Forum. By preparing a post that we can link to from a blog post, announcement, or community response will streamline how we gather feedback feedback and manage responses. This post should be created by a Forum admin in advance as a private post which is published at the time of the announcement. 

### Scheduling

* If a response is expected to require monitoring and responses outside of our normal working hours (typically Monday - Friday, 900 UTC - 2200 UTC), please inform our team with as much notice as possible so we can schedule coverage accordingly. 
* Developer Evangelism team members who cover situations outside of their normal working hours are strongly encouraged to offset the extra time by scheduling time off in the days/weeks before or after the situation. 
* Scheduling should be coordinated with the DRI and tracked in a spreadsheet. Collaboration with the social media team and other teams may be required depending on the amount of coverage required. 

### Monitoring 

* Our team will typically monitor the [GitLab Forum](https://forum.gitlab.com/), [Hacker News](/handbook/marketing/community-relations/developer-evangelism/hacker-news/), Reddit, and other online forums.
* We will flag any content that we identify on social media in the appropriate channels, as needed. 
* The goal of monitoring these forums is to identify comments that require a response from GitLab and to respond directly or share those comments with the appropriate team member to respond. 

### Response 

* When possible and appropriate, our team will engage experts to respond to the community. This direct feedback from the DRIs is [appreciated by members of the wider GitLab community](https://news.ycombinator.com/item?id=26261479). It also allows the DRIs and experts to better understand community sentiment and get direct feedback from our community. 
* If necessary, the Developer Evangelists will provide responses to community questions and concerns on the GitLab Forum, Hacker News, and other forums. 
* If a post about GitLab reaches the front page of Hacker News, we may use a Zoom room and Google Doc to collaborate on responses in synchrously. We will link to the Zoom room and Google Doc in the Slack thread or channel where the situation is being discussed or the [#developer-evangelism Slack channel](https://gitlab.slack.com/archives/CMELFQS4B).
* The Developer Evangelism team will rarely engage in responding on Twitter and other social media channels during these announcements and breaking news situations. Twitter, Facebook, and LinkedIn are primarily owned by the Social Media team in these situations.  
* For situations where messaging is sensitive, we will rely on approved messaging from the Corporate Communications team to create our responses. 

### Feedback 

* Our team is happy to provide sentiment assessments from our monitoring as part of the reporting or retrospective process for any announcements or in-progress situations which we are asked to support. Please tag us in the appropriate issue or document with a clear ask to request feedback. 