---
layout: handbook-page-toc
title: GitLab Event Information
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Events at GitLab

**This page outlines details for in-person events. For virtual event information, please visit the [Virtual Events Page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/).**

There are 3 groups within marketing who handle external events. Each group has a specific purpose. Please review each page for specific details.

- [Community Relations](/handbook/marketing/community-relations/evangelist-program/)
- [Corporate Events](/handbook/marketing/corporate-marketing/#corporate-events)
- [Field Marketing](/handbook/marketing/revenue-marketing/field-marketing/)

[Marketing Programs](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/) supports Corporate and Field Marketing in relation to all events with communication timeline, technical tracking, reporting, email deployment, landing page creation, and more as required by the event.

## COVID-19 Event Disclaimer

GitLab’s top priority is the health and safety of our attendees and team members. GitLab reserves the right to switch any hosted in-person event to a virtual event. If this is done, we will communicate to all attendees via email.

- Note, when building invite landing pages, please ensure the complete paragraph above has been added to the landing page.

Travel guidance on COVID-19 can be [found here](/handbook/travel/#travel-guidance-covid-19).

### Legal verbiage that should be included in all event contracts thoughout FY21

- This language is what we should be adding to supplier agreements (when agreeing on their template):
    - "Termination for Convenience. GitLab may terminate this Agreement, any SOW or Order, or all at any time, for no reason or for any reason, upon notice to Vendor. Upon receipt of notice of such termination, Vendor shall inform GitLab of the extent to which it has completed performance as of the date of the notice, and Vendor will collect and deliver to GitLab whatever Work Product then exists, if applicable. GitLab will pay Vendor for all Work acceptably performed through the date of notice of termination, provided that GitLab will not be obligated to pay any more than the payment that would have become due had Vendor completed and GitLab had accepted the Work. GitLab will have no further payment obligation in connection with any termination. Upon termination or expiration of this Agreement, Vendor shall return or destroy any GitLab Confidential Information and provide certification thereof."
- In the event that any supplier does not accept the termination for convenience language here, then, we can following up with this option
    - "Neither party shall be liable to the other for delays or failures in performance resulting from causes beyond the reasonable control of that party, including, but not limited to, acts of God, labor disputes or disturbances, material shortages or rationing, pandemics, riots, acts of war, governmental regulations, communication or utility failures, or casualties (“Force Majeure”).
- In the event of a Force Majeure impact on the performance of either party, then the parties are immediately relieved of obligation to perform. From notice to supplier, GitLab is relieved of the payment obligation. As soon as is practical, but not more than sixty (60) days from GitLab’s notice to supplier, GitLab shall receive from supplier a pro-rata refund of, any fees previously paid, from the date of notice to supplier to the end of the term."

## Which events is GitLab already sponsoring?

- Internal tracking of in person events, please add our [Events and Sponsorship Calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9laWN2b3VkcHBjdTQ3bG5xdTFwOTlvNjU2Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to your calendar.
- Internal tracking of virtual events [can be found here](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#gitlab-virtual-events-calendar).
- External facing event tracker can be found at [https://about.gitlab.com/events/](/events/)

## Interested in attending an event we are already sponsoring?

- If your primary interest in the event is attending sessions and networking with folks at the conference, you should likely attend said event as a training opportunity rather than as part of our official event sponsorship. Next steps for you would be to get your manager's approval to book. Send them your requested budget for travel and the event ticket to get approval. We suggest a budget of $300-$500 a day depending on the location. Once you have your manager's approval, you can book a pass and expense.
- If you want to attend to practice your pitch, meet customers/prospects or work the booth then it sounds like you would benefit from attending as part of our sponsorship. People who attend an event as part of a sponsorship should expect to spend at least 80% of their time at a show dedicated to specific sponsorship activities. You will be on booth duty or something simmilar most of the day and doing GitLab networking events in the evening. Do not expect to go off on your own to explore a new city or meet up with friends in the area. You can come early or stay late or see option above if that is what you are hoping for.

## Suggesting an Event

To determine who would handle the event, please refer to our [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit#gid=0). If it is not clear who should own an event based on the decision tree, please email `events@gitlab.com`.

Please _only request_ event support/sponsorship if your proposed event fits the following criteria:

- The event will further business aims of GitLab.
- The event has an audience of **250+ people** (the exception being meet-ups (which are run by our community team) or is part of an Account Based Marketing activity.
- The event is a more than a month away.

If your event fits the criteria above and you would like support from marketing, create an issue in the appropriate marketing project.

- [**Community Relations**](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request)
    - `For Corporate Event DRIs:` If you are the DRI for the event see below for "event execution" for next steps once you have opened and completed issue template. We are using Epics as the SSOT for the event over Meta Issues.
- [**Corporate Events**](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=Corporate-Event-Request) - use the "Event-Request" template
- **Field Marketing**
`Please Note:` _These requests will be reviewed by the appropriate regional Field Marketing Manager. Even if the event is free or low-cost, all requests must be approved by Field Marketing prior to any commitment to ensure the event or tactic aligns to the marketing strategy, meets MQL objectives and Field Marketing can properly execute to maximize GitLab’s future success._
    - To suggest a Field Marketing event (in-person or virtual) or other tactic for review, please open an [`Event_Tactic_Request`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=Event_Tactic_Request) issue and follow the instructions.

Be sure you review the issue template and provide all necessary information that is asked of you in the issue. We especially need goals/ reason to attend.

## How We Evaluate and Build Potential Events

All GitLab events must check at least drive two or more of the aims of our events below to be considered.

- Brand awareness- we want to be a household name by 2020!
- Build community
- Gain contributors
- Thought leadership
- Help with hiring
- Gather new relevant leads/ drive ROI
- Educate possible buyers or users on our product or features
- Marketplace positioning
- Partnerships/ Alliances

### Corporate events must also meet:
{:.no_toc}

- Audience minimum requirement of 5000+ attendees NA, 3,000+ EMEA, 1500+ other territories and...
- Audience demographic requirements. We consider the balance of roles represented (contributor, user, customer, potential hires), and the Global reach of the audience. Audience profile must be over 50% national/ global.

## Questions we ask ourselves when assessing an event:

- How and where will this position us as a brand?
- Does this event drive business goals forward in the next quarter? Year?
- Is the event important for the industry, thought leadership, or brand visibility? We give preference to events that influence trends and attract leaders and decision makers. We also prioritize events organized by our strategic partners.
- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team-member or a member of the wider GitLab community.
- What type of people will be attending the event? We prefer events attended by diverse groups of decision makers with an interest in DevOps, DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We stress events that provide opportunities for meetings, workshops, booth and/or stands to help people find us, as well as create other interactions with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of support.
- What is the size of the opportunity for the event? We prioritize events based their potential reach (audience size, the number of interactions we have with attendees) and potential for ROI (also account for cycyle time).
- What story do we have to tell here and how does the event fit into our overall company strategy, goals, and product direction?
- Do we have the bandwidth and resources to make this activity a success? Do we have the cycles, funds, collateral and runway to invest fully and make the event as successful as possible? Event must be weighed against other current activity in region and department.

Suggested events will be subject to a valuation calculation - will it meet or exceed objectives listed above?

### For Corporate Marketing - Event Scorecard

Each question above is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 8 are not eligible for corporate sponsorship or financial support.
- Events scoring 10+ are given top priority for staffing, and resources.

| Criteria / Score | 0 | 1 | 2 |
| ---------------- | --- | --- | --- |
| Thought Leadership |  |  |  |
| Audience type |  |  |  |
| Attendee interaction |  |  |  |
| Location and Timing |  |  |  |
| Event Relevance/ Strategy |  |  |  |
| Brand Reach |  |  |  |
| Opportunity size/ Potential ROI |  |  |  |

We ask these questions and use this scorecard to ensure that we're prioritizing the GitLab's brand and our community's best interests when we sponsor events.

## Event Execution

### Step 1- Issue Creation

#### All Events - New issue Creation

1. **Corporate Marketing Event Owner** creates issue using the [Corporate Event Request Template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=corporate-event-request#) in the Corporate Marketing project. **Field Marketing Event Owner** creates issue using the appropriate [Field Marketing Event Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/tree/master/.gitlab/issue_templates) in the Field Marketing project.
1. Add as much information about the event as possible. For the event to move into contracting, we will specifically need to know the cost and ROI.

### Step 2- Campaign Tag Creation

#### All events - Setting up the `Campaign Tag`

**NOTE**: Event owners are DRI to create/setup campaign names & tags!

1. When **Corporate Marketing Event Owner** (DRI) begins campaign process, they will add the `campaign tag` (using proper `ISOdate_Name` format & within 31 character limit) into the associated budget line item. When **Field Marketing Event Owner** (DRI) begins campaign process, they will utilize the `campaign tag` that is auto-created in the Field Marketing budget document (this campaign tag is created in the campaign's line item by formula).
    1. `Campaign Tag` ISO date = **first** day of event (if multiday) - Example: AWS reInvent Dec 2-5, 2019, `Campaign Tag` = `20191202_AWS_re:Invent` this will also be applied to the SFDC Campaign name & Marketo Program name
    1. If budget line corresponds to one vendor and the vendor is known, they will add that information as well to make matching future invoices easier.
    1. We do not want to set this up too soon as it affects many systems - see [date change](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/#changes-to-offline-events-dates) but it does need to be done when we go into contracting or before any money is spent toward said event.

### Step 3 - Netsuite campaign tag creation for events UNDER the employee [authorization matrix](/handbook/finance/authorization-matrix/#authorization-matrix) approval threshold **AND** no contract or security review required

1. **Event Owner** copy/paste the `campaign tag` form the budget document into the Corporate request issue or Field Marketing issue.
1. **Event Owner** to ping the GL Accountant (@GGGONZALEZ) in the main event issue to request campaign tag be created in Netsuite.
1. When event budget is approved by Finance, **Event Owner** verifies that the tag on the event and finance issue are accurate and match.
1. MPM will use the <b>exact `campaign tag`</b> as the SFDC Campaign and Marketo Program name so it is the unique identifier across all systems.

##### **NOTE:** All events under the employee [authorization matrix](/handbook/finance/authorization-matrix/#authorization-matrix) approval threshold **AND** No contract or security review required can skip step 4- contracting process.

### Ancillary Events

If the overarching event includes a speaking session, workshop, dinner, and/or happy hour (anything that requires a separate SFDC campaign and list upload), please follow the steps below and abide by the SLA guide to give necessary time for work to be complete.

**⏰ SLAs for YES/NO section to be filled out for an ancillary event before moving to WIP:**

- **45 business days:** if the ancillary event will _require Marketo invitation_.
- **20 business days:** if the ancillary event will only need tracking and follow up email.

The purpose of creating a new issue for each ancillary event is to better facilitate the needs for each event, to clearly indicate the timelines, and to clarify the DRIs which may be different for the different events. For example, if there is a speaking session hosted by a GitLabber at a larger conference, and the speaker is planning to write the email copy after the event, the timeline and DRI would be different from the timeline and DRI for the general booth follow email.

**Steps for Ancillary Events:**

1. **DRI to create a new issue for each ancillary event** that corresponds to the main Field/Corporate Event (ex: Dinner + Date - Event Name as shown on the original issue) and assign it to event DRI & MPM DRI.
    - For each ancillary event issue, fill out the MPM YES/NO checklist to communicate if there will be a landing page, invitation & reminder, and/or follow-up as part of the ancillary event.
    - Note: All copy (both for the overall event and the ancillary events) is kept in a single copy doc and linked from the Event Epic
    - Timeline and SLAs will follow existing agreement [FM/MPM](/handbook/marketing/events/#timelines-and-slas-between-field-marketing-and-marketing-programs), [Corp/MPM](/handbook/marketing/events/#timelines-and-slas-between-corporate-marketing-and-marketing-programs)
1. **Marketing Program Manager will add the related necessary execution issues** as indicated by the YES/NO section in the issue. **This section must be complete before the issue moves to `status:wip`.**
    - Ex: Landing Page - Dinner + Date - Event name as shown on original issue)
1. **Marketing Program Manager will add as a new line item for each ancillary event in the [Events googledoc](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=0)**
1. If the event is approved to move forward to contracting, is on the budget sheet, and the campaign tag has been created (Corporate Marketing) or auto-created by formula (Field Marketing), the DRI will start finance issue for contract review and signature following the [Procure to Pay Process](/handbook/finance/procure-to-pay/). The event DRI/FMC (for Field Marketing) is in charge of the finance issue creation and ushering it along. The finance issue will include:
    - ROI calculation for event in the final cost section.
    - Add the finance issue as a `Related issue` to the original issue for reference.
    - Add in any discounts or contract negotiations you have already completed.
    - **Link to main event issue.**
1. DRI to complete all relevant steps in the contract issue and close it out.

### Step 5- Contract Completed - FMM

1. When contract has been signed the DRI will update issue label from `status:plan` to `status:wip`.
    - At this point, all relevant details (loation, venue, time, date, etc.) and the MPM Checklist in the Field Marketing issue must be completed.
    - The designated MPM will begin the backend execution using this information. [See MPM steps to set up event epic](/handbook/marketing/events#mpm-steps-to-set-up-event-epic)
    - The designated MPM will begin the backend execution.
    - The MPM will create the event Epic, adding the checklist of related issues that need to be opened by respective team and high level information.
    - The MPM will also associate any issues opened for the event to the Epic.

### Step 5- Contract Completed- Corporate Event DRI

1. For **Corporate Event DRI**,When contract has been signed the DRI will being event **epic creation process**.
    - Below is the tactical process DRIs take to organize actions and timelines for field and corporate events. This process was created by MPM's to keep projects organized and on track.

#### Step A (Corporate Marketing): DRI creates the event epic

- DRI creates epic for the event
  - **please note that this epic should only be used for in-person events**) Use the [sponsored virtual conference epic](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-sponsored-virtual-conference) for virtual conferences
- Naming convention: [Event Name] - [3-letter Month] [Date], [Year]
- DRI copy/pastes epic template below into the epic description.
- In "Issue Creation" section, DRI deletes any pieces that aren't necessary.
- The DRI will also associate any issues opened for the event to the Epic. Link the Original _Field_ or _Corporate_ marketing issue (some may refer to this issue as "META")
- Booth number (should be included in Epic name)
- Any other high level information that will be relevant to anyone attending

NOTE: The Epic is the main hub for all event information. **All issues** associated with the Event **must** be linked to the Epic!

# Epic Template

```
## Event Details

*Reminder: please only use this template for in-person events*

* `place details from the event issue here`
  * [main salesforce campaign]()
  * [main marketo program]()
  * campaign utm `enter utm here` (Format: Campaign Tag, must be all lowercase, with no spaces, underscores, or special characters)
  * Event Budget: 
  * Event Goals: 
  * [Add the planning sheet]()
  * Link to event landing page:

## [Copy for landing page and emails >>]() - [template](<a href="https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit">https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit</a>)

* Alliances/Partner Marketing involved - (if yes, must designate a DRI)
* Will this event include use of Marketing Development Funds (MDFs)? YES/NO
   * if yes, please tag Tina Sturgis here, and complete the section below
        * [ ]  MDF request completed by FMM and sent to Tina at least 1 quarter out
        * [ ]  event accepted for use of MDF (tag Tina here for her to approve)
        * [ ]  receipt submitted post event
        * [ ]  proof of payment submitted
        * [ ]  leads submitted 

## Issues to be created

* [ ] [Facilitate tracking issue created](<a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking">https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking</a>) - Event owner creates, issue goes through triage
* [ ] [List clean and upload issue created](<a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list">https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list</a>) - MOps creates, assigned to event owner and MOps
* [ ] [Follow up email issue created](<a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email">https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email</a>) - Event owner creates, issue goes through triage
* [ ] [Add to nurture issue created](<a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture">https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture</a>) -(Optional) Event owner creates, issue goes through triage
* [ ] [Marketo landing page copy issue created](<a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-copy">https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-copy</a>) - Event owner creates, assign to Corp/FMM and FMC
* [ ] [Marketo landing page creation issue created](<a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-creation">https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-creation</a>) - Event owner creates, issue goes through triage
* [ ] [Invitation and reminder issue created](<a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-03-invitations-reminder">https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-03-invitations-reminder</a>) - (Optional) Event owner creates, issue goes through triage
* [ ] [Target segment issue created](<a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=dma_request">https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=dma_request</a>) - (Optional) MOps creates, assigned to event owner and MOps
* [ ] [Onsite registration form & landing page](<a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-08-onsite-reg-form-lp">https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-08-onsite-reg-form-lp</a>) - (Optional) Event owner creates, issue goes through triage

cc @jburton to create list upload issue and DMA request as required

```

#### Step B (Corporate Marketing): Templates to create the necessary issues listed above and add to epic

- Using the relevant issue templates MPMs and MOps create issues in proper project and then link from the epic.
    - [Follow up email](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email)
    - [Landing page](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-02-landing-page)
    - [Invitations & reminder](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-03-invitations-reminder)
    - [Add to nurture](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-Add-to-Nurture)
    - [List clean and upload](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list)
    - [Target segment](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=dma_request)
    - [Onsite registration form & landing page](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-08-onsite-reg-form-lp)
- Naming convention: `[Issue naming convention] - [Event Name]`
- During these issue creations, DUE DATE is required to be added by issue creator.
- Issue creator associates all issues to the event epic.
- **IF** there are ancillary events associated with the main event, the DRI will have created a separate event issue for each ancillary event. Each ancillary event will need it's own YES/NO checklist to create related necessary issues (ex: Landing Page - Happy Hour - Event Name). ONce created tag your MPM. MPM will add all issues and new program links (Marketo/SFDC) to the overall event Epic.

⚠️ Note: MOps is required to create their list clean and upload issue, and DMA list issue in their project with the proper template and associate back to the epic.

☝️ _Tip: DRIs use [this document](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=0) to auto-populate timelines / SLAs for events. They will add the event as a new row when it is created with a status of Plan, add the start and end date, and look at the resulting due dates for each action item._

### Step 6- DRI Event Admin Tasks

1. Add the event to Events Cal and [Events Page](/events/).
    - How to [add an event to the events page](#how-to-add-events-to-aboutgitlabcomevents).
1. Start an [event DRI issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=corporate-event-request#) and start checking off the necessary tasks. Some things to note as you go through process in template:
    - Start an event specific slack channel.
    - Once team member staffing has been selected invite them to the channel in addition to other Field Marketing or Alliance team members that will be involved.
    - Do not link anything but the Epic in the slack channel.
    - Share the planning sheet for team members to add their contact and travel information.
    - Instruction everyone to book travel and lodging ASAP.
    - The planning sheet is used to track all details for travel, meeting setting, booth duty, speaker list, networking events, PR, etc.
1. If the event needs a speaker, start an issue with the `Speaker Request` issue template.
1. If a customer speaker is required for this event, assign the `Speaker Request` to your regional [Customer Reference lead](/handbook/marketing/strategic-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact). The Customer Reference lead will work with the Event Managers and the Sales team in identifying and engaging with customers to speak at events.
1. Landing Pages for Events (detailed instructions)
    - Corporate event landing pages are generated from the `events.yml` - Detailed instructions
    - The MPM will create an issue for content to be provided working with Alliances &/or Product Marketing team on copy.
    - A collaborative decision will be made to include a form on the landing page.
    - Owned events will use a landing page generated by the `events.yml` **OR** a Marketo landing page. (Detailed instructions) Marketo will be used if **ALL** of the following criteria are met:
    - The event is **owned** by Field Marketing
    - The event will cost GitLab less than $50,000 USD (or your country's equivalent)
    - Promotion of the event will be no longer than 1.5 months
1. Schedule
    - Event kick off call scheduled approx **two months** out from event will include all people involved in planning
    - Final event check in meeting including everyone attending, involved Alliance team members and Technical Marketing team who created demos to review content with team.
    - Event recap will include all planners and stakeholders.
1. Copy needed
    - Landing page copy
    - Email invite copy - **3 to 4 weeks** in advance of event
    - Post Event Email copy - **1 to 2 weeks** in advance of event
1. Social
    - Start issue using the `Social Request` template for general social awareness posts and any social ads that need to be created.
    - For complete instructions on how to obtain social support at your event, please review the [social requests instructions](/handbook/marketing/corporate-marketing/social-marketing/admin/#requesting-social-promotion-).
1. Design
    1. For the latest approved booth design & messaging, email `events@gitlab.com`.
    1. Open issue in the `Corporate Marketing` project for booth design. Assign to Design team and provide booth spec and due date. Provide as much notice as possible.
    1. For any content or major layout changes, tag Strategic Marketing and Design in the related booth design issue created in the `Corporate Marketing` project.
1. Digital
    - Coordinate all digital marketing requests with your regional MPM. See [Requesting Digital Marketing Promotions](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#requesting-digital-marketing-promotions) for more info.
1. For Corporate events - Meeting Setting
    1. All leads gathered through meeting setting must be tracked in their own campaign which will be set up by the MPM and associated to the main event campaign.
    1. We generally provide a small give (under $50 USD or country equivalent) for anyone who takes a meeting with us.
    - **NOTE**: We share these prep documents with the client. The document is intended to provide everyone attending the meeting with background information on the prospect &/or customer. The document should also include any objectives or topics to cover in the meeting.
    1. All on-site meetings must have a [meeting prep doc](https://docs.google.com/document/d/1hyA12EN5iEwApAr_g4_-vhUQZohKxm5xkX9xxZ1JNog/edit), which will be linked in the master planning sheet.
    - The event spreadsheet will be locked 24 hours before the event starts.
    - Any changes need to be submitted by making a **Comment** on the spreadsheet and assigning it to the **Field Marketing** DRI.
    1. Most **Corporate events** will have an onsite meeting setting initiative tied to the event goals.
    1. Corporate Marketing will work with the regional Field Marketing DRI and designated event MPM to decide on best meeting setting strategy.
    1. If any Executives are attending, all meetings scheduled for them will be coordinated through the designated EA for that event.
    1. Meetings are tracked on the master event spreadsheet.
1. Demos, booth decks and documentation
    - Product Marketing helps make all displayed collateral at events.
    - The [standard demos](/handbook/marketing/strategic-marketing/demo/) should be preloaded on the event iPads.
    - If you need something specific for an event, start an issue in the Product Marketing project and assign to Dan Gordon at least **three weeks in advance**.
1. Swag
    - Decide appropriate giveaway for the event and audience.
    - Coordinate ordering with one of the preferred swag vendors.
    - Order extra storage at the event if all swag will not fit witin the booth.
1. Leads and Campaign Setup
    - Field Marketing DRI is responsible for pulling, cleaning and sharing the lead list with the MPM and MktgOps within 24 hours of event close or as soon as received by event coordinators.
    - If the event had multiple parts (booth, happy hour, meetings, etc) each will have its own Salesforce campaign and [Member status progressions](/handbook/marketing/marketing-operations/#campaign-type--progression-status).
    - Use template to standardize the data following the [list import guidelines](/handbook/sales/field-operations/gtm-resources/).

### Ancillary Events

If the overarching event includes a speaking session, workshop, dinner, and/or happy hour (anything that requires a separate SFDC campaign and list upload), please follow the steps below and abide by the SLA guide to give necessary time for work to be complete.

**⏰ SLAs for YES/NO section to be filled out for an ancillary event before moving to WIP:**

- **45 business days:** if the ancillary event will _require Marketo invitation_.
- **20 business days:** if the ancillary event will only need tracking and follow up email.

The purpose of creating a new issue for each ancillary event is to better facilitate the needs for each event, to clearly indicate the timelines, and to clarify the DRIs which may be different for the different events. For example, if there is a speaking session hosted by a GitLabber at a larger conference, and the speaker is planning to write the email copy after the event, the timeline and DRI would be different from the timeline and DRI for the general booth follow email.

**Steps for Ancillary Events:**

1. **DRI to create a new issue for each ancillary event** that corresponds to the main Field/Corporate Event (ex: Dinner + Date - Event Name as shown on the original issue) and assign it to event DRI & MPM DRI.
    - For each ancillary event issue, fill out the MPM YES/NO checklist to communicate if there will be a landing page, invitation & reminder, and/or follow-up as part of the ancillary event.
    - Note: All copy (both for the overall event and the ancillary events) is kept in a single copy doc and linked from the Event Epic
    - Timeline and SLAs will follow existing agreement [FM/MPM](https://about.gitlab.com/handbook/marketing/events/#timelines-and-slas-between-field-marketing-and-marketing-programs), [Corp/MPM](https://about.gitlab.com/handbook/marketing/events/#timelines-and-slas-between-corporate-marketing-and-marketing-programs)
1. **Marketing Program Manager will add the related necessary execution issues** as indicated by the YES/NO section in the issue. **This section must be complete before the issue moves to `status:wip`.**
    - Ex: Landing Page - Dinner + Date - Event name as shown on original issue)
1. **Marketing Program Manager will add as a new line item for each ancillary event in the [Events googledoc](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=0)**

## MPM steps to set up event epic- transitioning out of this process

Below is the tactical process DRIs take to organize actions and timelines for field and corporate events. This process was created by MPM's to keep projects organized and on track. Please comment in the [#marketing_programs slack](https://gitlab.slack.com/messages/CCWUCP4MS) if you have any questions.

#### Step 1: The event issue (in the field marketing or corporate marketing repo) moves to Status:WIP

- When the FMM/Corporate issue moves to WIP, all relevant details (loation, venue, time, date, etc.) **must** be included by the event organizer at the top of the issue.
- At this point, the FMM/Corporate DRI must have already documented the elements (i.e. landing page, speaking session, etc.) needed for the event.

⚠️ *If the FMM does not list the elements needed of the MPM in the issue, the MPM will not be able to move forward. Having this information up-front will minimize back-and-forth with MPM and let them take action appropriately and efficiently.

#### Step 2: DRI creates the event epic

- When "status:wip" is on the issue and necessary elements are documented, MPM creates epic for the event.
- Naming convention: [Event Name] - [3-letter Month] [Date], [Year]
- MPM copy/pastes code below into the epic

```
## [Main Issue >>]()

## [Copy for landing page and emails >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## Event Details
  * `place details from the event issue here`
  * [ ] [main salesforce campaign]()
  * [ ] [main marketo program]()
  * [ ] campaign utm `enter utm here` (Format: Campaign Tag, must be all lowercase, with no spaces, underscores, or special characters)

## Issue creation

* [ ] [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - MOps creates, assigned to event owner and MOps
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) - MPM creates, assigned to event owner and MPM
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) -(Optional) MPM creates, assigned to event owner and MPM
* [ ] [Landing page issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-02-landing-page) - (Optional) MPM creates, assigned to event owner and MPM
* [ ] [Invitation and reminder issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-03-invitations-reminder) - (Optional) MPM creates, assigned to event owner and MPM
* [ ] [Target segment issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=dma_request) - (Optional) MOps creates, assigned to event owner and MOps
* [ ] [Onsite registration form & landing page](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-08-onsite-reg-form-lp) - (Optional) MPM creates, assigned to event owner and MPM

cc @jburton to create list upload issue and DMA request as required
```

#### Step 3: MPM updates the event epic details

- In epic description, MPM copies over the "need-to-know" event details section from the WIP main issue.
- In "Issue Creation" section, MPM deletes any pieces that aren't necessary based on the FMM answers for event elements at top of main event issue.

#### Step 4: MPM and MOps create the necessary issues and add to epic

- Using the relevant issue templates MPMs and MOps create issues in proper project and then link from the epic.
    - [Follow up email](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email)
    - [Landing page](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-02-landing-page)
    - [Invitations & reminder](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-03-invitations-reminder)
    - [Add to nurture](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-Add-to-Nurture)
    - [List clean and upload](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list)
    - [Target segment](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=dma_request)
    - [Onsite registration form & landing page](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-08-onsite-reg-form-lp)
- Naming convention: `[Issue naming convention] - [Event Name]`
- During these issue creations, DUE DATE is required to be added by issue creator.
- Issue creator associates all issues to the event epic.
- **IF** there are ancillary events associated with the main event, the FM will have created a separate FM event issue for each ancillary event. The MPM will follow the usual YES/NO checklist for each ancillary event to create related necessary issues (ex: Landing Page - Happy Hour - Event Name). MPM will add all issues and new program links (Marketo/SFDC) to the overall event Epic.

⚠️ Note: MOps is required to create their list clean and upload issue, and DMA list issue in their project with the proper template and associate back to the epic.

☝️ _Tip: MPMs use [this document](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=0) to auto-populate timelines / SLAs for events. They will add the event as a new row when it is created with a status of Plan, add the start and end date, and look at the resulting due dates for each action item._

## Timelines and SLAs between Field Marketing and Marketing Programs

In this timeline, BD = Business Days, and incorporates US public holidays and T = start date of the event.

- **T-45 BD:** Final landing page copy and invitation 1 copy due by FMM to MPM
- **T-40 BD:** Final landing Page launched
- **T-30 BD:** Invitation 1 is sent
- **T-20 BD:** Final copy for invitation 2 due by FMM to MPM
- **T-15 BD:** Invitation 2 is sent
- **T-10 BD:** (Optional) Final copy for invitation 3 due by FMM to MPM
- **T-5 BD:** (Optional) Invitation 3 is sent
- **T-5 BD:** Final copy for reminder email due by FMM to MPM
- **T-3 BD:** Final copy for follow up emails due by FMM to MPM
- **T-1 Day:** Reminder Email is sent _Note - not business day_
- **T+2 BD:** Follow up email(s) are sent

## Timelines and SLAs between Corporate Marketing and Marketing Programs

In this timeline, BD = Business Days, and incorporates US public holidays and T = start date of the event.

- **T-70 BD:** Final landing page copy and invitation 1 copy due by Corp Mktg to MPM
- **T-65 BD:** Final landing Page launched
- **T-30 BD:** Invitation 1 is sent
- **T-20 BD:** Final copy for invitation 2 due by Corp Mktg to MPM
- **T-15 BD:** Invitation 2 is sent
- **T-10 BD:** (Optional) Final copy for invitation 3 due by Corp Mktg to MPM
- **T-5 BD:** (Optional) Invitation 3 is sent
- **T-5 BD:** Final copy for reminder email due by Corp Mktg to MPM
- **T-3 BD:** Final copy for follow up emails due by Corp Mktg to MPM
- **T-1 Day:** Reminder Email is sent _Note - not business day_
- **T+2 BD:** Follow up email(s) are sent

### Changes to Offline Events dates

Sometimes, it becomes necessary to change the date of an offline event. Once the FMM has identified that the date will change, the **FMM** will:

- Update the event date and the campaign tag on the main event issue and in the Budget Document.
- In the main event issue, ping the GL Accountant (@GGGONZALEZ) with the old campaign tag to be removed from Netsuite and the new campaign tag to be added in Netsuite.
- Tag the MPM for the event in a comment to notify them that the date has changed.

If the change in the date is related to COVID-19, the **FMM** will:

- Remove the line item from your region tab in the budget document.
- Complete the [COVID-19 Cancellations Tracker - FM Only](https://docs.google.com/spreadsheets/d/1QC6P0VRWwJheOlGB-9bX8JIF8_4UY3h1cGVT_gacv5M/edit#gid=1753355316&range=A1) - ONLY if we have made a payment against the cancelled event, as this is where Finance and Accounting will go to track the refunds we may need to account for.

If the date changes after the MPM set up the issues, epic, and tracking for the event, the **MPM** must make the following updates:

- **No Registrations Received:** If no registrations have been processed for the event (even if members have been added to the campaign as "Marketing Invited"):
    - SFDC: Change the ISO date in the SFDC Campaign name to the new date. Update the campaign start date to 30 days prior to the new date, and the end date to 60 days after the new date.
    - Marketo: Change the ISO date in the Marketo Program to the new date. Update the tokens and confirm landing pages, confirmation emails and invites are updated.
    - GitLab: Update event date in epic and issues. Update due dates and email deployment dates.
- **Registrations Received:** If registrations have been processed for the event:
    - SFDC: Change the ISO date in the SFDC Campaign name to the new date. Leave the campaign start date as-is (reflecting 30 days prior to the original date), and update the end date to 60 days after the new date.
    - Marketo: Change the ISO date in the Marketo Program to the new date. Update the tokens and confirm landing pages, confirmation emails and invites are updated.
    - GitLab: Update event date in epic and issues. Update due dates and email deployment dates.

### Offline Events turned Digital

#### When Owned Offline Events turn to Virtual Owned events

FMM and MPM determine format of this virtual event from [documented options.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/) FMM lets interested teams know that the event has switched to digital, the existing landing page is no longer accepting registrations and sales-nominated flow has been stopped (if applicable).

- **If this is a self-service ZOOM Meeting with promotion:**
    - FMM: create the new epic, issues needed (e.g. list upload) and ZOOM meeting.
    - FMM: provide new campaign tag (if applicable).
    - MPM: create new marketo program under Virtual Events cloning `YYYYMMDD_HostName_Topic (External Webcast Template)` template and save to the `GitLab Webcasts` folder. If you are using the same campaign tag as the original live event, add "OLD" to the beginning of the existing live event Marketo program as Marketo will not allow two programs with the same name.
    - MPM: create a new folder in the `GitLab Webcasts` folder for the virtual event.
    - MPM: create new SF campaign by syncing new marketo program to SFDC.
    - MPM: update links in Epic to new SFDC and Marketo links. Update the utm if applicable.
    - MPM: follow standard processes with tokens, program activation, etc.
    - FMM: set up Zoom Meeting and provide Zoom link and ID to MPM.
- **If this is a GitLab Hosted ZOOM Webcast:**
    - FMM: provide new campaign tag (if applicable).
    - MPM: If Epic wasn’t created, create the epic with the [Virtual Event Epic code.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#step-2-mpm-will-create-the-virtual-event-epic) If Epic was created, change it to the Virtual Event Epic Code.
    - MPM: Create new marketo program under `Virtual Events` cloning `YYYYMMDD_WebcastTopic_Region (Single time slot)` or `YYYYMMDD_WebcastTopic_Region (Multiple Time Slot)` template and save to the `GitLab Webcasts` folder. If you are using the same campaign tag as the original live event, add "OLD" to the beginning of the existing live event Marketo program as Marketo will not allow two programs with the same name.
    - MPM: create a new folder in the `GitLab Webcasts` folder for the virtual event.
    - MPM: create new SF campaign by syncing new marketo program to SFDC.
    - MPM: update links in Epic to new SFDC and Marketo links. Update the utm if applicable.
    - MPM: follow standard processes with tokens, program activation, etc.
    - MPM: [set up ZOOM Webcast](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#step-3-configure-zoom)
- **Existing event Marketo program MPM tasks (applies to both types):**
    - Move the existing event marketo program under the new folder you have created and add `OLD` at the beginning of the title.
    - Deactivate the `Registration` and `Interesting Moments` smart campaigns.
    - Deactivate any invitations (ex: sales-nominated) smart campaigns that are live and stop all promotions.
    - If you want to create a new landing page for the virtual event and the initial landing page has already been promoted:
        - Add a note about how the virtual event is collecting registrations.Suggestion: `We’ve gone virtual! Please register the virtual event page [link to new page] to register for this event.`
        - Remove the form
    - If you want to keep the same landing page URL as the offline event:
        - Delete the landing page from the newly created program
        - Move the old landing page into the new program
        - Update the page copy with virtual event details
    - If the landing page has not been promoted: Unapprove the landing page.
    - When the new campaign is completely set up (landing page, Zoom, etc), use the `Reminder Email` smart campaign from the original event program to send a note to those that have already registered with directions about how to re-register for the virtual event.
- **Salesforce MPM tasks (applies to both types):**
    - Change the existing SF campaign to status: `aborted`

#### When Offline Field Events & Conferences turn to Virtual

These will be Virtual Sponsorships with booths (not just a sponsored webcast).

- MPM: if Epic wasn’t created, create the epic with the [Virtual Event Epic code.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#step-2-mpm-will-create-the-virtual-event-epic) If Epic was created, change it to the Virtual Event Epic Code.
- FMM/CMM: let interested teams know that the event has switched to digital and sales-nominated flow has been stopped (if applicable).
- MPM: Add [Virtual] to the beginning of the epic and all supporting issues.
- FMM/CMM: Add a note to the epic and the main issue: `This event has changed from live to virtual. Original date [2020-xx-xx], New Date: [2020-xx-xx], This [epic/issue] reflects information for the virtual event.` Date can be left off if it is the same.
- FMM/CMM: provide new campaign tag (if applicable).
- MPM: Create new marketo program under `Virtual Events` cloning `YYYYMMDD_Vendor_VirtualConfName (Virtual Conference Template)` template and save to the `Virtual Sponsorships` folder. If you are using the same campaign tag as the live event, add "OLD" to the beginning of the existing Marketo program as Marketo will not allow two programs with the same name.
- MPM: create a new folder in the `Virtual Sponsorships` folder for the virtual event.
- MPM: create new SF campaign by syncing new marketo program to SFDC.
- MPM: update links in Epic to new SFDC and Marketo links. Update the utm if applicable.
- MPM: follow standard processes with tokens, program activation, etc.
- MPM: For conferences: update event YML with event’s virtual details (change location to virtual, update copy dates if needed)
- **Existing event Marketo program MPM tasks:**
    - Move the existing event marketo program under the new folder you have created and add `OLD` at the beginning of the title. If the original Marketo program wasn’t being used yet (no invites, no members, only linked to SFDC) it can also be deleted or moved to an archived folder.
    - Deactivate the `Interesting Moments` smart campaign.
    - Deactivate any invitations (ex: sales-nominated) smart campaigns that are live and stop all promotions.
- **Salesforce MPM tasks (applies to both types):**
    - Change the existing SF campaign to status: `aborted`

### Cancellation of Offline Events

Once the DRI has identified that an event is cancelled, the **FMM** will:

- Update the event issue with [Cancelled] in the event title.
- Tag the MPM and relevant internal contacts for the event in a comment to notify them that the date has changed.
- Close the main Field/ Corporate Marketing event issue.
- Remove the event line item from the regional tab
    - FMM DRI will add requested refund info directly [into field budget document](https://docs.google.com/spreadsheets/d/1QC6P0VRWwJheOlGB-9bX8JIF8_4UY3h1cGVT_gacv5M/edit#gid=1753355316&range=A2).
    - See [instructions](https://gitlab.com/gitlab-com/finance/-/issues/2287) on how to technically obtain a refund with finance.
    - For Corporate Events please track cancellations refunds in [this doc](https://docs.google.com/spreadsheets/d/1WVWZjSF6f5jAFqHO4hXcV8mN975ITT4eXScSn0F_FU8/edit#gid=1109485360). Only add events where we will be getting money back from a vendor at this time.
- For owned events, work with the MPM to draft a cancellation notice to registered attendees.
- Remove the calendar invite from the internal `Sponsorships and Events` calendar

The FMC will:

- Remove the event from our external facing events page - about.gitlab.com

If an offline event is cancelled after the MPM set up the issues, epic, and tracking for the event, the **MPM** will:

- **Field Event and Conference:**
    - SFDC: Change the event campaign status to `Aborted`. No other changes.
    - Marketo: No changes
    - GitLab: Comment in related issues and slack channel that the event was cancelled and close the issues. Update the epic with [Cancelled] in the event title.
- **Owned Event:**
    - SFDC: Change the event campaign status to `Aborted`. No other changes.
    - Marketo: Update landing page to reflect the cancellation and remove the form. Create a cancellation email for registered attendees.
    - GitLab: Comment in related issues and slack channel that the event was cancelled and close the issues. Update the epic with [Cancelled] in the event title. Work with the FMM/ DRI to send a cancellation notice to registered attendees.

#### Event Types

For a full breakdown of our various types of events, please review [this list](/handbook/marketing/marketing-operations/#campaign-type--progression-status).

#### Important Planning Note
{:.no_toc}

The above planning list is not exhaustive - see planning issue template in field marketing project for most up to date list of tasks.

### How We Decide Who Attends Which Events?

- The event DRI determines how many staffers we need at the event and is responsible for ensuring the staffers are all set to attend the event.
- If the event is more enterprise-focused we try to send more marketing/sales. Regional Sales Managers in partnership with FM select team members based on who has the most potential contacts in the area or going to an event.
- If the event is more user-focused we will lean towards sending more technical people to staff and fewer sales.
- Suggestion for staffing: Field Marketing will evalute GitLabbers who live in the area that might be a good fit for the audience.
- We lean towards those who might be thought leaders, specialists, or more social for a specific show - i.e. if we are sponsoring an AWS show, we would like for a GitLab + AWS expert to staff the event.
- We aim to bring minimal team members to keep costs and disruption to normal workflow low. We take into account what value everyone will provide as well as coverage balance. Please check with the event DRI if you would like to or would like to suggest someone participate in an event.
- Once you have agreed to attend an event, you are not able to back out unless there is a customer facing obligation you need to tend to. We have this in place to avoid unnecessary rework on the event DRI’s behalf.
- A lot of times a technical sales resource needs to also be assigned to attend an event. In order to do so, please review the SA handbook for [instructions](/handbook/customer-success/solutions-architects/#when-and-how-to-engage-a-solutions-architect) on how to secure one of our awesome SA's. Tag the meta issue with technical-staff::required and once staffing attained change to label technical-staff::complete.
- All those attending will need their manager's approval.
- If you have been approved by the DRI and your manager to help staff an event, all your travel will be included during the time for the event/ expo days. You need to be onsite and ready to help out as soon as the first expo hall shift opens up and you may book travel any time after the expo hall closes. We will cover the night of lodging before the expo hall opens through to the night it closes. Any additional nights will need to be covered by the individual.
- Event staffing list will close 2 weeks for **field** events or 3 weeks for **corporate** events before commencement of the event.
- If you are not officially involved in the event as part of the sponsorship, we would still like to know you will be attending so we can include you in any activities surrounding the event. Please comment in the event specific slack channel notifying the Field or Corporate Marketing team of your plans to attend after obtaining approval from your manager. Please add a comment in the Event epic as well.

### Requesting Technical Staffing

Some events require technical staffing (Solutions Architects and/or others from the Customer Success team). To request technical staffing, do the following:

- Add the label `technical-staff::required` to the meta issue - this will make it appear on the [Event Technical Staffing board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1254947)
- Add a note in the staffing section how many people you need and any particular criteria
- Add one or more of the `technical-staff-type` labels to the issue to indicate which group(s) might be able to send staff. For example, if the event is on the west coast of the USA, consider adding `technical-staff-uswest` and `technical-staff-commercial`
- CC the SA manager for the group(s) indicated
- Once the staffing is solidified, change the label to technical-staff::complete so it no longer appears on the event staffing board(s)

### COVID-19 Event Outreach

It is important that we are communicating with our customers and prospects that we are monitoring COVID-19. Please include the following in all email outreach for hosted events.

- GitLab is taking extra precautions to ensure the safety of our attendees including adopting a no-handshake policy at the event and is asking attendees to practice respiratory hygiene (wash hands often and cough into your elbow instead of your hand).
- Additionally, we are requiring that all attendees who may have traveled to China, South Korea, Japan, Singapore, and Italy in the weeks leading up to the event only attend the event, if more than 14 days have passed between the time they join us onsite and the time they left any of these areas, and that they have shown no flu-like symptoms in that time.
- For the safety of everyone, we ask that anyone who is feeling ill skips the event and substitutes a colleague in their place. We apologize to those attendees whom this affects and hope you understand that we are enacting measures we feel are necessary to ensure the safety of all attendees. Thank you in advance for taking these precautions and we look forward to welcoming you to our event.

### Event Outreach

It is important that we are communicating with our customers and prospects when are attending an event. This is done through a targeted email sent through Marketo & also through SDR & SAL outreach.

- Receive attendee list and contact customers and prospects before event using talking points provided by content DRI with the goal of setting up meetings/ demos at the event. Invite them to anything specific we have happening at or around event.
- If there is not an attendee list process is as follows:
- Target speakers for outreach.
- Utilize previous years attendee list that can be found in SFDC. That person may not be attending, but their colleague might be. Ask for intros.
- Follow event hashtags to see who will be attending.
- Join local meetup and pre event events (this works well for large events like AWS).
- Join LinkedIn Groups and slack channels dedicated to event.
- Download event app and engage with attendees via app.
    - You can search for relevant talks and see who has registered for talks that might apply to potential customers.

#### Email alias usages for outreach

We use several email aliases for pre and post event outreach. Below are guidlines as to which one should be used based on the event

- Corporate events (all communications): `info@`
- Field marketing events (invitations and follow up emails): `info@`
- Field marketing event day before reminders: `fieldmarketing@`

#### Pre-Event Registration Tracking & Reporting

As propects register for the event the Field Marketing team has the ability to track any potential dietary concerns:

- [LEAD report](https://gitlab.my.salesforce.com/00O4M000004dr8v)
- [CONTACT report](https://gitlab.my.salesforce.com/00O4M000004dr95)

If the FM DRI for an event needs to reach out to someone prior to the event, they will do so leveraging their personal `@gitlab.com` email address & cc `fieldmarketing@` alias.

The `Dietary Restrictions` field can be leveraged as a list filter by the MPMs if they are needing to send a bulk email of any kind.

#### For Field Events

- Adding Records to the Campaign in order for the record to receive an invite to the event:
- SALs/SDRs should add members to campaign to be invited to event, using the appropriate `Campaign Status`:
    - `Sales Invited` = You have personally invited the person to the event.
    - `Sales Nominated` = Marketing will invite the person on your behalf. They will receive invitation email pre-event plus any confirmations/reminders if they register. [Video training on how add is also available.](https://drive.google.com/open?id=1QNB3DXXWtnmMBvzeHPkT7cRmigJdIWq5)
    - `Marketing Invited` = Marketing will be sending geo-targted invitation emails to specific events pulling the names from our active database. Anyone included in this master send will be added to the campaign with this status.
    - **Any other Status** = Do not assign any other status to records. The campaign members will be updated by MPM or automated through registration.

#### Employee Booth Guidelines

- Perfect your Pitch
    - Most people have two ears and one mouth. Successful pitching is two parts listening to one part talking. Be engaged and interested in folks that visit. Be genuinely curious about their story. Understand them first before you start telling them about us.
    - A great way to start is to offer a handshake and say,
        1. "Hi, my name is [your_name]”
            1. “Hi I’m [their_name]”
        1. “[their_name, are you familiar with GitLab?”
            1. “Yes”
                1. “Thanks for using us, are you using just the source code management or are you also using the built-in GitLab CI/CD?”
            1. “No” - “Great, in a nutshell we <140 character description> - for example, what company do you work for?”
                1. “Company X”
                1. And what’s your role at company x?
                1. “Title X”
                1. Tailor your pitch to their specific experience. Ask about what tools they are using today, what they like or dislike about those tools.
    - Working at the booth is a great place to try out different ways of explaining technology and trying out different value propositions to see what resonates the most.
    - Know some stock answers
    - You can ask them which talk they’ve heard so far has been the most interesting.
- Close the deal
    - Figure out what the next step for this person is. Are they a decision maker at a large org?
    - If a conversation is running long, get their info and schedule a time to chat or follow up at a later time outside of the booth. The goal of the booth is to make initial contact and connections.
    - Give a personal follow up
- Stand at the front of booth facing the crowd.
    - Don’t make folks walk into the booth and seek you out. Stand out at the front. Make eye contact and smile at folks walking by. If they stop or pause you can ask them, “Are you familiar with GitLab?”
- Hangout outside the booth.
    - Too many GitLab team-members in the booth discourages other folks from coming by. The booth can be a great place to meet up, but don’t hang out there. Move the conversation to a near by lounge or social area.
    - When you are at the booth keep conversation with your coworkers to a minimum.
    - When you are at the booth focus on serving the attendees.
    - Do not do normal daily work in the booth - the booth is not a place for taking calls, or responding to emails. When you are at the booth you are on booth duty and that is it.
- Be prepared to schedule follow up meetings
    - To make it easier for our prospects and customers to continue conversation initiated at the event (considering all of the vendor follow-ups and the need to catch up on missed days of productivity following events), please have your computer handy (in a safe and secure location within the booth) to schedule Initial Qualifying Meetings when appropriate.
    - Some example verbiage to facilitate this would be: “Let's schedule some time to follow up on your conversation with us today in a setting that is a little less hectic so we can answer any questions that you may have”
- Keep an abundant tidy stash of swag out
    - During slow times, restock swag and tidy up booth.
- Keep the booth clean.
    - The booth should be clean and organized at all times.
- Avoid eating meals in booth, please keep lids on beverages and out of sight.
- If we have the bandwidth or the traffic is slow do not be afraid to walk around to other booths and talk to people. Make friends we could partner with, create interesting content with, or just have friendly beers.
- If press comes to the event feel free to put them in contact with CMO.
- Engage the competition.
    - Be friendly and polite to competitors to come by the booth.
- Don't forget your business cards.

#### Scanning Best Practices

- Be an active but polite Badge Scanner
    1. Don’t reach for the badge without first asking if you can scan someone’s badge and don't lead a conversation with can I scan you. Ask folks politely, “Would you appreciate a follow up email?” or “Mind if I scan your badge?” Many folks will say, “yes.” If they say, “Not really.” You can say, “Great, we don’t want to clutter up your inbox. You can always go online to about.gitlab.com if you’d like to check back in with us.”
    1. Trade scans for all swag if the attendee consents. If we have the space and staff, someone should be in charge of distributing and organizing swag, and scanning folks who come by for swag.
- Take good notes:
    1. Your initials (This way the email can be more personalized to say, “We saw you chatted with [name] in the booth.”) Add your initials + “follow up” if you personally can send them an email within a week of the conference.
    1. The tech stack (what tools they are using)
    1. Any specifics needed in the follow up (schedule a call, send docs for X, interested in Y, etc.)

##### Suggested Attire

- Wear at least one piece of branded GitLab clothing. If you prefer to wear something dressier than the GitLab branded items available that is also acceptable. Feel free to wear our sticker on your clothing.
- If the conference is business casual try some nice jeans (no holes) or dress pants.
- Clean, closed-toed shoes please.
- A smile.

##### Booth Set Up

- Bring:
    - Generic business cards
    - Stickers + any other swag
    - Events laptop (for slideshow) + charger + dongles
    - Backup power banks
    - Mints & hand sanitizer
    - One pagers + GitLab cheat sheets

##### Quick Booth quality check

- Check on cleanliness of booth
    - Monitor(s) there and in right place
    - Printematerials in good supply and in right places
    - Are badge scanners and other rentail items accounted for?
    - make sure swag is orderly
    - remove any personal device son booth surfaces?
    - renmove any open containers or food waste?
    - Do we have too many or not enough people staffing?
- send pic of booth to be sent out on social

##### Day of Booth Staffing

- Ideally booth shifts will be around 3 hours or less.
- Staff more people during peak traffic hours.
- Avoid shift changes during peak hours.
- Aim to staff the booth with individuals with a variety of expertise and backgrounds- ideally technical and non-technical people from various departments should be paired.
- Send out invites on the Events & Sponsorship calendar to booth staff with the following information:
    - Time and date of event, booth, and shift
    - Any instructions on using or locating lead scanner
    - Any relevant event set up or clean up

#### Post Event

- Add event debrief to event issue in marketing project. The debrief should include the following if applicable:
    - Was the event valuable?
        - Would you go again? Should we go again?
        - Did we get good leads/contacts? What was the audience profile like?
        - Best questions asked and conversations. Trends in questions asked.
        - Was our sponsorship/involvement successful? Did we go in at the proper sponsorship level?
    - How was the booth set up?
        - How was the booth staffing?
        - Did the booth get enough traffic?
        - Booth location and size
    - How did our swag go over?
        - Did we have enough/too much?
    - Contests
        - Did the contest(s) effectively build our brand and connecting with our target audience?

#### Event List

1. List received by event DRI from event organizers
1. Event DRI reviews and cleans up list following the guidelines for [list imports](/handbook/sales/field-operations/gtm-resources/)
1. List is sent to Marketing Ops for upload to Marketo & associate to related Campaign (w/in 24hrs of receipt from event)
    - Marketo will match based on `Email Address` to existing records regardless if LEAD or CONTACT object.
1. Marketo will sync to SFDC automatically. LeanData assigns records based on Territory ownership.
1. Marketing Ops will create a LEAD & CONTACT view in SFDC for the SDR team to facilitate follow up.
    - LEAD/CONTACT views will be retained in SFDC for no more than **90 days**
1. Marketing Ops notifies MPM/FMM in the list clean issue when the upload has been completed so the follow up email(s) can be scheduled.
1. Marketing Ops makes a post on the `#sdr_global` slack channel with a link to SFDC campaign, link to SDFC LEAD & CONTACT view, and link to Outreach view (if exists).
    - Ops will ping the `sdr` slack alias and cc the `@sdr-leadership` slack alias.
1. Event DRI follow up on leads to make sure expected followup completed and lead status changed.

Common lead questions:

- Record ownership will be assigned using the [Global Ownership](/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement) rules
- All followup needs to be tracked in SFDC
- List upload needs to be done **before** follow up is done so we can ensure proper order of operations & attribution is given correctly
- Record Owner and/or SDR doing follow up need to be sure to update the [`Contact Status`](/handbook/sales/field-operations/gtm-resources/#lead--contact-statuses) on the record as follow up is done.
- Campaign type & meaning of [Campaign Member status](/handbook/marketing/marketing-operations/#campaign-type--progression-status) can be found in the Marketing Ops handbook

#### Onsite registration form & landing page

For **FIELD** events that don't have badge scanners onsite, the FMM event owner can request an onsite registration form and landing page which can be used to collect names at the booth. The landing page (including the form) can be accessed by a unique URL via iPad, mobile phone or laptop and should only be used by staff at the booth and deactivated post-event by Marketing Ops.

**How to create an onsite registration form**

- FMM to request the creation of the onsite registration form & landing page via the MPM checklist (YES/NO/MAYBE) on the field event issue template. This form can be utilised ONLY when there are no badge scanners available onsite.
- MPM to create issue under the event epic, using the [Onsite Registration Form & Landing Page](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-08-onsite-reg-form-lp) template.
- MPM to navigate to the Marketo program for the event and find `No Lead Scanner - Onsite Reg` landing page under assets. Update the URL to be relevant to the event.
- MPM to navigate to the Marketo program for `03 No Lead Scanner - Onsite Event Reg` Campaign and check that Smartlist and Flow are set up correctly for the relevant landing page. MPM then to **ACTIVATE** this campaign.

**How to use the onsite registration form**

- FMM to share the landing page URL with booth staff (and with booth staff only).
- GitLab reps at the booth can use the landing page & form to add the details of booth visitors that they had a conversation with.
- Depending on whether GitLab reps added or didn't add notes on the notes field of the form, then the program member enters the Salesforce campaign as indicated below:
- 
    - If the Notes field has notes > campaign status = `Follow Up Requested` > lead status = `MQL`
- 
    - If the Notes field is empty > campaign status = `Visited Booth` > lead status = `Inquiry`
- Booth staff will have 24hrs post-event to go to the event salesforce campaign and add any further notes they have about a campaign member.
- Once the event has ended, MPM should notify Marketing Ops via the Onsite registration form & Landing Page issue to de-activate the form and landing page.

#### Marketo Check-in App

For **OWNED** events, the Marketo program is pushed to the Marketo Check-in app that allows the FMM team to be self-sufficient when it comes to updating an event registration list.

**User Experience Flow (Registration & Check-In) With System Actions**

- Individual registers for the event via landing page (Marketo form)
- [system] individual added to the Marketo program and Salesforce campaign as `Registered`
- Individual attends the event and FMM checks them in using the app
- [system] when FMM syncs the data to Marketo, individual is flipped from `Registered` to `Attended`
- [system] automation is set to automatically flip anyone still in `Registered` status to be `No Show` 12 hours after the event

⚠️ **Important! FMM must check-in all individuals and sync to Marketo within 12 hours of the _start time_ of the event so that automation can flip the correct records to `No Show`.**

##### How to activate the Marketo Check-in App

1. MPM to navigate to the Marketo program event
1. At the top of the page, change `View: Summary` to `View: Schedule`
1. You should see the event on the right side of the calendar
^ Events are displayed intially based on the program <b>`created date`</b>, not the event date
1. Double click your event, update `date`,`end`, and event times (remember all times are based on the MPM's timezone setting in Marketo)
1. Click the slide bar to change event from `tentative` to `confirmed`. Slide bar will change from _gray_ to _orange_ when done correctly.
_The event will not be visible in the Marketo Check-in app until 7 days prior to the event_

##### Using App for Event Check-in

_If FMM is unsure they will have internet at venue, it is very important to fully sync program while connected to the internet before event._

1. Sign into the Marketo app.
1. Locate the event from the list displayed, press the blue "Sync" button that appears across the top menu bar.
    - This will ensure all late registrations are fully synced to the app
1. All program members with `Status` = `Registered` will show in the list on the left side of the app.
![](/images/handbook/marketing/events/marketo_checkin_sync.jpg)
1. As people arrive, search using the search bar or scroll alphabetically by `Last Name` through the list.
    - You could also have the iPad available for people to check-in themselves
1. Select person to check-in & press the large blue button that says "Check-in"
![](/images/handbook/marketing/events/marketo_checkin_personselect.jpg)
    - If the person's contact details need to be updated click "edit" (top right) and make changes.
    - When the program re-syncs the contact details will be updated as well
1. To **ADD** a person to the event at the door, click the "person+" button in the bottom left corner of the app.
![](/images/handbook/marketing/events/marketo_checkin_addperson.jpg)
    - Blank form will appear
    ![](/images/handbook/marketing/events/marketo_checkin_newattendee.jpg)
    - All fields are **required** except `Phone`
    - Click green "Done" in top right corner
    - Complete checkin by clicking blue "Check-in" button
1. As people are checked in there will be a red numeric icon over the "Sync" button, this will remain until the app is synced to Marketo
![](/images/handbook/marketing/events/marketo_checkin_checkedin.png)
1. Once event is over, FMM presses blue "Sync" button to send all the records to Marketo
1. The Marketo program will immediately be updated with `Campaign Member Status` = `Attended` and the sync to SFDC will begin
1. Notify the Ops team via List clean & upload issue to verify status of records & request the remaining `Registerd` -> `No Show` status
1. Ops will verify Bizible attribution has been applied correctly.

If you need support, use the #mktgops slack channel.

## Swag for Events

Swag selection and creation is managed by Corporate Marketing. All information on swag can be found in the [Corporate Marketing handbook](/handbook/marketing/corporate-marketing/#swag). For event related swag and tracking purposes, we utilize the tags `swag_corporate` and `swag_community`.

## Specifics for Community Relations

GitLab's Community Relations team reviews and manages our community events (typically an event with less than 250 attendees that is targetting end users of GitLab) including [Meetups](/handbook/marketing/community-relations/evangelist-program/#meetups). The [Community Relations handbook](/handbook/marketing/community-relations/evangelist-program/#community-events) has additional information on the events the Community Relations team will support.

## Specifics for Corporate Events

- See [Corporate Events Handbook](/handbook/marketing/corporate-marketing/#corporate-events)

## Specifics for Field Marketing Events

- For events where a field marketing representative cannot be present, the FM DRI will assign an onsite lead. The DRI will be responsible for coordinating with this person and providing them with any info they will need to help run the event in their absence. This person will be the venue point of contact as well as responsible for set up and tear down.
- FOR EMEA: We must ensure we are gathering GDPR compliant leads - Lead devices scanning follow up needs to be in event T&C. If GDPR is not in the T&C, we are not allowed to follow up on the leads. Scanning a lead is not automatically GDPR compliant if visitors have not agreed to it.

### Field Marketing Campaign Tags

For Field Marketing campaign tag details, please visit the Field Marketing page [here](/handbook/marketing/revenue-marketing/field-marketing/#field-marketing-campaign-tags).

### Field Marketing Swag and Event Assets

For all details regarding Field Marketing Swag and Event Assets, please visit the Field Marketing page [here](/handbook/marketing/revenue-marketing/field-marketing/#field-marketing-swag).

### AMER Field Marketing QR Codes for Events

A QR code with a general GitLab PathFactory track and a QR code with a general PubSec PathFactory track are available for use. FMMs/DRIs can follow the links below to print and display at events. Table-top signs to display the QR codes are available in Nadel - search for `Tabletop Display`.

- [General track QR code](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/uploads/4e60a958192f8d3e336d1d963bfbe551/General-Track-Qr-Sign.pdf)
- [PubSec track QR code](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/uploads/3538c3694620b1d8c1a4e3ec99427b00/gitlab-pubsec-events-path-factory-qr-code-8_5x11in.pdf)

Field Marketing is able to create additional QR code signs using a template by following instructions [here](https://docs.google.com/presentation/d/1H36fbkRxyPLMWJ5xqKge4yINJQlUsKZ0tuGujCf7-fA/edit?usp=sharing). Please be sure to make a copy of the file rather than editing directly.

If this template does not meet your needs, additional designs may be requested by opening an issue in the corporate marketing [repo](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/). Be sure to include:

- what the sign is for
- verbiage for the sign
- URL to attach to the QR code
- whether or not the URL needs tracking
    - If YES and the QR code is for a specific event, pull the existing UTM from the related event epic and attach to the end of the URL that needs tracking
    - Please loop in your MPM for assistance with campaign UTMs, or `@mnguyen4` & `@shanerice` if your MPM is not available

Design will then create and provide a PDF for the FMM/FMC to print on their own for the event.

### AMER Field Marketing Badge and Event Check-In Process

**Items to Purchase:**

- [Badge Carrying Case](https://www.pcnametag.com/redi-reg2-badge-case-orreg.html?gclid=EAIaIQobChMI8vnJmoik5QIVGJSzCh3Gxw9oEAQYBSABEgIQZfD_BwE)
- [Avery Badges with Clear Holder and Badge Clips](https://www.avery.com/products/namebadges/74541)
- [Speaker Ribbons](https://www.namebadgeproductions.com/product/4648/4-stick-n-stack-horizontal-ribbons)

**Marketo Check-in App on iPad:**

- Please review the [Marketo Check-in App Instructions](/handbook/marketing/events/#marketo-check-in-app)
- In order to use the Marketo Check-in App, you must have a wifi connected device.
- Before the event, use your personal Marketo login to log in to the Marketo Check-in App on the iPad and make sure it is working properly and that your event is displayed. NOTE: Your event is pulled into the app 7 days before the event begins and will not show up before this time.
- The Marketo Check-in App will pull attendees in alphabetical order unless the first letter of the attendee’s name is not capitalized. Those attendees will show up out of order at the very bottom of the list of names. As a result, it is often faster to check attendees off as attended in a spreadsheet as they come in (see below instructions) and then check them in on the Marketo App after registration.
- You must use this process for GitLab owned events. MktgOps will NOT complete a list upload as FMM is supposed to use the Marketo Check-in App.

**Creating Event Badges:**

**SFDC**

- Log into SFDC, click Reports and search for `Name Tag Export` in the `Field Marketing` folder, or go to this [link](https://gitlab.my.salesforce.com/00O4M000004aIM0).
- Update `Select Campaign` by clicking the lookup next to the field
- Find the campaign that has members you need to export
- Click `Run Report`
- This generates a report with everyone who has registered for the event.
- Click `Export Details`
- Under the `Export File Format` field, select either `Excel Format` or `Comma Delimited` (whichever works best on your laptop)
- This will create a downloaded file of registered members. From here, copy and paste into a Google sheets document and make any corrections to capitalization, spelling errors, etc.. This document will be utilized when creating the badge template so remember to also filter alphabetically by last name. If you will be checking off attendees utilizing this spreadsheet, remember to add a check-in column as well.

**Avery Template**

- Go to avery.com and log in with the events login found in the Marketing 1pass
- Under `Templates` select `Avery Design & Print`
- Click `Start Designing`
- Under `Quick Search`, type in the badge number found on the box of your previously purchased Avery badges (link provided in `Items to Purchase` above) - Avery 74541
- Select either one-sided or two-sided (if you intend to print on the back of the badge) and click `Select this Template`
- At the top of the page select `Apply from a Saved Project`
- Click `Select and Customize` on the horizontal orientation option
- Select the `GitLab Connect` template option with our standard Tanuki logo and click `Apply this Design`
- On the left side of the page, click `Import Data Mail Merge`
- Select `Replace Spreadsheet` and `Browse for File`
- Select your previously downloaded event spreadsheet and click on the fields you would like to include in the badge template (generally first name, last name, organization). Remember to unclick the first row if it contains your headers.
- Click `Next` and then drag and arrange fields to your preference and click `Finish`
- The top right `Navigator` section will allow you to format and make changes to all badges or individual badges
- Format badges accordingly and check all badges for any errors. If you have not purchased a speaker ribbon and would like to indicate speakers on individual badges, this is the stage where you can add details as needed.
- When ready, click `Preview and Print` and `Print it Yourself`
- If everything looks correct, click `Get PDF to Print`
- Remember to save your design to the account or your personal computer
- Click `Open PDF` and print utilizing the Avery badge paper previously purchased
- Separate individual badges, stuff in clear holders/clips that come with the badge paper, and arrange in badge carrying case

### Field Marketing Venue Search

For venue search requests, the FMM can open an issue utilizing the [`Venue_Search`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=Venue_Search) template. Follow the instructions to provide event details and assign to the regional FMC.

### Lightning meetings with CXO

At GitLab owned events we will sometimes host 10 minute lightning meetings with the GitLab CXO(s) in attendance. We do this to facilitate many interactions with our leadership in a short amount of time.

#### Process

- Written & verbal announcement is made to audience either prior to the event in the Know before you go email or the announcement is made onsite at the event.
- The location should be clearly communicated to the audience as well as the 10 min time limit.
- The DRI for these meetings is responsible for writing down the attendees name and company as attendees arrive to form the queue of folks who will be meeting with the CXO.
- This should be done in the shared google sheet event briefing in a separate tab.
- The DRI can either stay in the room for the duration of the meeting or can step out to continue checking other attendees in.
- With 8 mins left, the DRI should begin to bring the meeting to a close by sensing the flow of the conversation and politely interrupt stating there is time for 1 more question.
- At the 10 minute mark, the DRI will politely cut the meeting off.
- The DRI walks out the attendee and then brings in the next.
- This process continues until the time limit has been reached.

#### Space considerations

- If possible there should be a location separate from the meeting space where the attendees can wait their turn to meet with the CXO.
- Consider offering light snacks and beverages as people wait.
- Have CXO and meeting attendee on the same level, physically, vs. one party sitting higher or standing and vice versa.

#### Things to consider

- If people would just like to meet the CXO and don’t need to take the full 10 mins for the meeting that is great! The max time is 10 mins.
- There is no agenda for the 10 min meeting. The DRI for the meetings should quickly research the company via SFDC (if you don’t have the SFDC app on your phone, this is a great use case for why it’s useful!) gather as much info as quickly possible and relay this info to the CXO. As an example, if there is an enterprise customer next in-line to meet with CXO, let the CXO know. Short brief info, as you won’t have much time to relay this info.
- Notes should be taken by the DRI or a designated delegate in the room so the CXO can focus on the meeting. These notes should be added within 48 hours to the person record in SFDC by the DRI note taker. If any next steps were discussed and an action was assigned to someone not in the room, be sure to tag the person who was assigned!

## How to add events to [about.gitlab.com/events](/events/)

In an effort to publicly share where people can find GitLab at events in person throughout the world, we have created [about.gitlab.com/events](/events). This page is to be updated by the person responsible for the event. To update the page, you will need to contribute to the [event master.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/events.yml).
If you need more information about our exact involment in an specific event please visit the marketing project in gitlab.com and search the name of the event for any realted issues. The "Meta" issue should include the most thorough and high level details about each event we are participating in. Place your event in the order in which it is happening. The list runs from soonest to furthest in the future.
Save event images and headers here: Save images for featured events [here](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/events)

### Details to be included (all of which are mandatory in order for your MR to pass the build):
{:.no_toc}

- **Topic** - Name of the event you would like to add
- **Type** - Please choose one of the following: `Diversity`, `Conference`,
`MeetUp`, `Speaking Engagement`, `Webinar`, `Community Event` or `GitLab Connect`. **Events cannot have more than one type.** If more than one apply, choose the best. If you feel your event doesn’t fit in the below category, do not just manually add a type. Please reach out to events@gitlab.com to suggest a new type of event.
- **Date starts** - Day event starts
- **Date ends** - Day event ends (For a single day event, please put the date in both the `Date starts` and `Date ends` section)
- **Description** - Brief overview about event (can be taken from event homepage).
- **Location** - city, state,provinces, districts, counties (etc depending on country), country where event will take place
- **Region** - `AMER`, `LATAM`, `EMEA`, `APAC`, or `Online`
- **Social tags** - hashtag for event shared by event host
- **Event URL** - homepage for event

#### Example
{:.no_toc}

```
- topic: The Best DevOps Conference Ever
  type: Conference
  date_starts: January 1, 2050 # Month DD, YYYY
  date_ends: January 3, 2050 # Month DD, YYYY
  description: |
               The Best DevOps Conference Ever brings together the best minds in the DevOps land. The conference consists of 3 full days of DevOps magic, literally magic. Attendees will have the opportunity to partake in fire talks and moderated sessions. This is one you won’t want to miss.
  location: Neverland, NVR
  region: APAC
  social_tags: DEVOPS4LIFE
  event_url: https://2050.bestdevops.org
```

#### Template
{:.no_toc}

```
- topic:
  type:
  date_starts:
  date_ends:
  description:
  location:
  region:
  social_tags:
  event_url:
```

For featured events include:

```
featured:
    background: background/image/src/here.png
```

## Creating an event specific landing page

**All landing pages** require the involvement of a Marketing Program Manager (MPM) as there are required steps to set up programs & tracking in both Marketo/Salesforce to correctly manage inbound submissions from the landing page.

For corporate tradeshows we will want to create an event specific page that links from the [about.gitlab.com/events](/events/) page. The purpose of this page is to let people know additional details about GitLab’s presence at the event, how to get in touch with us at the event, and conference sessions we are speaking in (if applicable).

For select Field Marketing events, that meet the critera below, a Marketo landing page is used instead of an `events.yml` created landing page. By doing this, the MPMs own the creation of these pages and they are the only ones who will have edit access to these pages.

When to specifically use a Marketo landing page vs. the events yml:

1. This is an event owned by Field Marketing.
1. The event cost the company less than $50,000 (or your country's equivalent).
1. We will be driving traffic to the marketo landing page for less than 1.5 months.

Steps to take to create the new `events.yml` generated landing page:

1. Create new a new branch of the [www-gitlab-com project.](https://gitlab.com/gitlab-com/www-gitlab-com). - Branch name should be what event you’ve added.
1. From new Branch, navigate to `Data`, then to `events.yml`
1. Scroll down to the area where its date appropriate to add the event
1. Add event using instructions in [handbook](#how-to-add-an-event-to-the-eventsyml)
1. To create the event specific page you need to add a subset of the following information:

- **url:** - you make this up based on what you want the URL to be from about.gitlab.com
- **header_background:** choose from an image already in the images folder or add your own image. If you do not know how to do this, please watch [this tutorial](https://drive.google.com/open?id=14wCjHZLbcUUDArGYBNeinsHAlET6ubMy).
- **header_image:** choose from an image already in the images folder or add your own. (optional- if you prefer not to include, remove field altogether)
- **header_description:** what CTA would you like the person to do on the page
- **booth:** booth number at event, if there is no booth number, then remove this line of code (optional)
- **form:** code that tells the system to add the contact info form. Marketing Ops will provide you with this number. They need to create a specific form for each page associated with a campaign in sfc.
- **title:** CTA for why someone would want to give their contact info. Also used in `contact:` to distinguish a header title.
- **description:** additional info on why someone would want to give their contact info
- **number:** Marketo form number - Marketing Operations will need to give this number to you. Under `form:`
- **content:** all of the information in the example section is all optional based on your event. If its not needed, simply delete. "Content" block to take markdown.

1. Please watch [this tutorial](https://drive.google.com/open?id=14wCjHZLbcUUDArGYBNeinsHAlET6ubMy) for additional help.

### Example
{:.no_toc}

```
- topic: AWS re:Invent
  type: Conference
  date_starts: December 2, 2019
  date_ends: December 6, 2019
  description: |
             AWS re:Invent 2019 is the Amazon Web Services annual user conference dedicated to cloud strategies, IT architecture and infrastructure, operations, security and developer productivity.
  location: Las Vegas, NV, USA
  region: AMER
  social_tags: AWSreInvent2019
  event_url: https://about.gitlab.com/events/aws-public-sector-summit/
  # Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url.
  # If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.
  url: aws-reinvent
  header_background: /images/blogimages/gitlab-at-vue-conf/cover_image.jpg
  header_image: /images/events/aws-reinvent.svg
  header_description: "Drop by our booth to see a demo and speak with our GitLab experts!"
  booth: "S1607"
  form:
      title: "Request a meeting!"
      description: "Let us show you how GitLab can impact your business."
      number: 1691
      success message: "Thank you for requesting to meet! We'll be in touch shortly with more information."
  content:
    - title: "Make sure to stop by the GitLab booth at AWS re:Invent!"
      body: "Speak with our experts and learn how GitLab simplifies your deployment pipeline to accelerate delivery by 200%. See a live demo, learn about our latest releases, and explore what’s on the roadmap for GitLab."
    - title: "Request a meeting"
      body: "Fill out the form to request a meeting with GitLab. We'll share how we can impact your business as a complete DevOps platform, delivered as a single application. From project planning and source code management to CI/CD, monitoring, and security."
    - title: "Activities at AWS re:Invent"
      list_links:
        - text: "Register for our happy hour!"
          link: "#"
        - text: "Join our speaking sessions - see details below."
  speakers:
    - name: "Priyanka Sharma"
      title: "Director of Technical Evangelism"
      image: /images/team/priyankasharma-crop.jpg
      date: "Wednesday, May 22"
      time: "14:00 - 14:35"
      location: "Hall 8.0 F5"
      topic: "[The Serverless Landscape and Event Driven Futures](https://kccnceu19.sched.com/event/MPeI/the-serverless-landscape-and-event-driven-futures-dee-kumar-cncf-priyanka-sharma-gitlab?iframe=no&w=100%&sidebar=yes&bg=no)"
      description: "Serverless design patterns have grown in popularity amongst developers and enterprises alike and the ecosystem is exploding. Developers like moving faster by focusing on business logic without worrying about the underlying infrastructure. Today, there are umpteen solutions and OSS projects in the market and the space needs some organization to maximize effort. There is a lot of curiosity and confusion around serverless computing. What is it? Who is it for? Is it a replacement for IaaS, PaaS, and containers? Does that mean the days of servers are over? The CNCF created the Serverless Working Group to explore the intersection of cloud native and serverless technology. The first output of the group was creation of serverless landscape. The landscape lists some of the more common/popular Serverless projects, platforms, tooling, and services."
    - name: "John Jeremiah"
      title: "Director, Product Marketing"
      image: /images/team/johnjeremiah-crop.jpg
      date: "June 12, Wednesday"
      time: "1:25 PM–1:45 PM"
      location: "Partner Pavilion"
      topic: "Accelerating Speed to Mission: A Digital Transformation"
      description: "Velocity and speed of execution determine the winners. The faster your software teams can deliver, the bigger your advantage. The traditional software development processes have multiple layers of friction, checkpoints, and bottlenecks, often making simple projects complex, expensive, and lengthy. One of the hardest parts of delivering software is keeping everyone aligned and focused. Teams waste time waiting for inputs, fixing mistakes, shifting from one tool to another, waiting for infrastructure, and maintaining complexly integrated toolchains; this all creates friction and slows innovation. It doesn't have to be slow, learn how to accelerate your software delivery and increase your mission velocity."
```

### Template
{:.no_toc}

```
- topic:
  type:
  date_starts:
  date_ends:
  description: |

  location:
  region:
  social_tags:
  event_url:
  # Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url.
  # If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.
  url:
  header_background:
  header_image:
  header_description:
  booth:
  form:
      title:
      description:
      number:
      success message:
  content:
    - title:
      body:
    - title:
      list_links:
        - text:
          link:
        - text:
          link:
        - text:
          link:
  speakers:
    - name:
      title:
      image:
      date:
      time:
      location:
      topic:
      description:
    - name:
      title:
      image:
      date:
      time:
      location:
```

## Creating a Marketo Landing Page

[This has been moved to the Landing Page handbook page to focus on conversion best practices and add details for page creation](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/landing-pages)

## Email Process for Events

[Please see Emails & Nurture handbook page for in-depth details on email marketing](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture)

**Email Approval:**

1. MPM send yourself a test email and check that it is ready for DRI to review/approve
1. If email is ready for DRI approval, Click 'print', select 'Save as PDF' as your destination. Click 'More settings' and check the Options box for Background graphics, then save file (example - 20191016_Roadshow_NYC_reminder).
1. Add file to the description in your related issue (invitation-reminder or follow-up-email)
1. MPM to @ mention the DRI to let them know the email has been created and is in the description
1. DRI will review email for grammatical errors, event details, remove any gendered language, and links and provide feedback and/or approval.
1. DRI will provide @ MPM for any requested changes, and MPM will make update and follow the process again.

## Speaking at events

If you’re looking for information about speaking at an events head over to our [Corporate Marketing page](/handbook/marketing/corporate-marketing/#speakers) for complete details.

## Virtual Events

- See [virtual event best practices guide](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/best-practices/). Includes best practices for running a virtual event, tips for speakers, attendees, partners, and speakers.
