---
layout: handbook-page-toc
title: Threat Management sub-department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Threat Management engineering sub-department contributes to development in the [Sec section](/handbook/product/categories/#sec-section).

```mermaid
erDiagram
    Secure-Stage ||--|{ Static-Dynamic-Composition-Fuzz-And-Vuln-Research-Groups : contains
    Static-Dynamic-Composition-Fuzz-And-Vuln-Research-Groups }|--|| Secure-Department : reports-to
    Protect-Stage ||--|| Container-Security-Group : contains
    Threat-Insights-Group  ||--|| Threat-Management-Department: reports-to
    Container-Security-Group ||--|| Threat-Management-Department : reports-to
    Secure-Stage ||--|| Threat-Insights-Group  : contains
```

We are responsible for protecting applications, networks and infrastructure from security intrusions via giving users insights into threats at the vulnerability, network, container, operating system, and application layers. Our sub-department consists of all groups in the [Protect Stage](/handbook/product/categories/#protect-stage) and the [Threat Insights Group](/handbook/product/categories/#threat-insights-group) in the [Secure Stage](/handbook/product/categories/#secure-stage).

You can learn more about our approach on the direction pages for [Secure](/direction/secure/) and [Protect](/direction/protect/).

## Vision

Protect our users' applications, services, and infrastructure from the ever-evolving threat landscape.

### FY21 Vision

By the end of FY21:
* To give our customers increasingly better controls to protect themselves against attacks (and to help drive more revenue from more customers wanting these categories), we should successfully deliver on the maturity roadmap.
* All categories have customer use tracking enabled so we can accurately gauge customer acceptance.
* The (relatively new) Protect development team is achieving all metrics that all development teams track (MR's per month per engineer, developer to maintainer ratio, etc.)
* The sub-department has implemented one significant idea to improve the GitLab architecture overall (such as perhaps a framework and guideline for implementation of services).
* The sub-department publishes >=2 blog entries on security trends to inform our customers, prospects, and the security industry as a whole.

## Mission

Launch GitLab developed security technologies and integrate open-source projects to provide security controls for customers.  

Employ security controls for our customers at the container, network, host, and application layers.

Provide features to allow customers to manage their security risks effectively and efficiently.

For more details, see the [Secure stage](/stages-devops-lifecycle/secure/) and [Protect stage](/stages-devops-lifecycle/protect/).

## Sub-department development people leaders



<%=
departments = ['Threat Management']
department_regexp = /(#{Regexp.union(departments)})/

direct_team(role_regexp: department_regexp, manager_role: 'Director of Engineering, Threat Management and Growth')
%>
### Backend

#### [Secure Stage: Threat #Insights](/handbook/product/categories/#threat-insights-group)

<%=  direct_team(role_regexp: /Threat Insights/, manager_role: 'Backend Engineering Manager, Threat Management') %>

#### [Protect Stage: Container #Security](/handbook/product/categories/#container-security-group)

<%=  direct_team(role_regexp: /Container Security/, manager_role: 'Backend Engineering Manager, Threat Management') %>

### Frontend

<%=  direct_team(role_regexp: /Engineer/, manager_role: 'Frontend Engineering Manager, Threat Management') %>

### Timezones for each team member

[Timezone.io](https://timezone.io/team/threat-management)

## Performance Indicators

* [Threat Management Sub-department performance indicators](/handbook/engineering/development/performance-indicators/threat-management/)
* [Threat management OKRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Threat%20Management%20Sub-department)

## Open-source projects

The Threat Management team makes use of a number of open source projects including:
* [Kubernetes](https://kubernetes.io/)
* [ModSecurity Web Application Firewall](https://modsecurity.org/)
* [OWASP ModSecurity Web App Firewall Rule Set](https://owasp.org/www-project-modsecurity-core-rule-set/)
* [Cilium Network Policies for Kubernetes](https://github.com/cilium/cilium)
* [Falco Cloud-Native runtime security](https://falco.org/)
* [AppArmor operating system and application policies](https://gitlab.com/apparmor/apparmor/-/wikis/home)


## YouTube Playlists

You can find demos of features, recorded synchronous meetings, release kick-offs, public group sessions, and more in the Threat Management playlist:

- [Threat Management Sub-Department](https://www.youtube.com/playlist?list=PL05JrBw4t0KpqY7EwA_JaCtb9YrfPLA6d)
- [Threat Management Community Office Hours](https://www.youtube.com/playlist?list=PL05JrBw4t0KrmpGUt33tVXzONJiwCgb_S)
- [Protect: Container Security](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq4O8RBlOKi_euwv8NyI8yt)
- [Secure:Threat Insights Features & Demos](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp8oA6OJ6_wm7uw0muud_mZ)

You can watch the Threat Management Sub-Department playlist below.

<!-- blank line -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/playlist?list=PL05JrBw4t0KpqY7EwA_JaCtb9YrfPLA6d" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
<!-- blank line -->

## Issue Boards

We use the following Workflow Boards to track issue progress throughout a milestone cycle: 
* [Threat Insights group](https://gitlab.com/groups/gitlab-org/-/boards/364214?label_name[]=group%3A%3Athreat%20insights&milestone_title=%23started)
* [Container Security group](https://gitlab.com/groups/gitlab-org/-/boards/364214?label_name[]=group%3A%3Acontainer%20security&milestone_title=%23started)

## Delineate Secure and Threat Management

It is important to delineate who the EM and PM DRIs are for every functionality, especially where this may not be obvious.  It is documented on a dedicated [delineation page](delineate-secure-threat-management.html).

## Label Usage

In addition to the [Workflow Labels Documentation](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#workflow-labels), the labels below are of particular interest to us:

| Label 						                 | Use        									                                         |
| --------------------------------------------   |-------------------------------------------------------------------------------------- |
| `devops::protect`	    			                   | All issues related to the Protect Stage      	                                         |
| `devops::secure`	    			                   | All issues related to the Secure Stage      	                                         |
| `group::threat insights`                       | Vulnerability Management, Responsible Disclosure	                             |
| `group::container security`                    | WAF, Container Network Security, Container Behavior Analytics                         |
| `Category:Vulnerability Management`            | subset of `group::threat insights` issues related to Vulnerability Management         |
| `Category:Responsible Disclosure`              | subset of `group::threat insights` issues related to Responsible Disclosure           |
| `Category:WAF`                                 | subset of `group::container security` issues related to WAF                           |
| `Category:Container Network Security`          | subset of `group::container security` issues related to Container Network Security    |
| `Category:Container Host Security`             | subset of `group::container security` issues related to Container Host Security       |


## Skills

Because we have a wide range of domains to cover, it requires a lot of different expertises and skills:

| Technology skills  | Areas of interest         |
| -------------------|---------------------------|
| Ruby on Rails      | Backend development       |
| Go                 | Backend development       |
| Vue, Vuex          | Frontend development      |
| GraphQL            | _Various_                 |
| SQL (PostgreSQL)   | _Various_                 |
| Docker/Kubernetes  | Threat Detection          |
| Network Security   | Container network security |
| Host Security      | _Various_                 |


## Engineering Refinement & Planning

We are constantly iterating on our [planning process](./planning/) as a team. To maximize our velocity and meet our deliverables, we follow a [refinement process for all issues](./planning/#refinement).


## Product Documentation Process

As the product evolves, it is important to maintain accurate and up to date documentation for our users. If it is not documented, customers may not know a feature exists.

To update the documentation, follow this process:

1. When an issue has been identified as needing documentation, add the `~Documentation` label and outline in the description of the issue what documentation is needed.
1. Assign a Backend Engineer and Technical Writer to the issue. To find the appropriate TW, search the [product categories](/handbook/product/categories/).
1. For documentation around features or bugs, a Backend Engineer should write the documentation and work with the technical writer for editing. If the documentation only needs styling cleanup, clarification, or reorganization, the Technical Writer should lead the work, with support from a Backend Engineer as necessary. The availability of a technical writer should in no way hold up work on the documentation.

[Further information on the documentation process](https://docs.gitlab.com/ee/development/documentation/feature-change-workflow.html).


## Highlights on how we operate

* We [disagree and commit](/handbook/values/#transparency) when necessary.
* We decide on a [**D**irectly **R**esponsible **I**ndividual](/handbook/people-group/directly-responsible-individuals/) for a project or decision.
* We document how decisions are made on which features will be in the secure stage and which will be in the protect stage in order to avoid confusion.  _(Documentation WIP by product management)_
* We prefer asynchronous communication (merge request/issue threads) over synchronous communication (meetings).  In fact, when we cancel a meeting because everything that would have been discussed in the agenda was handled asynchronously, we cancel the meeting and celebrate by posting ["Another One Bites The Dust"](https://www.youtube.com/watch?v=rY0WxgSXdEE) in the team's slack channel .

## Common Links

* [Threat Management subdepartment calendar](https://www.google.com/calendar/render?cid=c_22hchbpp1srt17n7cavvrkggfo@group.calendar.google.com)
* [Secure stage calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9tZDBhbzM2Z3B2bDV2MWY0MTI4ZXJobmo2Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)
* [Protect stage calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9lZDYyMDd1ZWw3OGRlMGoxODQ5dmpqbmIza0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)
* [Secure and Protect Glossary of Terms](/handbook/engineering/development/secure/glossary-of-terms/)
* [Threat Management Wiki](https://gitlab.com/groups/gitlab-org/threat-management/-/wikis/home)

### Product Documentation Links

* [Web Application Firewall with ModSecurity](https://docs.gitlab.com/ee/topics/web_application_firewall/#web-application-firewall---modsecurity)
* [Network Policies with Cilium](https://docs.gitlab.com/ee/topics/autodevops/#network-policy)
* [Install Cilium Network Policies](https://docs.gitlab.com/ee/user/clusters/applications.html#install-cilium-using-gitlab-cicd)
* [Install Falco](https://docs.gitlab.com/ee/user/clusters/applications.html#install-falco-using-gitlab-cicd)
* [Install AppArmor](https://docs.gitlab.com/ee/user/clusters/applications.html#install-apparmor-using-gitlab-cicd)
* [Configure Pod Security Policies](https://docs.gitlab.com/ee/user/clusters/applications.html#using-podsecuritypolicy-in-your-deployments)
* [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)
* [Vulnerability Pages](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/)
* [Security scanner integration](https://docs.gitlab.com/ee/development/integrations/secure.html)
* [Securing your deployed applications](https://docs.gitlab.com/ee/user/project/clusters/securing)
* [Secure and Protect terminology](https://docs.gitlab.com/ee/user/application_security/terminology/)

### Slack

* [Stage #s_secure](https://gitlab.slack.com/archives/C8S0HHM44)
* [Stage #s_protect](https://gitlab.slack.com/archives/s_protect)
* [Subdepartment #sd_threat_mgmt](https://gitlab.slack.com/archives/C017A7JGP7S) and `@threat management engineering`
* [Subdepartment #sd_threat_mgmt_leadership](https://gitlab.slack.com/archives/C0159F9S6VA)
* [Subdepartment #threat_mgmt_standup](https://gitlab.slack.com/archives/CQ29K6XR7)
* [Group #g_protect_container_security](https://gitlab.slack.com/archives/CU9V380HW)
* [Group #g_secure_threat_insights](https://gitlab.slack.com/archives/CV09DAXEW)
* [#sd_threat_mgmt_frontend](https://gitlab.slack.com/archives/C013MM369B7) and `@threat management frontend`
* [#sd_threat_mgmt_backend](https://gitlab.slack.com/archives/C013CET9PGF) and `@threat management backend`
* [#sd_threat_mgmt_social](https://gitlab.slack.com/archives/C01BKFD6GUF)


## How to work with us

Community Contributions are welcome! Please follow these [general GitLab intructions](https://about.gitlab.com/community/contribute/development/) but also note the following items specific to the Threat Management sub-department:
* We've created this [Vulnerability Management: Community Contributor FAQs](https://gitlab.com/gitlab-org/gitlab/-/snippets/1999419) snippet to answer common questions. This is a living document and we encourage suggestions or additions.
* Upcoming Threat Management Community Office hours can be found on GitLab's [Community Office Hours](https://calendar.google.com/calendar/u/0?cid=Y191c2MyNGJ0MHNvZ3Q0N2FoMDJxMHFkNDhra0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) shared calendar. 
* Recordings of previous office hours can be found on our [Threat Management Community Office Hours](https://www.youtube.com/playlist?list=PL05JrBw4t0KrmpGUt33tVXzONJiwCgb_S) YouTube playlist.

While we love to get contributions from our users in the community, we also strive to attract talents in the Engineer teams of this stage to bring our product to the next level.

* Our open positions are listed on the GitLab [Careers page](/jobs/careers/).

Threat Management sub-department Office Hours are scheduled fortnightly. Details can be found on the GitLab Team Meetings, [Threat Mgmt Sub-Department](https://calendar.google.com/calendar/u/0?cid=Y18yMmhjaGJwcDFzcnQxN243Y2F2dnJrZ2dmb0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t), and [Community Office Hours](https://calendar.google.com/calendar/u/0?cid=Y191c2MyNGJ0MHNvZ3Q0N2FoMDJxMHFkNDhra0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) shared calendars. This is a chance for internal and exteral GitLab contributors to ask questions about topics related to the any of the groups or categories listed above.
