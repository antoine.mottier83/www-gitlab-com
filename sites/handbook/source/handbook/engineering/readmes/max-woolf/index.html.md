---
layout: markdown_page
title: "Max Woolf's README"
job: "Senior Backend Engineer"
---

## Max Woolf's README

**Max Woolf - Senior Backend Engineer, Manage:Compliance**

_This is a second attempt at a README based on my first 5 months employment at GitLab._

Hey! My name is Max Woolf and this README is an attempt to do a couple of things:

Firstly, for anyone who hasn't met me or is about to for the first time, this document should give you a rough idea about me both professionally and personally.
It isn't intended to be a detailed autobiography, but hopefully by its content and tone you might get a better feel for me, as a person, and the way that I tend to communicate.

Secondly, for those of you who have met me you may learn something new, reinforce an idea that you had about me, or maybe even refute something you may have incorrectly assumed!

## Related pages

* [LinkedIn](https://www.linkedin.com/in/max-woolf-488bb534/)
* [Personal CV & Blog](https://max.woolf.io)

## About me

**Technology is my passion.** I recently met one of my primary school teachers who told me, even at the age of 9, that she knew I would work with computers. _She was right._

Programming and software engineering in particular, are specific passions and have led me to follow the career path that I am currently on.
Coming to work every day and solving puzzles using code with teams of like-minded individuals is genuinely what I live for.
I am incredibly privileged that my skills are valuable and that I can make a living doing what I love.

I have worked as a full-time software engineer since 2011 and, until starting at GitLab in April 2020, have worked exclusively for small agencies or product startups. 

My specific technological interests (right now) are:

* Ruby on Rails (...obviously)
* iOS development: I'm in a dying breed of people who can code in Objective-C.
* Cloud native development: I used to be AWS SysOps certified.

I'm always looking to learn new things and spend a great deal of my personal time learning about new technologies. In 2021, I intend to start learning Vue.

## Personally

- I was born, raised and now currently live in Birmingham, England. Famous for [chocolate](https://en.wikipedia.org/wiki/Cadbury), [canals](https://en.wikipedia.org/wiki/Birmingham_Canal_Navigations) (we have more than Venice!) and [Black Sabbath](https://en.wikipedia.org/wiki/Black_Sabbath) (amongst other things).
- Me and my wife moved here in 2014 after a few years studying in Brighton and working in Cardiff, Wales.
- I attended University of Sussex in Brighton and studied [Music Informatics](https://en.wikipedia.org/wiki/Music_informatics). Creativity in SWE is really important to me.
- We live with our dog, Winston, who we adopted in 2018. In my new remote job, he is my official foot-warmer. This is, admittedly, a slight downgrade from his planned life of being a [Guide Dog](https://www.guidedogs.org.uk/).
- I'm a charity trustee for my [local park](http://cotteridgepark.org.uk) and recently helped build a small community building and café.
- I suffer from [Obsessive-Compulsive Disorder (OCD)](https://en.wikipedia.org/wiki/Obsessive%E2%80%93compulsive_disorder). I'm always happy to discuss it with friends, family and colleagues. I can often be found taking part in discussions in `#mental_health_aware` in Slack.

The values of **Iteration** and **Diversity, Inclusion and Belonging** are very important to me and inform my working practices.

## What I assume about others

* Everyone has something to teach me. Sometimes, but not always, I have something to teach them.
* [Everyone is intelligent and well-meaning.](https://github.com/thoughtbot/guides/tree/master/code-review#everyone)

## Communicating with me

* I'm generally working 8am - 5pm UK time. The start of my day often changes, but I'm fairly strict about finishing no later than 5pm to give me time to enjoy the evening.
* I'm _always_ available for coffee chats. One of the benefits of working at GitLab is meeting people from across the world and I want to! **Don't be shy, add a coffee chat to my calendar.**
* In general, I prefer Slack over Email for anything that doesn't belong in an Issue or Merge Request.
* Don't hesitate to ask me anything. If I can help, I will! 
* I've been told that my sense of humour is [very typically British](https://en.wikipedia.org/wiki/British_humour) or "dry". I understand that it can be jarring if it's unfamiliar.
* I'm fascinated by different cultures around the world and working at GitLab has given me a rare opportunity to meet a massive cross-section of people across the world. I will ask questions about you and your location if we have a social call, entirely out of my own curiosity. **Please don't hesitate to decline if you're not comfortable!**

## Things I'm trying to improve

* I'm not great at reading between the lines as a way of accepting criticism or constructive feedback. **If something is wrong, please say so.** I will thank you for it!
* I can sometimes act too much like a magpie; paying too much attention to the "new and shiny" things and not enough to the "older but more reliable" things.

## Finding me

Non-employees can drop me an email (mwoolf@gitlab.com) and I'll try to respond as quickly as possible.

GitLab employees can find me in Slack, either via DM or I'm active in the following channels (amongst others).

| Work | Social |
| ---- | ------ |
| `#s_manage` | `#dog` |
| `#g_manage_compliance` | `#mental_health_aware` |
| `#f_graphql` | `#coffee-and-tea` |
| `#development` | `#dad_jokes` |
| `#backend` | `#uk` |
