---
layout: handbook-page-toc
title: "Observability Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who We Are

Observability is:

| Person | Role |
| ------ | ------ |
|[Anthony Sandoval](/company/team/#AnthonySandoval)|[Engineering Manager, Reliability](https://about.gitlab.com/job-families/engineering/infrastructure/engineering-management/#engineering-manager-reliability)|
|[Cindy Pallares](/company/team/#cindy)|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Ben Kochie](/company/team/#bjk-gitlab)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Igor Wiedler](/company/team/#igorwwwwwwwwwwwwwwwwwwww)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Michal Wasilewski](/company/team/#mwasilewski-gitlab)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Matt Smiley](/company/team/#msmiley)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Craig Furman](/company/team/#craigf)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|


## Vision

The Observability SRE Team owns the logging, tracing, metrics, and monitoring services for GitLab.com operations. We foster discussions through data by providing insights into the behaviors of the GitLab application and its underlying infrastructure. We support our department's objective of targeted availability for our customers&mdash;internal and external&mdash;by using data for capacity planning and alerting. We build workflows that streamline incident response and management, collaborating with our product teams to bring the best of our experiences to the GitLab product's [Incident Management](https://docs.gitlab.com/ee/operations/incident_management/) features&mdash;and we dogfood those features into our workflows!

We believe that GitLab Team Members and GitLab.com's users both benefit from access to our metrics. And, so we provide some metrics to GitLab users at https://dashboards.gitlab.com.


## Tenets

"If you can't measure it, you can't improve it." - __Peter Drucker__ 

- We provide data that allows us to understand the past and current performance of our systems. 
- We make it easy to identify the current state of our systems.
- We provide data that helps us to predict the future state of our systems.
- Our data is consistently reliable, accurate, and available to all of GitLab.com's internal stakeholders.
