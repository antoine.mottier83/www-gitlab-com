---
layout: handbook-page-toc
title: "Engineering Internships"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

GitLab is establishing an on-going internship program that can offer students, graduates, career switchers, and those returning to the workforce an opportunity to further their careers. We desire to create an inclusive program that will provide a fair and equal opportunity for all qualifying candidates.

## Engineering Internship Pilot Program

To validate and refine our approach to offering internships at an [all-remote company](/company/culture/all-remote/getting-started/) we have launched a pilot program between May 2020 and August 2020. Due to it's success we have decided to roll out internships on a more continuous basis and have it be our entry level Engineering position at GitLab. More information on the learnings and the program success can be found on this [Working Group page](/company/team/structure/working-groups/internship-pilot/). 

### Candidate qualifying criteria
The following criteria are considered required for candidates to be eligible for Engineering Internship positions:
1. Contributor to open source projects
1. Available full-time during the internship
1. Able and willing to acclimate to, and operate in, an [all-remote environment](/company/culture/all-remote/getting-started/)
1. Interested in fulltime employment after the program (either immediately, or "next semester") - no upfront commitment to GitLab required

As an example of an intern position in our job families please review the [Software Engineering Intern job family](/job-families/engineering/software-engineer-intern/) for further details.

#### Duration and timing
The ideal duration is a minimum of 4 months and a maximum of 12 months depending on the availability of the intern and within the teams of GitLab.  

#### Location
The internship program will primarily be [remote](/company/culture/all-remote/). Below there's an example of the program setup for 4 months. 

#### Internship day-to-day activities
1. Week 1: Intern Fast Boot
    - The program coordinators and mentors will facilitate a pre-defined program. The aim is to onboard the interns, prepare them for their 16 weeks of remote working, and set them up for success.
    - By the end of Week 1, we expect an intern will:
        - Have completed key [Day 1-5 onboarding tasks](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding_intern_engineering.md)
        - Understand remote work and communication best practices
        - Set up their development environment
        - Work with their mentor to understand the development workflow and submit a small MR
        - Complete a Skill Gap Analysis with their engineering manager
        - Learn team specific processes
1. Weeks 2-15: Remote internship
    - An intern's daily schedule will generally reflect [how GitLab team members work](/company/culture/all-remote/getting-started/), which is to say we will not impose a rigid schedule.
    - Interns will be encouraged to favor [async communication](/company/culture/all-remote/management/#asynchronous) and to [set their own work schedule](/handbook/values/#measure-results-not-hours).
    - Mentors and program coordinators will provide coaching if an intern needs help in adjusting to remote work.
    - Interns will participate in the following pre-determined activities
        - Weekly [1:1](/handbook/leadership/1-1/) with their manager
        - Weekly 1:1 with a mentor
        - Weekly 1:1 with an internship program coordinator
        - Weekly intern [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats)
        - 2-3 group meetings per week moderated by a program coordinator.
        - Regular pair programming sessions with a mentor and other team members.
1. Week 16: Intern send-off
    - The program coordinators and mentors will facilitate a pre-defined program. The aim is to process the experience, gather feedback, and share learnings.
    - During the Week 16 program, an intern will:
        - Complete offboarding tasks and hand-off projects if they are not continuing employment with GitLab.
        - Participate in [retrospectives](/handbook/engineering/internships/#team-retrospective) to help improve the internship program.
        - Complete final Skills Gap Analysis with their engineering manager
        - Share experiences and learnings with the company and wider-community through media such as blog posts and GitLab Unfiltered.

#### Compensation

Internships at GitLab offered in the framework described on this page will be paid and follow the same logic as that depicted in our [Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator/) and according to our [Global Compensation Principles](/handbook/total-rewards/compensation/). This means that, as usual, the San Francisco benchmark, location and experience factors will be taken into account during the recruitment process and before making an offer. Depending on country regulations, we will have to align with national labor laws.

### Recruitment
For interns we will target students or career switchers and would look at our Talent community. We would not limit candidate intake to university students and are open to all qualifying candidates.

We exclusively focus all our active sourcing activities on candidates from [underrepresented groups](/handbook/incentives/#add-on-bonus-for-select-underrepresented-groups).

#### Advertising
We advertise on all the traditional recruiting platforms as well as the GitLab vacancies page. Additional advertising is done on internship focussed job boards (e.g. Youtern, angel.co etc.)

#### University Recruitment
We proactively reach out to and engage with universities/colleges to hold a Virtual Careers Talk to encourage students at those universities to apply. Also, we reach out to any associations within these universities that represent underrepresented groups and hold Virtual Careers Talk with them.

We hold virtual talks at a minimum of five universities in the following regions: AMER, EMEA and APAC. 

### Interviewing
For intern candidates we are setting the bar high at the application stage and apply a similar process as to other level roles at GitLab. On [this page](/handbook/hiring/interviewing/#typical-hiring-timeline) you can view a typical timeline for our hiring process. 

### Criteria for teams requesting an intern

Requests for opening a intern requisition will be evaluated on the following criteria:
1. Mentorship
    - Does the team have a manager and 1-2 engineers willing to serve as mentors for the duration of the program?
    - Do the mentors have previous experience mentoring interns or junior engineers? Previous experience is a nice-to-have, but not a must-have.
1. Workload
    - Does the team have a roadmap containing low weight issues with few dependencies suitable for an intern?
1. Team Size and maturity
    - How established is this team? Will they be able to take on an intern without risking a decrease in velocity?     

The team manager and mentors will also need to be able to actively participate in the interview process.     

The process for opening an intern requisition would be as follows: 
1. Opening a requisition starts via either a [backfill process](/handbook/engineering/#backfill-process) or would be based on headcount availability and budget
1. The requesting team writes up a proposal including: projects/workload, proposed career path, requirement knowledge, skills and abilities by the intern. [This example of Security](/handbook/engineering/security/internship.html) can be helpful for review
1. The proposal will be reviewed by Engineering Leadership up until the CTO
1. If approved the manager for the team will be the DRI for the internship
1. The DRI will make sure the intern level is added to the Job Family for their group
1. Recruitment/sourcing starts for intern
1. The recommended minimum length for an internship is 4 months, but can be longer, with a maximum of 1 year
1. The goal would be to bring the intern on board for a fulltime Engineering role after the internship
1. The DRI should connect with the Department Head and People Business Partner when they would want to promote the intern to an intermediate level role at the end of the internship
1. Before hiring, during the internship and when potentially transitioning to fulltime the DRI (manager) is responsible for aligning communication with the stakeholders. Learnings/documents can be used from [the Internship Pilot 2020](/company/team/structure/working-groups/internship-pilot/).

### Measuring success

#### Hiring process after the internship 

After the internship the aim would be to hire the intern as an Intermediate Engineer (depending on previous experience).
The timeline for hiring decisions in a 4 month program would be as follows: 
- Week 8: Check in on feedback with all hosting teams
- Week 10: Performing performance/skills assessments with interns based on the Job Families and [Competencies](/handbook/competencies/#where-we-use-competencies)
- Week 11: Sharing feedback with interns
- Week 12: Aligning with Finance/Business on headcount/budget
- Week 12: Making a decision on offers to interns
- Week 13: Communicating the offer/no offer to interns
- Week 14: Awaiting final decision of interns 
- Week 16: End of the internship program

If an offer is not made or an offer is not accepted, there are opportunities to stay in touch with GitLab's team or People Business Partner and re-enter a conversation at a later time. 

#### Team Retrospective

Upon the completion of the internship, participating teams will be asked to complete
a retrospective. This will follow the process used for GitLab's monthly [team retrospectives](/handbook/engineering/management/team-retrospectives/).
Engineering managers should create an issue in the [gitlab-com/engineering-internships](https://gitlab.com/gitlab-com/engineering-internships) project using the `Retrospective` template and customize it for their team. This issue will initially be confidential.

During the retrospective all team members should be encouraged to contribute, even if they didn't work directly with the intern, so that a complete picture of the effect on the team can be built.

Following the retrospective period the issues will be made public.

#### GitLab Intern Feedback Survey

The feedback survey provides interns with the opportunity to freely express views about working at GitLab. The People Experience team will send the [Feedback Survey](https://docs.google.com/forms/d/e/1FAIpQLSffF8sAFDHTp0JAeum9XYHaG_PsFSHREssSpGECMeJJClXVrg/viewform) to all interns 2 days prior to the pilot programme completion. The People Business Partner & the intern may proactively set up time to discuss their responses and ask for further information. The feedback survey are not mandatory, however your input will help provide GitLab with information regarding what worked well and what can be improved in future iterations.
