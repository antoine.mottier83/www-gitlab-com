---
layout: markdown_page
title: "Security Assurance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
As a sub department of the greater [Security department](/handbook/engineering/security/#assure-the-customer) and member of the [Engineering](/handbook/engineering/) organization, it is the mission of Security Assurance to provide assurance to GitLab customers of the security of GitLab as an enterprise application to use within their organisation. 

## Teams within Security Assurance
There are two teams in the Security Assurance sub department:
* [Risk & Field Security](/handbook/engineering/security/security-assurance/risk-field-security/)
* [Security Compliance](/handbook/engineering/security/security-assurance/security-compliance/compliance.html)

### Risk and Field Security Core Competencies 
These are the primary functions of the Risk and Field Security team:
* [Sales Enablement (Security)](/handbook/sales/onboarding/sqs-learning-objectives/)
* [Customer Knowledge Management (Security)](/handbook/engineering/security/security-assurance/risk-field-security/customer-assurance-package.html)
* [Customer Support (Security)](/handbook/engineering/security/#external-contact-information)
* [Third Party Risk Management Procedure](/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html)
* [Security Operational Risk Management](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html)

### Security Compliance Core Competencies 
These are the primary functions of the Security Compliance team:
* Governance
* [Continuous Control Monitoring/Auditing](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
* [Security Certifications](/handbook/engineering/security/security-assurance/security-compliance/certifications.html)
* [Observation/Remediation](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)

## How to Contact Us

* Join our slack channel: #sec-assurance
* Email: <security-assurance@gitlab.com>

## Customer Resources

Check out these great security resources built with our customers in mind: 

* GitLab's [Customer Assurance Package](/handbook/engineering/security/security-assurance/risk-field-security/customer-assurance-package.html)
* GitLab's [Security - Trust Center](/security/)
* GitLab's [Security Team Page](/handbook/engineering/security/)
