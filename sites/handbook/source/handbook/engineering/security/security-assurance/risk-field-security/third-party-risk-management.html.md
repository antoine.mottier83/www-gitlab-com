---
layout: markdown_page
title: "Third Party Risk Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

In order to minimize the risk associated with third party applications and services, the Risk and Field Security Team has designed this procedure to document the intake, assessment, and monitoring of Third Party Risk Management activities. 

## Scope 

The Third Party Risk Management procedure is applicable to any third party application or service being provided to GitLab. This includes, but is not limited to, third parties providing free or paid applications or software, professional services organizations, contractors, alliances or partnerships, and mergers and acquisitions. 

## Roles and Responsibilities

| Role | Responsibility |
| ------ | ------ |
| **Risk and Field Security team** | Maintain a mechanism to intake and respond to Third Party Risk Management Activities |
| | Provide complete and accurate responses within documented SLA |
| | Document and report any risks or trends identified during Third Party Risk Management Activities |
| | Maintain Metrics |
| **Business or System Owner** | Submit request to Risk and Field Security|
| | Work with the R&FS team to complete the assessment |
| | Accept or Remediate any observations identified |

## Third Party Risk Management Activities Workflow

Regardless of the type of Third Party being assessed, it is important to document the inherent risk they might pose to GitLab. In order to centralize this process, the Risk and Field Security will intake and assess all third parties based on a common methodology. It is important to note that the Third Party Risk Management procedure is meant to function **alongside** of GitLab's procurement and contracting processes and is not a **replacement** for any other required reviews and approvals. 

```mermaid
graph TD
subgraph Legend
	Link[These are clickable boxes]
	end
  POS[Purchase Request:<br>Software]-->1
  FRAP[Free Software] -->1
  POPS[Purchase Request:<br>Prof Serv/Contractors]-->1
  PMF[Purchase Request:<br>Field Mktg & Events]-->1
  ALLI[Alliances]-->1
  MRG[Mergers &<br>Acquisitions] -->1
  1[Submit Request] 
  1--> B{Risk Assessment}
  B -->|Low| D[No Further Action]
  B -->|Moderate| E[Security Assessment and BitSight Required]
  B -->|High| F[Security Assessment, BitSight and<br/>App Sec Review<br/>Required]

click POS "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-saas/"
style POS fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click POPS "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-professional-services/"
style POPS fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click PMF "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-marketing/"
style PMF fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click FRAP "https://about.gitlab.com/handbook/communication/#need-to-add-a-new-app-to-slack"
style FRAP fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

style Link fill:#EAF2FB, stroke:#2266AA, stroke-width:2px
```

## Risk Assessment

GitLab Team Members will follow the [Purchase Request for Software](https://about.gitlab.com/handbook/finance/procurement/vendor-contract-saas/), [Free Software/Apps](https://about.gitlab.com/handbook/communication/#need-to-add-a-new-app-to-slack), [Professional Services and Contractors](https://about.gitlab.com/handbook/finance/procurement/vendor-contract-professional-services/) and [Field Marketing and Events](https://about.gitlab.com/handbook/finance/procurement/vendor-contract-marketing/) process to submit a request for a new or renewing third party. 

Automation then creates a [Third Party Risk Management (TPRM)](https://gitlab.com/gitlab-com/gl-security/security-assurance/risk-field-security-team/third-party-vendor-security-management/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue. The issue template documents key information on the Organization, Reputation, Business Continuity, Legal and Regulatory, Financial, and Customer and Stakeholder impact of the third party on GitLab. 

The Risk and Field Security team converts the answers provided in the TPRM issue into numeric values based on the chart below. The values are then added together to determine the third party's inherent risk. **Note**: Data Classification is weighted 10X higher than the other questions. This ensures that if red data is in-scope, it will guarantee a high inherent risk score and thus require a full security assessment.

---

### Third Party Risk Methodology

| Numberic Value | Organizational Impact | Reputational Impact |Business Continuity Impact |  Legal & Regulatory Impact | Financial Impact |Customers & Stakeholders Impact |
| --- | -- | --- | --- | ---- | ---- | --- |
| 1 | Number of licenses or seats is less than 20% of the total company size | Volume of data, records or external users such as event attendees is no more than 100 |	No system integrations | No compliance frameworks in scope | Contract is up to $999| No data involved | 
| 2 | Number of licenses or seats is 30% - 40% | Volume of data, records or external users is no more than 500   | 1 system integration|  1 compliance framework in scope | Contract is between $1,000 and $9,999 |Green data classification |
| 3 | Number of licenses or seats is 40% - 50%	| Volume of data, records or external users is no more than 1,000 | 2 system integrations | 2 compliance frameworks in scope | Contract is between $10,000 and $29,999 |Yellow data classification | 
| 4 | Number of licenses or seats is 50% - 75% | Volume of data, records or external users is no more than 10,000  | 3 system integrations | 3 compliance frameworks in scope | Contract is between $30,000 and $99,999 |Orange data classification |
| 5 | Number of licenses or seats is 75%+ | Volume of data, records or external users is more than 20,000 | 4+ system integrations	| 4+ compliance frameworks in scope | Contract is for $100,000+ | Red data classification |

---
### Inherent Risk Calculation

**Inherent Risk Formula**= Organizational Impact + Reputation Impact + Business Continuity Impact + Legal and Compliance Impact + Financial Impact + (Data Classification *10). 

---
### Risk Assessment Results and Actions

| Inherent Risk Score | Inherent Risk Tier | Action |
| ------ | ------ | ------ |
| Less than 30 | Low | No further action required |
| 31-44 | Moderate | Security Assessment and BitSight Required |
| Over 45 | High | Security Assessment, BitSight, and App Sec Review Required |

---

## Security Assessment 

If a Security Assessment is deemed as necessary, the Risk and Field Security team conducts the following steps:

1. Reviews independent audits such as SOC1 Type 2, SOC2 Type 2 or similar, as well as any applicable bridge letters. If a third party does not have a SOC report (or similar), a questionnaire will be required based on the [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html). If the third party is in scope for SOX, any CUECs will be mapped to the applicable GitLab control within ZenGRC. 
1. Reviews external testing such as independent vulnerability scan reports or penetration test reports
1. Utilizes BitSight to obtain security rating
1. Engages the Application Security team, as needed, to conducted a technical assessment
1. Documents control effectiveness and ability to meet [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html) and follows [GitLab's Observation Management](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/observation-management/-/blob/master/ZenGRC%20Issue%20(Observation)%20Field%20Definitions.md) process to report risks. 
1. Documents the Residual Risk (the risk after assessing compensating controls)

<p>
<details>
<Summary>Additional Details</summary>

### New Third Parties and Systems

1. Only R&FS can create a new Third Party or System in ZenGRC
1. New Third Parties are created with the `Pending Assessment` status until the Residual Risk is determined, then it will changed to `Accepted`. Third Parties that GitLab no longer utilizes will be changed to `Rejected`.
1. New Systems are created with the `Draft` status until the Residual Risk is determined, then it will be changed to `Final`. Systems that GitLab no longer utilizes will have the `Not in Scope` status.
1. All Systems must be mapped to at least one Third Party; Not all Third Parties will be mapped to a System (example: professional services)
1. All Systems and Third Parties should be mapped to an Org Group

### Activity Location and Statuses
 
**GitLab**

1. Business Owner opens GitLab Issue (Software, Free App, Contractor, Field Marketing)
1. Automation creates a TPRM Issue for each of the GitLab issues noted above
1. R&FS works with the business owner to gather all relevant information
1. R&FS Calculates the Inherent Risk
1. R&FS ensures all areas in the `General Information` and `Inherent Risk Calculation' sections are completed, applies the applicable Inherent Risk Label, and notifies the Business Owner in the original GitLab issue of any next steps. 
1. R&FS closes the TPRM Issue and opens a ZenGRC Audit (pending future automation). Audits for LOW Inherent Risk third parties are automatically closed within ZenGRC as no further action is required. MODERATE and HIGH Inherent Risk third party audits are opened in `Draft` status. 
 
**ZenGRC**

1. R&FS sends `Documentation Request` questionnaire in ZenGRC to the security contact for the third party. **NOTE**: The Audit will stay in `Draft` with no auditor assigned until the questionnaire is completed. 
1. R&FS adds the `Documents Requested` tag to the audit. R&FS ensures the Inherent Risk and Time Period tags are applied in ZenGRC
1. Once the documents are received, R&FS moves the audit to `In Progress` and assigns the next available auditor. 
1. R&FS completes the audit and documents and risks in the `Notes` field. 
1. R&FS works with the Business Owner and the Third Party to mitigate any risks
1. If the Risk is resolved prior to implementation of the Third Party’s application or service, R&FS updates the `Notes` to show the risk as “resolved”. For any unresolved risk, R&FS opens a ZenGRC Issue for Security Compliance that includes the applicable observation risk rating.  
1. R&FS documents the Residual Risk and applies the applicable tag on the ZenGRC audit
1. R&FS exports the Audit Report results to the TPRM Template and attaches a PDF copy to the audit in ZenGRC
 
**GitLab**

1. R&FS completes the `Residual Risk Score and Outstanding Observations` section of the TPRM issue
1. R&FS attaches a PDF copy of the Audit Report to the TPRM Issue and ensures the applicable Residual Risk label is applied. 
1. R&FS attaches a PDF copy of the Audit Report to the original GitLab Issue and notifies thes Business Owner of any further action

### Quality Checklist

**TPRM Issue**
 
- [ ] All impact Score checkboxes are set
- [ ] Inherent and Residual Risk labels are applied
- [ ] Any Issues are added to the `Residual Risk Score and Outstanding Observations` section
- [ ] Final report is linked in the comments
 
**ZenGRC Audit**
 
- [ ] Audit Category, map:Section, map:Vendor, and map:System are set
- [ ] Time Period, Inherent and Residual Risk tags are applied and all other tags are removed
- [ ] Each Assessment `Notes` includes the Risk, Status, and Next steps
- [ ] A PDF copy of the Final Report is attached, and a link is included in the comments
- [ ] If CUEC mapping was done, the CUEC Import File is attached (not a link, the actual file)
 
**ZenGRC Vendor**
 
- [ ] Inherent and Residual Risk and map:System are set
- [ ] Any System Integrations (internal) or Sub-service Providers (such as AWS or GPC) are mapped
- [ ] The Status is set to `Accepted`
- [ ] The Vendor’s Security Contact information is included in the `description` field

**ZenGRC System**

- [ ] Data Classification, Critical System Tier, Compliance Requirements, and Subprocessor status are set
- [ ] map:Vendor is set
- [ ] Any System Integrations (internal) or Sub-service Providers (such as AWS or GPC) are mapped
- [ ] The Status is set to `Final`

</details>

## Email Request Template

> As part of GitLab's Third Party Risk Management program, our Risk and Field Security team assesses Third Parties and their applitcations or services, as applicable, to ensure they meet our [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html). Please provide us with any independent audit reports (such as a SOC2 Type 2), security certifications (such as ISO 27001), recent vulnerability scan summary report, and recent penetration test summary report. Additionally, if there are bridge letters available to supplement your SOC reports, please provide these as well. If you do not have these resources, please email security@gitlab.com and we will send you a link to our Third Party Security Assessment questionnaire. We appreciate your partnership in helping GitLab complete its due diligence requirements.

## Service Level Agreements

Once the request is received, the Risk and Field Security will complete the activities within **10 business days**. If the Third Party does not respond within that timeframe, any open items will be documented as Observations and presented to the requestor. 

## Exceptions

Not Applicable

## References
[Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html)
