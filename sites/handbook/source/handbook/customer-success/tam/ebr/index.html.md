---
layout: handbook-page-toc
title: "Executive Business Reviews (EBRs)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## What is an Executive Business Review?

An Executive Business Review (EBR) is a strategic meeting with stakeholders from both GitLab and the customer. Together with the SAL/AE, a TAM is responsible for scheduling and conducting EBRs and working with their customers to achieve the primary objectives and desired business outcomes.  The TAM will own the content creation, and together the SAL/AE and TAM will determine the flow of the review based on best practices and insight into the customer relationship.

TAMs should hold EBRs with each of their customers **at least** once per year.  Larger ARR accounts or those with more strategic initiatives may benefit from or even require a quarterly cadence, a decision to be made by the TAM, SAL/AE, and Sales/CS leadership.

The EBR aims to demonstrate to the [Economic Buyer](/handbook/sales/#capturing-meddpicc-questions-for-deeper-qualification) the value they are getting in their partnership with GitLab. It is interactive from both sides, discussing the customer's desired business outcomes and related metrics, progress against these metrics and desired outcomes, and aligning on strategic next steps.  The most crucial element in all EBRs is giving the buyer the time to speak about what matters to them, and creating a framework to enable them to do so.

EBRs typically consists of the following content:

1. Introductions to key team members/those present for the EBR
1. Overview/Update on GitLab
1. Reiteration of the customer's goals and desired positive business outcomes as we understand them
    - Direct reference to any desired outcomes/agreed goals from previous EBRs and updates against these specifically
1. Review of progress made against these goals and desired outcomes
1. Aligning with the key decision makers on their strategic vision
1. Recommendations & Next Steps

Also to be considered:
1. Product Roadmap
1. Year in Review (More usage focused)
1. Support Review
1. Delivered Enhancements


## How to prepare an EBR

The first step is the [CTA in Gainsight](/handbook/customer-success/tam/gainsight/#ctas) that will automatically open five months into the subscription, with a due date of one month later to give time to schedule, prepare for and conduct the EBR.  If doing a more frequent business review, please manually open a CTA, and within this CTA, open the "EBR" playbook. The CTA is where you will track the completion of tasks necessary for a successful EBR.

To create the content for the EBR, here you can find the [EBR Template](https://docs.google.com/presentation/d/1FfHBVMsY2a3cteylz7Alq-K43OmBPLylBoVdIccsO8g/edit?usp=sharing).  Please note this template is aimed at a one-hour EBR, and the account team may want to consider removing elements such as product and support insights if a buyer has only given us 30 minutes.

To schedule the EBR, the SALSA/TAM meeting or AE/TAM sync is the best time to review customers that are due an EBR, and agree on next steps, ensuring the economic buyer and key decision makers are invited and able to attend where possible.

Please view our [EBR Playbook](https://docs.google.com/spreadsheets/d/1nGjXMaeAFWEOGdsm2DPW-yZEIelG4sy46pX9PbX4a78/edit#gid=0) (internal to GitLab) for more details on how to propose, prepare, and present an EBR. This internal playbook also includes a link to [EBR sell sheets](https://drive.google.com/drive/folders/1MYIIEqOZ_lskuVUt4S-lkz1HR79ZbDjj?usp=sharing), which TAMs can copy and edit to send to their customers to help demonstrate what the customer will get out of the EBR, as well as an "[EBR in a Box](https://docs.google.com/presentation/d/1V3wzIZ9j6pVUbXpSeJgA_Lk-97C7_vr8NUJWk4J0__s/edit?usp=sharing)" presentation which contains several pointers on the logistics of preparing, such as a suggested timeline, how to prepare and tips on presenting.

[Usage ping](https://docs.gitlab.com/ee/development/usage_ping/) can provide data to build the usage and growth story. If usage ping is not enabled, a [payload](https://docs.gitlab.com/ee/development/usage_ping/#usage-ping-payload) may be periodically requested from the customer. Extracting the payload is a simple process and only takes about 30 seconds. Some examples included in this data are historical and current user count, and CI build history.

There are also several examples EBR decks and past recordings linked in the playbook and EBR in a Box for TAMs and other GitLab team members to review and take inspiration from (please keep all customer-specific content confidential).
