---
layout: handbook-page-toc
title: Growth and Development Benefit
description: A description of programs and resources for team members to access when using the Growth and Development benefit.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Introduction

At GitLab, team members have access to the [Growth and Development benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) to access external resources to develop their career. 

### Growth and Development Benefit Eligibility 

Team members who are full-time and have been employed for three months can participate in this benefit. If you are a part-time GitLab team member, you may still be eligible to participate in this benefit but your reimbursement might be reduced. These situations will be discussed on a case-by-case basis.

Examples of requests that may be approved:
* A Backend Engineer who is not on a performance improvement plan seeking a bachelor's degree in Computer Science.
* A People Operations Generalist seeking a master's in Human Resources with a concentration in International Employee Relations.
* A Security Analyst seeking to take courses to gain a Cybersecurity certificate through an accredited college or university.
* A Product Marketing Manager seeking to take courses to become a Certified Brand Manager from The Association of International Product Marketing and Management.
* A Technical Account Manager seeking to obtain a Cloud Certification such as CompTIA Cloud, AWS Certified Solutions Architect, MicroSoft MCSA/MCSE, Cisco CCNA/CCNP.
* Learning how to code for all team members (for example [Learning Rails on Codecademy](https://www.codecademy.com/learn/learn-rails)).

Examples of requests that may be denied:
* A Marketing Manager seeking a master's in Human Resources, but has no intention of applying to a role in people ops.
* A Frontend Engineer seeking a master's in computer science who is on a performance improvement plan or having discussions around underperformance.
* If the tuition reimbursement budget has been reached, then your request may be denied, even if you meet eligibility requirements.

**Please note that in order to be eligiblity for the Growth and Development benefit, a team member must be meeting performance expectations in their current role.**

### How to Apply For Growth and Development Benefits

**For Growth & Development Benefits that cost under $1,000 USD**
We encourage you to seek verbal approval from your manager before you sign up to anything. If the cost is under $1,000 USD, please fill out the [Growth & Development Benefit Form (Under $1,000)](https://forms.gle/ZLH2KtEqxBtW1Bja6) and submit your expenses via [Expensify](https://about.gitlab.com/handbook/finance/expenses/#expense-reimbursement). 

**For Growth & Development Benefits that cost over $1,000 USD**
Once your manager has verbally approved your plan, please fill out the [Growth & Development Benefit Form (Over $1,000 USD)](https://docs.google.com/forms/d/e/1FAIpQLScoql6Yr-r8j_XjUjJNnJRzgf2rOsOE-gXpydn6r1INLqEfKw/viewform) at least 30 days before the commencement date. Total Rewards will stage a Growth & Development Agreement for you and obtain signatures from the approvers per the table below. If the education provider does not allow for tuition/payment deferment and this would cause financial strain, you can request to receive 50% of the reimbursement up front and the other 50% upon successful completion of the course/program (this option is available for reimbursements above $1,000 USD only as it is processed via payroll).

Reimbursements above $1,000 USD will be paid via payroll in the following pay cycle after you have completed the program and meet any extra eligibility requirements stated in the table below. For Your 4th Trimester Coaching, GitLab will pay the provider directly.

If you voluntarily terminate employment with GitLab prior to completing twelve consecutive months of active employment, expenses of $1,000 USD and over will need to be refunded to GitLab. If you opted to receive 50% of the reimbursement up front but the total amount on your form is at least $1,000 USD, the amount will still need to be refunded back to GitLab.

### How to Apply for an Exception

GitLab understands that tuition costs for academic study can cost more than the $10,000 USD yearly limit. If you have a strong business justification to take up a course or degree and the tuition costs will exceed this limit, then you can apply for an exception (for a maximum amount of up to $20,000 USD per calendar year). To apply, send Total Rewards an e-mail (keep your manager copied) with the course information, business justification for the exception and the yearly cost. Please send the e-mail at least 30 days before your course commencement date.

Total Rewards will get an approval from GitLab's Compensation Group then notify you and the L&D team the outcome. If approved, you can fill out a [Growth & Development Benefit Form (Over $1,000 USD)](https://docs.google.com/forms/d/e/1FAIpQLScoql6Yr-r8j_XjUjJNnJRzgf2rOsOE-gXpydn6r1INLqEfKw/viewform) for the upcoming semester. A new form submission will be needed for each following semester.

#### Tax Implications for Tuition Reimbursement by Country

In some countries, the Growth and Development Benefit may be considered as taxable income and can be (partially) exempted from personal income taxes or subject to employer withholding taxes.

For example, in the United States 2021 tax year, if GitLab pays over [$5,250 for educational benefits for you during the year, you must generally pay tax on the amount over $5,250](https://www.irs.gov/newsroom/tax-benefits-for-education-information-center).

Please contact [Payroll](mailto:payroll@gitlab.com) or [People Ops](mailto:peopleops@gitlab.com) for any tax-related questions in your country.


### Types of Growth and Development Reimbursements

| Category | Description | Eligibility for Reimbursement | Approver (above $1,000 USD) |
| -------- | ----------- | ----------------------------- | --------------------------- |
| Academic Study | GitLab supports team members who wish to continue their education and growth within their professional career. <br> The benefit will cover only the tuition and enrollment related fees. <br> Additional fees related to parking, books, supplies, technology, or administrative charges are not covered as part of the program. Tuition will be validated by receipt showing proof of payment. | - The course must be from a credentialed college or university and must be relevant to your role’s goals and development. <br> - A [Growth & Development form](https://docs.google.com/forms/d/e/1FAIpQLScoql6Yr-r8j_XjUjJNnJRzgf2rOsOE-gXpydn6r1INLqEfKw/viewform) submission will be needed for each semester, however, the relevance of the classes taken for each semester will be subject to the approval of the manager and e-group leader. <br> - The grade must be equivalent to a “B”. A final grade report or transcript is required to receive reimbursement. | - Manager, People Business Partner, E-Group Member. | 
| [Your 4th Trimester Coaching](/handbook/total-rewards/benefits/parental-leave-toolkit/#team-member-tool-kit) | Your 4th Trimester is a 90-day coaching program with Broad Perspective Consulting. The purpose of the program is to help parents (regardless of gender) be as prepared as possible to be a working parent through coaching and development. The cost of this program is $2,300 USD. | - Team members returning from Parental Leave. <br> - The provider will invoice GitLab directly. | Manager and People Business Partner |
| Professional Coaching | Professional coaching for managers and leaders to support their development in their area of interest.  We encourage team members to utilize GitLab's internal resources but understand that some team members may desire to have additional external coaching resources to develop their leadership skills. Individual contributors are encouraged to receive coaching and mentorship from their managers and our internal resources. | - Managers/Grade 8 and above. <br> - Invoice from the coach is required to receive reimbursement. | Manager, People Business Partner, E-Group Member |
| English Language Courses | As GitLab expands globally, we want to support our team members where English may not be their first language. Managers are encouraged to recommend this benefit to team members whose engagement as a part of GitLab and/or performance may be improved by further developing their English language skills. Examples of English Language Courses: <br> - [Coursera](https://www.coursera.org/browse/language-learning/learning-english) offers a wide variety of online English courses in partnership with various reputable colleges and universities, like the [Business English for Non-Native Speakers course](https://www.coursera.org/specializations/business-english-speakers) and the [Improve your English Communication Skills Specialization](https://www.coursera.org/specializations/improve-english) . <br> Their courses range from improving your English language skills to more specialized courses that focus on English for STEM or career development. <br> - [Cambridge C2 Exam Prep Course](https://www.kaplaninternational.com/exam-preparation-courses/cambridge-c2-exam-preparation-course) is a program to consider if preparring to take the Cambridge English Qualification. This is an internationally recognized test and qualification of English language ability. More prep course materials can be found [here](https://www.cambridgeenglish.org/exams-and-tests/proficiency/preparation/) or via [International House London](https://www.ihlondon.com/course/cambridge-c2-proficiency-online) <br> - The [University of Texas Austin](https://global.utexas.edu/english-language-center/programs/self-paced) has multiple self-paced writing and pronounciation courses available. <br> - [LinkedIn Learning](https://www.linkedin.com/learning/search?keywords=english) has many courses for English. <br> - [Writing in Plain English](https://www.linkedin.com/learning/writing-in-plain-english) seems particularly well suited for improving async communication. Language learning Apps and sites can be effective tools. <br> - [Duolingo](https://www.duolingo.com/) offers gamified way to learn English, plus other languages. <br> - [Memrise](https://www.memrise.com/) is helpful for vocabulary building. <br> - Use [italki](https://www.italki.com/teachers/english) to find a English tutor. 1:1 instruction is often necessary for continued improvement, especially once a person has reached basic proficiency. <br> - English Language Courses offered in-person or online by a local college or university or English language courses offered online by a non-local college or university. | Courses offered in-person or online by a credential college or university or an English language school are eligible for reimbursement. | N/A as a course amount isn’t likely to exceed $1,000 USD. If this does exceed $1,000 USD, the academic study criteria would apply. |
| Professional Development/Certifications/Licenses | Any certifications/licenses that you and your manager think will be relevant to your development. | Certifications and licenses related to your role. For reimbursements above $1,000 USD, a final grade report or satisfactory certificate of completion will be required to receive reimbursement. | Manager, People Business Partner, E-Group Member |
| Workshops and Conferences | Work-related conferences including travel, lodging and meals. We encourage people to be speakers in conferences. More information for people interested in speaking can be found on our [Corporate Marketing](https://about.gitlab.com/handbook/marketing/corporate-marketing/#speakers) page. We suggest to the attendees bring and share a post or document about the news and interesting items that can bring value to our environment. | - Before scheduling any travel or time off to attend a conference, please discuss your request with your manager. <br> - The manager will approve the request if the conference is work-related and the timing doesn't interfere with GitLab deliverables. After manager approval, the team member can schedule travel and will be reimbursed for related expenses. | Manager, People Business Partner, E-Group Member | 
| Self-Service Learning (LinkedIn Learning, Coursera, Others) | The company will pay for all courses related to learning how to code (for example Learning Rails on Codecademy), and you may also allocate work time to take courses that interest you. If you are new to development, we encourage you to learn Git through GitLab, and feel free to ask any questions in the #git-help Slack channel. | - The course must be related to your role.<br> - A final grade report or satisfactory certificate of completion are required to receive reimbursements over $1,000 USD. | Manager, People Business Partner, E-Group Member | 

### Example programs by department

The following programs have been contributed by leaders across the organization. These programs may be approved under the Growth and Development policy. If you know of a specific course or program that you or a team member has benefited from, please open and MR and add to this page.

| Department | Program | Notes |
| ----- | ----- | ----- | 
| Any | [Crucial Conversations](https://www.vitalsmarts.com/crucial-conversations-training/) | `Crucial Conversations skills represent the standard in effective communication and the marker of high-performance individuals and organizations everywhere.` |
| Any | [MIT Cultural Awareness for Global Business](https://professionalprograms.mit.edu/online-program-in-cultural-awareness-for-global-business/) | `help professionals improve their intercultural and global leadership skills so that they can succeed in today’s multicultural business environments.` |
| Any | [MIT Leadership and Innovation](https://professionalprograms.mit.edu/online-program-leadership-and-innovation/) | `MIT Professional Education has created the online Leadership & Innovation program, which merges the development of leadership skills with the vision needed to address innovation processes in organizations.` |
| Any | [MIT Professional Certification Program in Digital Transformation](https://professionalprograms.mit.edu/professional-certificate-in-digital-transformation/) | `designed reskilling and upskilling solutions, to help organizations build a culture of continuous education providing their employees with the knowledge and skills needed for the 4th Industrial Revolution.` |
| Any | [Code Academy](https://www.codecademy.com/) | Review their [course catalog](https://www.codecademy.com/catalog) for all programs offered |
| Any | [O'Rielly Technical Training](https://www.oreilly.com/online-learning/) | `O’Reilly learning provides individuals, teams, and businesses with expert-created and curated information covering all the areas that will shape our future—including artificial intelligence, operations, data, UX design, finance, leadership, and more.` |
| Any | [Udemy](https://www.udemy.com/) | Online courses in a variety of content areas |
| Any | [Coursera](https://www.coursera.org/) | Online courses in a variety of content areas |
| Sales/Technical Sales/Marketing | [MIT Cloud and DevOps Online Program](https://professionalprograms.mit.edu/online-program-cloud-devops-continuous-transformation/) | `MIT Professional Education is proud to announce this new, practical program that will explain the vast technological transformation that has occurred in recent years. Content and activities are designed to allow participants to become part of this transformation through technologies such as DevOps and Cloud Computing.` |
| Engineering Managers/Directors | [7CTOs](https://7ctos.com/) | Executive coaching for CTOs | 
| Engineering Managers/Directors | [PlatoHQ](https://www.platohq.com/) | Leadership mentorship for engineering teams |
| Finance/Accounting | Licenses and CPE Credits | Program Examples: Certified public accountant, Certified financial analyst, Certified management accountant, Enrolled agent, Certified internal auditor, Certified fraud examiner, Chartered global management accountant, Certified Payroll Professional, Fundamental Payroll Certification |
| Development and Product | [Certified Agile Leadership](https://www.scrumalliance.org/get-certified/agile-leadership/certified-agile-leadership) | `Scrum Alliance certified agilists who teach Certified Agile Leadership demonstrate years of real-world experience and success coaching agile teams and supporting agile transformations.` |

#### Tax Implications for Tuition Reimbursement by Country

In some countries, the Growth and Development Benefit may be considered as taxable income and can be (partially) exempted from personal income taxes or subject to employer withholding taxes.

For example, in the United States 2021 tax year, if GitLab pays over [$5,250 for educational benefits for you during the year, you must generally pay tax on the amount over $5,250](https://www.irs.gov/newsroom/tax-benefits-for-education-information-center).

Please contact [Payroll](mailto:payroll@gitlab.com) or [People Ops](peopleops@gitlab.com) for any tax-related questions in your country.


### Administration of Growth and Development Reimbursements (Over $1,000)

1. A new form submission will notify the team that a new entry has been submitted and autofill a Growth and Development Benefit Agreement.
1. Check the team member's eligibility and check that the team member's year-to-date spend is within the $10,000 USD limit (check the `Tally` tab on the log). Some team members may have granted an exception to spend up to $20,000 USD for academic study. Any exceptions will be marked `Yes` on the `Limit Exception` column.
1. Download the team member's Growth and Development Benefit Agreement from the "Populated Agreements" folder and stage the document in HelloSign.
1. Fill out the Status columns (in blue) on the Growth and Development Benefit Log sheet.
   * For 4th Trimester expenses, tag Total Rewards on the sheet. 
1. Once all parties have signed the agreement, send an e-mail to the team member to notify them that the agreement has been signed and to send their transcript, certificate of completion or invoice (refer to the [reimbursement category table](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#types-of-growth-and-development-reimbursements)) once they are ready to receive the reimbursement.
1. Save the signed agreement in the team member's BambooHR "Benefits and ISO" folder.
1. Upon completion, update columns L-R on the sheet. Tag Total Rewards on the sheet to notify Payroll via the appropriate Payroll Changes files. 


### Administration of Academic Study Exceptions

1. Forward the team member's e-mail to the Compensation Group.
1. Notify the team member and L&D of the decision outcome.
1. Once approved and the team member submits a Growth & Development form, L&D can update the `Limit Exception` column on the log.


### Administration of Your 4th Trimester 

1. When a new Your 4th Trimester request comes through, Total Rewards will be tagged on the "Growth & Development Benefit Log".
1. Download the team member's agreement from the "Populated Agreements" folder. Delete the `Agreement` paragraph, `Manager Business Rationale` field, `People Business Partner` field and `C Level` field. Stage the agreement for the team member and manager to sign. 
1. Update the Status column on the log once the agreement is staged and once the form has been signed (change to "In Progress").
1. If the team member lives in a country where this benefit is considered taxable income (currently only in Australia and some cases in Belgium), notify payroll.
1. Total Rewards will send an introductory email to Your 4th Trimester to the team member and Barbara Palmer `barbara@your4thtrimester.com` outlining the expected leave date, expected return to work date and date of birthing event. Barbara will conduct the intake with the team member and communicate directly for any coaching throughout the program. 

**Monthly Reporting:**
GitLab will provide Your 4th Trimester with a report monthly of any team members with an upcoming parental leave: expected leave date, expected return to work date, date of birthing event, country, whether the person has already opted in, and maternity or paternity leave. The purpose of this monthly report is to ensure Your 4th Trimester can plan accordingly to the volume of coaching for GitLab team members. 

**Billing:**
Your 4th Trimester will bill GitLab for this benefit quarterly with 50% at intake and 50% at the completion of the coaching program per participant. If there are more than 5 participants in a quarter, there will be a discount on the bill. If there are any tax implications for the GitLab team member, Total Rewards will work with payroll to ensure this is reported compliantly. 

### Recoupment of Growth & Development Reimbursements

Process for Total Rewards Analysts for recouping Growth & Development reimbursements of over $1000 USD:

* Check when the reimbursement was processed (via payroll changes sheets)
* Ask payroll for the pay stubs
* Fill out a [Recoupment Notice Form](https://docs.google.com/document/d/1gKJlcLbQ-Y9KZZniYHRptG6qIOzleyDaCdbQWEkswTA/edit?ts=6033d0b3) 
* Have the form audited by someone in the Total Rewards team
* Stage the document in HelloSign
* Fill out the Growth & Development Recoupment Log
