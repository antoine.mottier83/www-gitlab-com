---
layout: handbook-page-toc
title: 'Sales Operations'
description: "Sales Operations aim to help facilitate new and existing processes throughout our field organization via the use of systems, policies, and direct support. "
---
## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
  {:toc .hidden-md .hidden-lg}


<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

{::options parse_block_html="true" /}

## **Charter**

Sales Operations is a part of Field Operations.
We aim to help facilitate new and existing processes throughout our field organization via the use of systems, policies, and direct support.
Sales Operations main focus is on the Sales organization and supports this group through the following key functions:

<div class="flex-row" markdown="0" style="height:80px">
  <a href="/handbook/sales/territories/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Territories</a>
  <a href="/handbook/sales/field-operations/sales-operations/#sales-operations-go-to-market" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Go To Market</a>
  <a href="/handbook/sales/qbrs/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Quarterly Business Reviews</a>
  <a href="/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Rules of Engagement</a>
    </div>

## **Meet the Team**

- Lindsay Schoenfeld - Senior Manager, Sales Operations
- Amber Stahn - Senior Sales Operations Analyst
- Tav Scott - Sales Operations Manager, Commercial Sales
- Melinda Soares - Senior Sales Operations Analyst
- Meri Gil Galindo - Sales Operations Analyst
- Jeny Bae - Associate Sales Operations Analyst
- Monika Deshmukh - Associate Sales Operations Analyst

## **Teams We Work Closely With**

<div class="flex-row" markdown="0" style="height:80px">
    <a href="/handbook/customer-success/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Customer Success</a>
    <a href="/handbook/sales/field-operations/sales-systems/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Sales Systems</a>
    <a href="/handbook/sales/commissions/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Commissions</a>
    <a href="/handbook/marketing/marketing-operations/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Marketing Operations</a>
    <a href="/handbook/business-ops/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Business Operations</a>
</div>

<div class="flex-row" markdown="0" style="height:80px">
    <a href="/handbook/sales/field-operations/sales-operations/deal-desk/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Deal Desk</a>
    <a href="/handbook/resellers/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Channel Partner</a>
    <a href="/handbook/sales/field-operations/field-enablement/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Field Enablement</a>
    <a href="/handbook/sales/field-operations/sales-strategy/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Sales Strategy</a>
  </div>

## **How to Communicate with Us**

The #sales-support slack channel is monitored by several groups within Field Operations to give guidance or direction.
Salesforce [chatter](/handbook/sales/field-operations/sales-operations/deal-desk/#salesforce-chatter-communication) `@sales-support` is monitored by the Deal Desk team and they will re-direct any questions to Sales Operations if needed.

- Slack: [#sales-support](https://gitlab.slack.com/archives/sales-support)
- Salesforce: [@sales-Support](https://gitlab.my.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F94M000000fy2K)

## **How to Get Help**

<details>
<summary markdown='span'>
  Steps to Getting Help with Okta for Sales Tools
</summary>

1. Visit the [Okta Handbook Page](/handbook/business-ops/okta/) to learn more about Okta.
1. Create a [General Help Desk Request](/handbook/business-ops/team-member-enablement/). Use the _submit issue_ button on the linked page and then select the General Help Desk Request template.
1. Reach out to #it-help in Slack.

</details>

<details>
<summary markdown='span'>
  Steps to creating an issue for Sales Operations
</summary>

1. Create an issue in our [project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations), making sure to provide detailed business requirements.
   Please leave assignee blank.
1. There are existing templates to use, most will fall under the General Request template.
1. New Issues that are in review will be tagged with the `SalesOps::New_Request` label automatically on creation.
1. An issue will be assigned to a Milestone, given an assignee and the `SalesOps::Assigned` if it is ready to be worked on.
1. Any issue that cannot be slotted into the next two milestones will be put in the backlog denoted by `SalesOps::Backlog` until it can be planned.
1. Please review the status of any issue on our agile [board.](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations)

</details>

<details>
<summary markdown='span'>
  Steps to requesting help using Salesforce Chatter
</summary>

1. [Contract assistance](/handbook/sales/#reaching-the-sales-team-internally)
1. [Updating or creating Opportunity Splits ](/handbook/sales/forecasting/#opportunity-splits)
1. [Salesforce Lightning for Gmail](/handbook/sales/prospect-engagement-best-practices)
1. [Support from the Community Advocacy Team](/handbook/marketing/revenue-marketing/sdr/#working-with-the-community-advocacy-team)
1. [DataFox/Zoominfo segmentation conflicts](/handbook/sales/field-operations/gtm-resources/#segmentation)
1. [Reassigning to a Territory Rep](/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement)
1. [Requesting Reassignment](/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement)
1. [If LEAD or CONTACT is owned by SDR team member](/handbook/sales/field-operations/gtm-resources/#record-creation-in-salesforce)
1. [Locked Deal](/handbook/sales/field-operations/gtm-resources/#locking-opportunities-as-a-result-of-their-at-risk-potential)
1. [Deal Desk assistance](/handbook/sales/field-operations/sales-operations/deal-desk/#salesforce-chatter-communication)

</details>

<details>
<summary markdown='span'>
  Steps to create an issue for an account list import
</summary>

Here are the guidelines for requesting account list loads from Sales Operations.
Please follow the instructions below.
The SLA for account list loads into Salesforce is 5-7 business days.

**For uploading a list of net new accounts**

- We have a template you can use to dedupe a list of accounts you have sourced [here in the google drive](https://docs.google.com/spreadsheets/d/1zm4uA2_d7aj31BY2wTxNw6HoElyWJQkjMYY4e1D3tRM/edit#gid=1823098798).
  This template will help you dedupe the account list and also format your list for upload into Salesforce.
  Please follow the directions in the README in the template doc and reach out to #sales-support in Slack if you have any questions.

**Preparing the list:**

1. Clean up list to remove any duplicates and columns not needed.
1. Update field names to Salesforce compatible values. Only include the required fields listed below.
1. Unless you discuss with us prior, nothing else will be loaded and the extra columns will be ignored in the import.
1. Account Source format: List - Name of Source - Date with no spaces or characters.
1. Create an issue in our [project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations) using the Account List Import [template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues).
   Include a link to the list and description of the list load.
1. One tab per sheet, one list load per sheet / one sheet per issue.

**Required Fields:**

| **Label**               | **Field Name**          | **Data Type** |
| ----------------------- | ----------------------- | ------------- |
| Account Source          | AccountSource           | Picklist      |
| Employees               | NumberOfEmployees       | Number(8,0)   |
| Account Name            | Name                    | Name          |
| Type                    | Type                    | Picklist      |
| Account Record Type     | RecordType              | Picklist      |
| Website                 | Website                 | URL(255)      |
| Billing Street          | Billing Street          | Address       |
| Billing City            | Billing City            | Address       |
| Billing State/Province  | Billing State/Province  | Address       |
| Billing Zip/Postal Code | Billing Zip/Postal Code | Address       |
| Billing Country         | Billing Country         | Address       |

<details>
<summary markdown='span'>
  Operators Guide: Instructions for Sales Operations team on completing Account List Imports
</summary>

**Prepping the List**

1. Check the data in the provided list:

   - Only one tab per list
   - Make sure the google sheet template was used to ensure the list has been de duped
   - Double check that everything is in the correct format. [**Reference for correct Billing Address Formatting.**](https://docs.google.com/spreadsheets/d/1_FOkc7CHBDaEzPmpoXtkiQE-u-QB_uuJIcAA4mU1gd0/edit?usp=sharing) and ensure that there are no extra columns, only required fields in the template.
   - If there is an exception and there are additional columns not in the template check the account fields and check to see if they are in the correct format, check for field dependencies, etc.

1. Add Account Owner ID
   - Add a column to the spreadsheet and Label it Account Owner ID
   - Go into Salesforce Setup>Manage Users>Users and find your User ID (15 or 18) number and copy
   - Paste your User ID into the Account Owner ID column in the spreadsheet
1. Add Record Type ID:
   - Insert a column to the left of the RecordType column and label RecordType ID
   - Go into Salesforce Setup>Customize>Accounts>Record Types (Pull the number out of the URL (number after id= and before the &))
   - Paste into the RecordType ID column>copy down
1. Format the Account Source Column
   - Naming convention: List-Vendor-Identifier-Date (example:List-Zoominfo-FranceAC-20200407
   - Copy and paste values down the column with the correct format
1. Create the Account Source Name in Salesforce
   - Go into Salesforce>Setup>Customize>Account>Fields>Account Source
   - Select New and type the Account Source Name you created in step 4
   - Select the record types that it pertains to (standard and channel)>save
   - Select Reorder>check “Display values alphabetically, not in the order entered
1. Save the prepped list for the data load:

   - Save the Excel file in CSV format on your computer
   - Go into the Salesforce reports tab>Account List Import Folder
   - Clone an existing list report. Add a filter Account Source equals and select the name of the list you created from the picklist
   - Select SAVE AS and type the name of the list you created in step 4
   - Save and Run and leave open as this can be refreshed during the data load

**Data Load Instructions**

Important note before using data loader: Turn ZoomInfo Instant Enrich off while using the Data Loader. (Toggle off for Accounts,
Contacts, and Leads, Verify and Save)

1. Open the data loader. Select Insert. Login in production
1. Go to Settings. Change the batch size to 20. Click OK
1. Select Account Object. Browse for the file. Select list CSV file. Click Next
1. Create or Edit Map Fields. Auto-Match Fields to Columns (Usually works for most columns
   _ Make sure that website is mapped
   _ Map Number of employees to the NumberofEmployees: Manual-Admin field
   _ Drop and drag any missing fields
   _ Leave RecordType blank (nothing matches) \* Click OK. Click Next. Select where the error log will save to. Make sure to save to a location otherwise it will save to a mysterious location on your machine
   
</details>
</details>

<details>
<summary markdown='span'>
  Steps to creating an issue for account list update
</summary>

**Preparing the list:**

1. Create an Account report in SFDC.
1. Include the Account ID as a field on your report. This is required to do a mass update.
1. Include the fields on your report that you want updated.
1. Save your report in a public folder so it can be accessed later if needed.
1. Export your report, making sure you have included the Account ID.
1. Update the export. Please keep the original values in the export and create a new column with the new values. Ex: If you want to update website, you will have 2 website columns. Website and then Website New. Website New will be the column you create and where the new value is captured.
1. Create an issue in our [project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations) using the Account List Import [template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues). Include a link to the list and description of the list load.
1. One Tab per sheet, one list update per sheet / one sheet per issue.
1. If you need help pulling the report or walking though these steps, please slack us with a link to the issue you created in the `#sales-support` Slack channel.

</details>

<details>
<summary markdown='span'>
  Steps to update existing reports with ARR fields
</summary>


Below are the steps to update and/or replace ACV with ARR in existing SFDC reports and dashboards.
 
1. **Step 1:** Navigate to the report that needs to be updated 
1. **Step 2:** Click on the **Customize** button to make edits
1. **Step 3:** In the **Preview** section of your report, click on the carrot next to **Show** and make sure  Details has been selected.  This will populate all the fields currently added to your report.
1. **Step 4:** In the **Fields** panel on the left side of your screen, enter “ARR” into the Quick Find search to populate available ARR fields
1. **Step 5:** Drag the ARR field(s) you want to add to your report from the **Fields** panel and drop into the reporting grid.("Net ARR" is the standard field)
1. **Step 6:**  Once your ARR field has been added to the report grid, hover over the name and select the carrot.  From there choose the **Summarize this field** option and check the applicable summary option(s). Select **Apply**
1. **Step 7:** Remove any old IACV fields by dragging them from the report grid to the **Fields** panel OR hover over the name and choose **Remove Column** from the drop down list
1. **Step 8:** If there is a chart or a graph associated with your report that has been edited, you should also edit the chart to reflect your field changes. After you have added and summarized the Net ARR field, Select **Edit** on the very left corner of the chart. On the pop-up screen, change the data that is populated as Sum of Net IACV/Incremental ACV to Sum of Net ARR from the drop down list and Select **Ok**.
1. **Step 9:** When you’ve added and removed all appropriate fields, click the **Run Report** button at the top of your screen.
1. **Step 10:** On the following screen, review your changes to ensure the report is functioning as you expect (select the **Hide Details** button if you don’t want to see all fields) and click the **Save** button. 
1. **Step 11:** In many cases, no changes need to be made to the name or location of the report so choose **Save** or **Save & Return to Report** to complete the final step.  However, if the report name references IACV in any capacity, please update the report name to the corresponding ARR from the **Report Properties** tab at the top before your final Save option.

**Common fields that need to be updated**


| **Existing Field Name** | **Field to Replace With** | 
| ----------------------- | ----------------------- | 
| Net IACV                | Net ARR                 | 
| Incremental ACV         | Net ARR                 | 
| Renewal ACV             | ARR Basis               | 

**Resources**

- Please take a moment to review this quick **video outlining the steps above**: https://www.screencast.com/t/dJxbf069 
- Refer to the [ARR in Practice handbook page](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#net-arr) for **additional information on ARR** 
- If you **need help** updating the report or walking though these steps, please slack us with a link to the SFDC report in the `#sales-support` Slack channel

</details>

## **How we work**

- [Sales Operations Agile Board](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/boards/1655825?label_name[]=SalesOPS)
- [Sales Operations Project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations)
- [Focus Fridays](/handbook/sales/sales-meetings)
- Field Operations Calendar

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_hhs5o85g05lho9agbkfhv8lc40%40group.calendar.google.com&ctz=America%2FLos_Angeles" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

## **Important Resources & Processes**

<details>
<summary markdown='span'>
  Primary Quote System
</summary>

The Primary Quote system is a 1:1 relationship in SFDC that connects the total transaction amount on a quote with the amount on its related opportunity.
This is to ensure we are forecasting the same amount that we will book and enables further automation as the quote is sent to Zuora billing.
To support sales situations that require multiple quotes (for instance: a small deal option and a big deal option), sales reps can identify which one of their quote is "Primary".

[Primary Quote technical documentation here:](/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/#primary-quote-system)

</details>

<details>
<summary markdown='span'>
  Tech Stack
</summary>

The full company tech stack list with definitions can be found on the [Business Operations - Tech Stack Details page
](/handbook/business-ops/tech-stack/). Here are the tools that the Sales Operations team works with on a daily basis.

1. [Clari](/handbook/business-ops/tech-stack/#clari)
1. [Datafox](/handbook/business-ops/tech-stack/#datafox)
1. [ZoomInfo](/handbook/marketing/marketing-operations/zoominfo/)
1. [Gainsight](/handbook/business-ops/tech-stack/#gainsight)
1. [Leandata](/handbook/business-ops/tech-stack/#leandata)
1. [Salesforce](/handbook/business-ops/tech-stack/#salesforce)
1. [Chorus](/handbook/business-ops/tech-stack/#chorus)

</details>

<details>
<summary markdown='span'>
  Overdue Opportunity Process
</summary>

1. An overdue opportunity report goes out every Tuesday to managers.
1. An email alert goes out directly to the opportunity owner when the oppty is past due upon edit of the opportunity. Ex: an action needs to happen to trigger the email.
1. Managers have "over due" reports on their dashboards for review as needed.

</details>

<details>
<summary markdown='span'>
  Salesforce Access Removal Process
</summary>

1. To ensure the appropriate users have access and that we're being fiscally responsible in terms of overall usage, users with no usage in 90 days will be deactivated.
1. Usage will be reviewed once at the beginning of the second month of the quarter so as not to disrupt any quarter end/quarter start cadences. The dates are scheduled on the Field Ops calander.
1. An [Access Change Request](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#access-change-request) will be created and an email will be sent to users as extra notification.
1. If access is needed in the future, please submit a new [Access Request](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#single-person-access-request) and we can confirm if SFDC is the correct place to gather this information or if other tools can provide it.

<details>
<summary markdown='span'> Instructions for Sales Operations team on completing SFDC Access Removal Process
</summary>

**Identifying the Users**

1. Check the Data in the [Provided Report](https://gitlab.my.salesforce.com/00O4M000004aGGo):
   - Make sure the last login date is set to LESS than or equal to 90 Days ago.
   - Verify that no integration users or users that might be tied to an external system are not included in the access removal. If there are questions, error on the cautious side and work with Sales Systems.
1. Create an [Access Change Request](/handbook/business-ops/employee-enablement/it-ops-team/access-requests/#access-change-request)
   - List the users that will be removed so that we have record of reason and users if needed in the future.
1. Email Notification:

   - Work with Sales Operations Manager to send the email notification. This is an extra step for extra visibility to users and might not always be needed depending on volume and other communication that has occured.
   - Example Email:

     Hope that you are having a good week. We in SalesOps are doing a cleanup of our tech stack tools to ensure the appropriate users have access and that we're being fiscally responsible in terms of overall usage. During this process, we've discovered that nearly **XXX** Salesforce.com users haven't logged in for 3 months or more. If you're receiving this message, your user account falls into this bucket.
     As such, I'm writing to let you know that we'll be deactivating your SFDC license on XXX in an effort to ensure that we have enough licenses to provision to our new and existing Sales people; who leverage the tool daily.
     So what does this mean for you?  
      Effective XXX you will no longer have access to SFDC
     If there's critical SFDC data that you need for your role, please submit a new Access Request and we can confirm if SFDC is the correct place to gather this information or if other tools can provide it
     Please let me know if you have any questions.

1. Deactivating Users in Salesforce
   - From Salesforce, access the setup menu and then manager users.
   - Locate the user and uncheck the Active box, or click the Freeze button. Freeze should only be used if the user can not be fully deactivated due to impact to other system or process.
   - If a user is frozen set a reminder in an issue to go back and deactivate user once related systems / process have been udpated.

</details>
</details>

<details>
<summary markdown='span'>
  Sales Operations Case Management
</summary>

**What:**
Salesforce case management system to enable the transfer of certain cases (created from Chatter requests) from the Deal Desk group to the Sales Ops group to take action and respond to the original chatter request.
   - As of 2020-09-21, this will include all cases pertaining to account ownership/ROE and account segmentation, except for any cases that touch closed opportunities. 

**Where:**
- [Deal Desk Queue](https://gitlab.my.salesforce.com/500?fcf=00B4M000004O4ni)
- [Sales Ops Queue](https://gitlab.my.salesforce.com/500?fcf=00B4M000004tewG)

**How:**
1. All Sales Support Chatter requests will continue to flow into the Sales Support queue managed by Deal Desk.
1. Upon reviewing a case and identifying it as an account ownership/ROE or account segmentation request, Deal Desk will change the “Case Reason” field to “Account Reassignment/ROE” or “Account Segmentation Review.”
1. Deal Desk will then change the Case Owner to “Sales Ops Queue,” which will move the case out of the Sales Support queue into the Sales Ops queue. 

**Notes on Case Behavior:**
1. Changing the “Case Owner” to the name of an individual team member marks that team member’s ownership of the case. This action can be taken at the individual case level or in bulk on the queue level.
1. When case ownership is updated to a Sales Ops team member, the case will remain visible in the Sales Ops queue until closed.
1. Changing the “Status” field to “In Progress” shows that someone is currently working on the case. 
1. By changing Status to “In Progress,” ownership will automatically change to the individual who changes status. 
1. By changing Status to “Closed,” the case will no longer appear in the queue.
   - Note: Check the “Suppress auto case reply” button in the same motion, before saving, to prevent automated Chatter posts. If you do not check this box, and you change the status to “In Progress,” upon saving the case, Salesforce will create a Chatter reply from your account, tagging the original requester, noting that you are working on the case. Similarly, when you close the case, Salesforce will Chatter the requester to note that the work is complete.
Click the “Related Chatter Object” link to view the original request. 

**General Notes:**
1. If working a case, and the requester makes a second request out of scope, please tag Sales Support so that a new case is opened for Deal Desk to review the request.
1. If working a case, and any aspect of the request relates to ownership of a closed opportunity, please change the case owner back to Sales Support for Deal Desk to review the request.

<details>
<summary markdown='span'> Common Scenarios for Sales Operations team on completing Sales Operations Cases
</summary>

## Scenarios (these will expand):

1.  Request to own an Account currently owned by the Sales Admin user:
   - Check for duplicates, accurate segment and territory data, accurate reflection of hierarchy.
   - Follow the [ROE](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement) to assign the account to the correct owner.
   - If territory or segment needs to be updated, follow the [Account Review Time Frame and Fields ](https://docs.google.com/spreadsheets/d/1jVz-SzYvBZ6odBW7UoHd4E4b_EUIJgZu7nNY_1StYJs/edit?ts=5f775d4e#gid=0)doc.
1.  Request to own an Account currently owned by a Sales or Marketing Operations team member:
   - NOTE: Most of the time these accounts came from a list load and need extra care in data validation.
   - Check for duplicates, accurate segment and territory data, accurate reflection of hierarchy.
   - Follow the [ROE](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement) to assign the account to the correct owner.
   - If territory or segment needs to be updated, follow the [Account Review Time Frame and Fields ](https://docs.google.com/spreadsheets/d/1jVz-SzYvBZ6odBW7UoHd4E4b_EUIJgZu7nNY_1StYJs/edit?ts=5f775d4e#gid=0)doc.
1.  Request to reassign an account owned by the Impartner Integration user:
   -  Chatter the Channel Manager to assist and validate the move.
1.  Request to reassign an account owned by the Vartopia Integration user:
   -  These customer accounts are OK to move and should follow the [ROE](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement) .
1.  Request to move a Channel / Partner account from or to a Sales Rep or Channel Manager:
   -  Chatter the Channel Manager to assist and validate the move.
1.  Request to move an account to a Public Sector SAL. 
   - Since PubSec is excluded from TSP at this time, the process will be different.
   - Always cc Brent Caldwell on the chatter for awareness.  If unsure, ask Brent to advise on correct owner/territory. 
   - Reference the [Master Territory List Public Sector Account Coverage](https://docs.google.com/spreadsheets/d/19LmWOxDegBfuPCdFK5e0voIUh7EpmF_OT4H4gY9YHUU/edit#gid=1263929675) sheet to determine the owner and the territory. The master tab will give the overview of the territory / SAL relationship.  The following tabs will list the accounts that belong in each territory.
   - NOTE: Public Sector accounts use the **Owner field** and the **Account Territory field**. These will be the only 2 fields to update for Public Sector requests. 
 

</details>
</details>


<details>
<summary markdown='span'> Field Marketing Manager Update Process-Operator's Guide
</summary>

The `Field Marketing Manager` field on the account is maintained by Sales Operations. Reporting for acccounts that need updated are on the[SalesOps CleanUp Dashboard](https://gitlab.my.salesforce.com/01Z4M000000skZj).  Requests to update the field will come in through Chatter requests or Sales Operations issues for requests to change more than 10 accounts.

1. Enterprise, Mid-Market and First Order accounts are updated through LeanData routing and the mapping is maintained in the [Territories Mapping File - SSoT](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=1404562293). 
1. Named and PubSec are dataloaded manually on a monthly cadence. PubSec mapping is maintained in the Master Territory List Public Sector Account Coverage file.  Named mapping is maintained in [Territories Mapping File - SSoT](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=1404562293)in the Named Reps tab. 

</details>

## **Sales Operations Sponsored Dashboards and Maintenance**

The Sales Operations team has sponsored a comprehensive but consumable "Reporting Package" (via SFDC Dashboards) with validated (SalesOps approved) metrics to the Account Executives.
This list will continue to evolve and will be maintained during the onboarding and offboarding process.
In addition to sponsored reporting, Sales Ops will maintain existing reports by archiving or deleting any report or dashboard not used in 180 days.

<details>
<summary markdown='span'>Enterprise Dashboards </summary>

**Enterprise: West**

1. [WEST ENT Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXBZ)
2. [FY21 CQ WEST ENT Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXBo)
3. [WEST ENT Pipeline Health Check](https://gitlab.my.salesforce.com/01Z4M000000oXVy)

**Enterprise: East**

4. [EAST ENT Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXBU)
5. [FY21 CQ EAST ENT Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXBj)
6. [EAST ENT Pipeline Health Check](https://gitlab.my.salesforce.com/01Z4M000000oXVo)

**Enterprise: PubSec**

7. [PubSec Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXC3)
8. [FY21 CQ PubSec Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXCD)
9. [PubSec Pipeline Health Check](https://gitlab.my.salesforce.com/01Z4M000000oXW8)

**Enterprise: EMEA**

10. [EMEA Enterprise Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oX78)
11. [FY21 CQ EMEA ENT Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slbx)
12. [EMEA ENT Pipeline Health Check](https://gitlab.my.salesforce.com/01Z4M000000oXVe)

**Enterprise: APAC**

13. [APAC ENT Pipeline Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXAR)
14. [FY21 CQ APAC ENT Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXA2)
15. [APAC ENT Pipeline Health Check](https://gitlab.my.salesforce.com/01Z4M000000oXW3)

**Enterprise: ISR**

16. [Enterprise ISR Metrics](https://gitlab.my.salesforce.com/01Z4M000000oXXV)

The Pipeline Health Check Dashboards will be sent to each regional team for their review every Monday.

</details>

<details>
<summary markdown='span'>Commercial Dashboards </summary>

**SMB AMER**

1. [SMB AMER Team Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXOY)
2. [SMB AMER Forecasting Splits](https://gitlab.my.salesforce.com/01Z4M000000oXWh)

**SMB EMEA & APAC**
1. [SMB EMEA & APAC Team Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXRh)
2. [SMB EMEA & APAC Forecasting Splits](https://gitlab.my.salesforce.com/01Z4M000000oXWh)

**Mid Market East**
1. [Mid Market-East Team Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXOE)
2. [Mid Market-East Forecasting Splits](https://gitlab.my.salesforce.com/01Z4M000000oXWh)

**Mid Market West**
1. [Mid Market-West Team Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXRr)
2. [Mid Market-West Forecasting Splits](https://gitlab.my.salesforce.com/01Z4M000000oXWh)

**Mid Market EMEA**
1. [Mid Market EMEA Team Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXRw)
2. [Mid Market EMEA Forecasting Splits](https://gitlab.my.salesforce.com/01Z4M000000oXWh)

</details>

## **Sales Operations Go To Market**

### **Account Ownership Rules of Engagement**

[Account Ownership Rules of Engagement Handbook](/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement)

### **Sales Territories**

[Sales Territories](/handbook/sales/territories/)

### **Territory Success Planning (TSP)**

TSP is an automated process workflow intended to properly segment & route Salesforce accounts to the correct Sales territory & respective owner.
This clarifies who should own which accounts & reduces current Ops overhead to manage manually.
TSP fields are designed to be real time reflections of the best data we have, not necessarily the current Go To Market approach.
Requests to override the TSP information can also be submitted in the Account Review section of the account.

<details>
<summary markdown='span'>Operators Guide: Territory Success Planning (TSP) </summary>

**Primary TSP Workflow Components**

1. **Account Routing** (_Next Owner recommendation process_):
   - Sales Segment (i.e. max employee count of the account hierarchy)
   - Primary Account HQ Address (of top Parent Account in the hierarchy)
     - Inputs for both are formulated via our standardized account enrichment tools (in order):
     - Manual Override > DataFox > Zoominfo > Ship-To > Bill-To
   - LeadData then compares these TSP Input Values against our [SSoT Territory Mapping File](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=720021722) & automatically outputs an Approved Next Owner and Territory.
2. **Account Assignment** (_Account Transfer Process_):
   - (Re)Assignment of an account to the correct owner
   - Updating of Account Territory, Sales Segment, Employees fields

**Firmographic TSP Fields**

- `[TSP] Sales Segment`: Segment of the account based on the MAX employee count in that account's hierarchy (regardless if MAX is parent or child).
- `[TSP] Account Employees`: Number of employees **for this specific account**
- `[TSP] MAX Family Employees`: MAX employee count (number) in hierarchy
- `[TSP] Address (Street, City, State, Post Code, Country)`: Location of Ultimate Parent Account based on the TSP data hierarchy
- `[TSP] Geo Story`: Source of address data from TSP Data Hierarchy

**Ownership TSP Fields**

- `[TSP] Next Approved Owner`: Owner of territory as determined by [SSoT Territory Mapping File](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=720021722)
- `[TSP] Transfer Date`: Date when account ownership will change to `TSP Next Approved Owner`

**Territory TSP Fields**

- `[TSP] Territory`: Territory account falls under, as per the [SSoT Territory Mapping File](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=720021722)
- `[TSP] Region`: Sales territory region the account falls under
- `[TSP] Subregion`: Sales territory sub-region the account falls under
- `[TSP] Area`: Sales territory area the account falls under

</details>

### **What if TSP is wrong? How can I request a change?**

In the event our data enrichment tools are outdated or incorrect (primary address or employee count), you can chatter @sales-support and request for an update. Please provide as much detail as possible, justification for the requested change and website links with the information to be reviewed. Be sure to copy your manager if additional approvals will be needed. Please allow at least 48 hours for account changes to take place once the review process begins.

 <details>
<summary markdown='span'>Operators Guide: Process for Requesting TSP Changes:</summary>

**Operations Review Process:**

- Ops will be assigned cases to review and make updates to the `[Admin]` fields in the `Account Review Admin` section on the account.

  - `Number of Employees: Manual - Admin`

  - `Account Address - Manual Source - Admin`

  - `[Admin] Address Street`

  - `[Admin] Address Post Code`

  - `Number of Employees: Manual Source -Admin`

  - `[Admin] Address City`

  - `[Admin] Address State`

  - `[Admin] Address Country`

- Turnaround time for Approved TSP changes to re-populate typically takes 24-48 hours.
  - Accounts with a `[TSP] Transfer Date` populated a day in the future will be re-routed that night to the `[TSP] Next Approved Owner`.
  - `Account Territory`, `Sales Segment` & `Employees` fields will also be updated upon TSP transfer, to continually align accounts.

</details>

### **Opportunity Splits**

The Sales Operations team supports the opportunity credit split approval process with support from Deal Desk. Sales Operations also maintains the process and data maintenance for opportunity splits outside of credit splits. A non-credit split is strictly functionality to support correct 100% split allocation to the opportunity owner when a credit split does not apply. The non-credit splits are automated and the tasks will be to monitor the data for accuracy in Salesforce, ensuring correct data in CaptivateIQ and Clari.

[Opportunity Splits Approval Process](https://about.gitlab.com/handbook/sales/forecasting#opportunity-splits)

<details>
<summary markdown='span'>Operators Guide: Opportunity Splits </summary>

**Opportunity Split Rules**

1. Deal Desk, Sales Comp and Sales Ops are the only users with permissions to add, edit, update and delete splits.
2. 100% Split always goes to the opportunity owner
   - This does not apply for a credit split. Ex: 50/50 or any other percentage split.
   - If the opportunity is incorrectly assigned, the split should still match the owner until correct opportunity assignment is made.
   - This is automated and should be monitored on this [Exception Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXQt) weekly.
3. There will never be a split that totals above 100%.
4. SFDC reports and Dashboards will only reflect opportunity splits if the report type includes opportunity splits.

**Opportunity Splits in Clari**

- Clari will pull the split data out of SFDC.

**Updating | Adding | Deleting a Split In Salesforce**

1.  Navigate to the opportunity in SFDC.
2.  Locate the opportunity splits relationship by using the hover links at the to or scrolling down the page.
3.  Choose the button to add or edit a split.
4.  Once on the split page, assign either the approved credit split, or edit the existing split percentage.

</details>

### **Sales Operations Owned System Provisioning**

Below are instructions on how Sales Operations provisions users within the various Sales-owned systems and the process followed.

For status on an issue please view the [Sales Operations Access Request Board](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/boards/1959331).

<details>
<summary markdown='span'>Operators Guide: Sales Operations Owned Provisioning </summary>

### Sales Operations Access Request Labeling System

1. SalesOpsAR::Action Needed - Sales Ops action item is immediate or within current FQ.
1. SalesOpsAR::In Progress - Sales Ops action item is presently being worked on.
1. SalesOpsAR::On Hold - SalesOps AR that is blocked or not presently w/in scope of work.
   1. SalesOPS: Waiting on SFDC License - a reason an AR might be on hold.
1. SalesOpsAR::Completed - Sales Ops has completed their task on this issue although the issue may not be closed.

### Sales Operations Provisioning Workflow

### Baseline Templates | New Hires

1. The onboarding issue will auto create the AR on day 2
2. The templates will automatically:
   - Add the SalesOpsAR::Action Needed label
   - Leave the assignee blank
   - CC Amber and Meri as the Provisioning DRIs
3. The DRIs will receive an email when a new AR is created
4. All will be able to monitor the [Sales Ops Access Request Board](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/boards/1959331)proactively.
5. When the AR is picked up by the Sales Operations team member, the team member will:
   -assign the issue to themself
   -add the label SaelsOpsAR::In Progress
6. The Assignee should be responsible for working the AR to completion by completing all Sales Operations task items and working the label system accordingly.
7. If Assignee is out of office, the rest of the team is responsible for moving the AR forward in a timely fashion and can see ARs in flight by looking at the [Sales Ops Access Request Board](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/boards/1959331)proactively.

### Single Person or Bulk Access Request

1. The user will manually create a [single person or bulk AR](https://about.gitlab.com/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#singlebulk-access-requests-instructions)
2. In the instructions, they are directed to the Tech Stack doc with the following tasks:
   - Add the SalesOpsAR::Action Needed label
   - Leave the assignee blank
   - CC Amber and Meri as the Provisioning DRIs
3. The DRIs will receive an email when a new AR is created
4. All will be able to monitor the [Sales Ops Access Request Board](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/boards/1959331)proactively.
5. When the AR is picked up by the Sales Operations team member, the team member will:
   -assign the issue to themself
   -add the label SaelsOpsAR::In Progress
6. The Assignee should be responsible for working the AR to completion by completing all Sales Operations task items and working the label system accordingly.
7. If Assignee is out of office, the rest of the team is responsible for moving the AR forward in a timely fashion and can see ARs in flight by looking at the [Sales Ops Access Request Board](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/boards/1959331)proactively.

### Chorus

1. From the [Settings Page](https://chorus.ai/settings/personal-settings) in Chorus, click in **User Management** and then on the **Invite New User** button.
1. To invite users to Chorus, enter their emails in the designated field separated by commas and click the **Add To List** button.
1. Updated the Role, Team and License Type. These will be based off of the users job role/region and should be listed and or approved in the Access Request. If questionable, look at other provisioned users of the same title/role.
1. Click the **Send Invites** button to invite the user to Chorus.

Note: The most accurate access level will be on the Tech Stack or the AR, genenerally AMER and all of Commercial will get recorder, EMEA Commercial and SDR will get recorder once they have completed the GDPR training course.all else listener.

### DataFox

1. From the [Settings Page](https://app.datafox.com/settings/general-information) in DataFox located in the top right under your name, click on **Team Management** located on the left.
1. From **Team Management**, click the **Invite Your Team** button, enter the email addresses of the users to be provisioined, click **Send Invitations**.
1. Most users will have a **Status of Member**, unless otherwise stated in the Access Request.
1. Most users will also have the permission to **Bulk Sync Accounts** unless otherwise noted in the Access Request

### Clari

### SalesForce

</details>

### **Sales Operations Owned System Offboarding**
Below are instructions on how Sales Operations offboards users within the various Sales-owned systems.

1. Sales Operations DRIs will add the salesOps label to any Offboarding issue with SalesOps tasks and work off of this list view to identify users to offboard: [Ready to Offboard List](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=offboarding&not[label_name][]=SalesOps%20Tech%20Stack-Completed).
1. This can also be pulled by going to the employment project and filtering using the Offboarding and SalesOps labels.
1. When the user has been deactivated in the systems listed for the Sales Operations DRIs, or the items are completed, the Sales Ops team member will add the SalesOps Tech Stack-Completed and/or SalesOps Records-Completed label(s) identifying that issue as 'Done.'
