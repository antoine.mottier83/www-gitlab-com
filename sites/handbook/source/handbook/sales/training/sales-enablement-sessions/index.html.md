---
layout: handbook-page-toc
title: "Sales Enablement Sessions"
description: "Continuous education webcast series for GitLab sales team members" 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

# Sales Enablement Sessions

## Upcoming Training
To see what training is coming soon, view the [Sales Enablement Session issue list](https://gitlab.com/groups/gitlab-com/sales-team/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=sales%20enablement%20sessions).

**Spring 2020 Lineup**

| DATE   | TOPIC    | SPEAKERS                                       |
|--------|--------------------------------------------------------------------------|-------------------------------------------------|                      
| Mar 4 | [Co-Selling with IBM](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/107)
| Mar 11 | [Positioning of Verify and Product Groups Visions](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/110)          |  
| Mar 18 | [Q1 FY22 Channel Update](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/103)              |                 |
| Mar 25 | [Co-Selling with GCP](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/106)              |                 |
| Apr 1  | [Co-Selling with AWS](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/108)              |                 |
| Apr 8  |  [Incident Management](https://gitlab.com/gitlab-org/gitlab/-/issues/290229)             |                 |
| Apr 15 | [Q1 FY22 Market Research and Customer Insights (MRnCI) Update](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/95)
| Apr 22 | *No sessions during last 2 weeks of the quarter*
| Apr 29 | *No sessions during last 2 weeks of the quarter*

                                                                                                                                  
                                                                                                                                   
## Past Sessions
<details>
<summary markdown="span">Click here to see links to past sessions on YouTube</summary>


| **Date** | **Topic** | **Public or Private** |
| ------ | ------ | ------ |
| 2021-02-25 | **[Q1 FY22 Competitive Update](https://youtu.be/WzxQjr94mOM)** | Private |
| 2021-02-04 | **[Peak Performance - SKO 2021 Preview](https://youtu.be/f_EZik5MaRU)** | Private |
| 2021-01-14 | **[The GitLab / Red Hat Partnership](https://youtu.be/dfjuUQp5Y90)** | Private |
| 2021-01-07 | **[Q4 FY21 Competitive Update](https://youtu.be/vTfAZIUtris)** | Private |
| 2020-12-10 | **[Deal Registration Program and Working with Partners](https://youtu.be/8XyM4Z-0XLg)**  | Private |
| 2020-12-03 | **[Deal qualification: Best practices in figuring out if you truly have a deal](https://youtu.be/S2MpIS18noM)** | Public|
| 2020-11-19 | **[Maximizing Value of Partners in a Deal (Co-Selling with Partners Part 2)](https://youtu.be/ndrNX8fIIqs)** | Private |
| 2020-11-12 | **[How GitLab Evaluates and Completes Software Purchases](https://youtu.be/55dkX_hNd4s)** | Private |
| 2020-11-05 | **[Co-Selling with Google - Alliance Overview](https://youtu.be/albyLzQNo7U)** | Private |
| 2020-10-15 | **[Proof Points & Analyst Resources Update](https://youtu.be/B0-Y4SNVfXU)**  | Private      |
| 2020-10-08 | **[Deal Structure Best Practices](https://youtu.be/t55rDyNd0wE)**             | Private
| 2020-10-01 | **[Leveraging the GitLab/VMware Alliance](https://youtu.be/unpgyIR9yH0)**      | Private
| 2020-09-24 | **[Q3 FY21 Competitive Update](https://youtu.be/fOeLdLwAgOI)**                 | Private |
| 2020-09-10 | **[Accelerating Your Pipeline with AWS](https://youtu.be/iPDPtOUuNls)**            | Private |
| 2020-09-03 | **[GitLab Standard ROI Models](https://youtu.be/znCrnpTDRoE)** | Public |   
| 2020-08-27 | **[Demystifying the Metrics Conversation](https://youtu.be/dbB6gzNYsG8)**      | Public        |
| 2020-08-20 | **[Q3 FY21 GitLab Product Release Update for Sales](https://youtu.be/Wldab4cx9OE)**               | Public      |
| 2020-08-13 | **[Gainsight Overview for Sales](https://youtu.be/mBdVeZJKthw)** | Private | 
| 2020-08-06 | **[The Value of Professional Services: A CFO's Perspective](https://youtu.be/arF-6BBRAxU)** | Private |
| 2020-07-16 | **[The GitLab Journey & the GitOps Use Case](https://youtu.be/Ap14indu2-w)** | Public |
| 2020-07-14  | **[Special: Forrester TEI Report](https://youtu.beJ_6QgemCz2E)** | Private |
| 2020-07-09 | **[Channel Partner Success for GitLab Sellers](https://youtu.be/OeykHQetd7U)** | Private |
| 2020-07-02 | **[Closing Best Practices](https://youtu.be/y3iLSsAD24s)** | Private |
| 2020-06-25 | **[Q2 FY21 Competitive Update](https://youtu.be/S_z_hgf3ASc)** | Private |
| 2020-06-18 | **[NEW Forrester TEI Report](https://youtu.be/AwYPuWruCjE)** | Private |
| 2020-06-11  | **[Best Practices for Selling Premium and Ultimate](https://youtu.be/jvMPcLdQNI0)** | Private |
| 2020-06-04  | **[Q2 FY21 GitLab Product Release Update](https://youtu.be/36-G7teGhwY)** | Public |
| 2020-06-01  | **[GitLab Pricing Overview](https://youtu.be/qn4rlhJ21Nw)** | Private |
| 2020-05-21  | **[Competitive Update: GitHub Support](https://youtu.be/HY1bffR8Kfc)** | Private |                 
| 2020-05-14  | **[Compliance](https://youtu.be/MRaCKYDXmuI)** | Public |                      
| 2020-05-08 | **[Quarterly Co-Terms](https://youtu.be/CnLfeDhbzcM)** | Private |
| 2020-05-07 | **[Operational Efficiencies](https://youtu.be/8z1ek4Qwnqw)** | Public |
| 2020-04-16 | **[Competitive Update: GitHub](https://youtu.be/1iMBhe0wUdI)** | Private |
| 2020-04-09 | **[Professional Services Offerings & Positioning](https://youtu.be/_04S2JhVZ5A)** | Private |
| 2020-04-02 | **[End to End Visibility & Insights](https://youtu.be/cduwGbXOScY)** | Public |
| 2020-03-26 | **[GitLab Account Based Marketing and Ideal Customer Profile](https://youtu.be/6TbtGiS9Eak)** | Private |
| 2020-03-19 | **[True-Ups](https://youtu.be/vNWUz1BGs2E)** | Public |
| 2020-03-12 | **[The GitLab Journey & The SCM Use Case](https://youtu.be/GN891Bqc6QY)** | Public |
| 2020-03-05 | **[The GitLab Journey & The CI Use Case](https://youtu.be/JoeaTYIH5lI)** | Public |
| 2020-02-27 | **[Q1 FY21 GitLab Release Update](/handbook/marketing/strategic-marketing/release-updates/#123128-sales-enablement)** | Public |
| 2020-02-06 | **[SKO Preview](https://youtu.be/wFqMfGSXsXQ)** | Public |
| 2020-01-13 | **[FY21 Product Outlook - Ops Section](https://youtu.be/dLBPS1T5Wbk)** | Public |
| 2020-01-13 | **[FY21 Product Outlook - Secure and Protect](https://youtu.be/jbuvqD2Ge3M)** | Public |
| 2020-01-09 | **[Reference Edge training session](https://youtu.be/8Le_Ovglnq8)** | Private |
| 2019-12-19 | **[Microsoft Competitive Update](https://youtu.be/C_Lb5jkIcmA)** | Private |
| 2019-12-12 | **[FY21 Product Outlook - CI/CD Section](https://youtu.be/I_H7zGZitRM)** | Public |
| 2019-12-05 | **[Build Pipeline Leveraging the New Digital Transformation Customer Deck](https://youtu.be/UdaOZ9vvgXM)** | Public |
| 2019-11-21 | **[Ultimate Is Ready for Primetime!](https://youtu.be/3M8SIeykbrM)** | Private |
| 2019-11-14 | **[FY21 Product Outlook - Dev Section](https://youtu.be/RzXiZtMGvpQ)** | Public |
| 2019-11-07 | **[How to Sell with AWS to accelerate your pipeline and find new Customers](https://youtu.be/Mi3dtKxypkA)** | Private |
| 2019-10-17 | **[The Art Behind Command of the Message](https://youtu.be/-iIK7SlplEo)** | Private |
| 2019-10-10 | **[Selling Services to Accelerate Customer Adoption](https://youtu.be/ngTI5-1cQK4)** | Public |
| 2019-10-03 | **[Forrester Wave Cloud CI Report: What It Means to GitLab Sales, Customers & Prospects](https://youtu.be/q6YXIvHZuDU)** | Private |
| 2019-09-26 | **[Renewals Update from Sales Ops](https://youtu.be/mpJKNwjcW-4)** | Private |
| 2019-09-19 | **[GitLab Comparison Story against Cloudbees and Jenkins](https://youtu.be/a95DQqRTOHw)** | Private |
| 2019-09-12 | **[Crayon Launch (Market and Competitive Intelligence Tool)](https://youtu.be/qCKj6zB-Ebk)** | Private |
| 2019-08-22 | **[Q3FY20 GitLab Release Update for Sales](https://youtu.be/y2KrovJD76Q)** | Public |
| 2019-08-15 | **[Things I Wish I Knew During My First Few Quarters at GitLab](https://youtu.be/3gprWrDTEQM)** | Private |
| 2019-08-01 | **[Sales and Customer Enablement Update](https://youtu.be/UtPfNfmGnvo)** | Public |
| 2019-07-11 | **[Social Selling 101](https://youtu.be/Ir7od3stk70)** | Public |

</details>


**Note: To watch private videos on GitLab Unfiltered, please look at the Handbook page on [watching private videos.](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) which includes a link to [this 2-minute video](https://www.youtube.com/watch?v=dZtCuOf5aGk).**

## Get an invite to the weekly session

To get a calendar invite to the weekly Sales Enablement session, make sure you are added to the one of the following email groups.

- Sales Team `sales-all@`
- SDR Team `sdr@`
- Marketing team `marketing@`

You can [fill out an access request](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/) to request access to the email group. These groups are invited to the meeting so that each individual person does not need an invite.

## Training Playlist

There are a few places to see previous Sales Enablement sessions:

- The latest, most relevant sessions are highlighted within the [GitLab Sales Learning Framework](/handbook/sales/training/#gitlab-sales-learning-framework)
- [Sales enablement Youtube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX) on GitLab Unfiltered YouTube
- [Sales enablement Youtube playlist](https://www.YouTube.com/watch?v=ZyyBq3_rzJo&list=PLFGfElNsQthYe-_LZdge1SVc1XEM1bQfG) on GitLab YouTube
- Historical sessions that were recorded in Google drive can be accessed via this [deprecated spreadsheet](https://docs.google.com/spreadsheets/d/1ETY7FfCzb2q9h2EkYttlW_Qpl7IHUF-F2rOJG2W03Yk/edit#gid=0)

## Recording

#### Public vs Private

- Some enablement sessions, typically those that focus on industry or technical product knowledge, are [made public](/handbook/values/#public-by-default), including the Q&A
- Other sessions that focus on topics like competitive analysis or openly discussing customers are [kept private](/handbook/communication/#not-public)
- At the start of every session, and before moving into Q&A, the facilitator will announce to the audience if the call is public or private
- Here is a [how-to video](https://www.youtube.com/watch?v=LKZ23pRfpBg&list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX&index=33&t=0s) on accessing private videos on GitLab Unfiltered.

## Enablement pages
- [GitLab CI/CD for GitHub FAQ](/handbook/marketing/strategic-marketing/enablement/github-ci-cd-faq/)
- [Cloud Native Ecosystem](/handbook/marketing/strategic-marketing/enablement/cloud-native-ecosystem/)
- [Enterprise IT Roles](/handbook/marketing/strategic-marketing/enterprise-it-roles/)
- [How to set up Chorus.ai call recording](/handbook/business-ops/tech-stack/#chorus)
- [GitLab Serverless FAQ](/handbook/marketing/strategic-marketing/enablement/serverless-faq/)
- [GitLab.com Subscriptions](/handbook/marketing/strategic-marketing/enablement/dotcom-subscriptions/)
- [Services to Accelerate Customer Adoption](/handbook/customer-success/professional-services-engineering/sales-enablement)

## To request new sales enablement sessions
- Complete and submit this [Sales Enablement Session Request issue template](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/new?issuable_template=sales_enablement_request)
- For urgent requests, please send a Slack message to #field-enablement-team and mention @John Blevins

## Scheduling trainings

**4-6 weeks before the start of a new quarter**
*   Solicit input from sales leaders, Product Marketing, Channel, Alliances, Competitive Intelligence, Analyst Relations, and others (e.g. Sales Ops) on sales enablement topics for the next quarter

**2-4 weeks before the start of a new quarter**
*   Alignment call to finalize topics & tentative schedule with
    *   Sales Training Facilitator (John Blevins)
    *   Director, Sales & Customer Enablement (David Somers)
*   After alignment call, John Blevins to open issues for each enablement session with
    *   Documentation of preliminary learning objectives, title, expected outcomes, etc.
    *   Tentative dates listed as “TENTATIVE - ZZZ”
    *   Intended SME/speaker(s) assigned to the issue
    *   Ping the SME in the issue to ask if they can commit to it, get their commitment before you schedule.
*   John Jeremiah to open issues in Product Marketing aligned to each of the above

**1-2 weeks before the start of a new quarter**
*   John Blevins to schedule meeting with all SMEs/speakers
    *   Goal: Ensure SMEs/speakers are aware of the upcoming sessions and can own the sessions
        *   Review schedule, outcomes, objectives
        *   Answer questions
        *   Adjust as needed
    *   Manage ongoing collaboration via issue, Google Docs, and public Slack channels (e.g. #product-marketing or #sales-and-customer-enablement)

    - The Sales and Customer Enablement team discusses upcoming trainings in regular meetings with sales leadership and the GitLab Product Marketing Management (PMM) team.
- Sessions picked to execute on should be chosen from the backlog, or a new issue created, and moved to the `status:plan` column.
- Assign the issue to the speaker and add `Moderator: <name>` to the issue description.
  - The speaker will then research and generate the conent for the training.
- Once the speaker is ready, a moderator and date should be chosen for the training.
  - Assign the moderator (in addition to the speaker) to the issue and add `Moderator: <name>` to the issue description.
  - Add a due date to the issue.
  - Add the date in ISO format to the issue title.
  - Move the issue to the `status:scheduled` column.
  - Manually drag the issue to order issues in the column by date.

## Enablement calendar
Sales enablement sessions are scheduled on the [Sales Enablement calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_5n3g60l58thum9aovp8iisav34%40group.calendar.google.com&ctz=America%2FLos_Angeles) so that everyone on the Sales and Customer Enablement and PMM team has the ability to edit the calendar event.

## How to conduct a sales enablement training

- Each training session has a [speaker](#speaker) and a [moderator](#moderator)
- Sessions are 30 minutes long
- The presentation portion should be 15 minutes leaving 15 minutes for Q&A

## Speaker

- Create your content as a handbook page (don't use a slide deck)
  - Create a new directory under `/handbook/sales/training/sales-enablement-sessions/enablement/` with the title of your talk
    - For example: `/handbook/sales/training/sales-enablement-sessions/enablement/cloud-native-ecosystem/`.
  - Add an `index.html.md` file to that directory.
    - Use this template:

    ```
    ---
    layout: markdown_page
    title: "Title goes here"
    ---

    ## On this page
    {:.no_toc}

    - TOC
    {:toc}

    ## Title goes here
    ```

  - Add your content to this page.
  - Add links to any other pages you need to reference on this page so you can present from training page.
  - Add a link to the training page from this page in the [#enablement-pages](#enablement-pages) section.

## Moderator

The moderator should serve as the host of the call and overall wingperson for the presenter. The moderator monitors chat to raise questions to the presenter and searches for links that are mentioned on the call to make sure they get linked in the handbook page for the training.

- Log in to zoom 5 minutes ahead of time
- The video should not be recording, but pause if this is the case
- At 12pm ET / 9am PT welcome everyone to the call and remind them that the presentation will be public/private. Specifically mention how to talk about sensitive info on every intro.

> Hello and welcome to today's sales enablement session. As a reminder we'll be posting this session to YouTube. Please remember to not share any private info such as the names of non-referenceable customers.

- Record the call to your local computer.

> For today's GitLab sales enablement training we are pleased to have `<speaker name>` talk to us about `<topic>`. With that, I'd like to pass it over to `<speaker name>`

- Monitor the time. If the presentation goes longer than 15 minutes, interrupt to remind the speaker that we are at 15 minutes and we want to leave time for Q&A.
- Stop the recording.
- After the sales enablement session, upload the video to  the [YouTube Unfiltered channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A). Here's a video tutorial on how to upload a video to the GitLab Unfiltered channel:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xGwX9zjNr2E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

- Make sure a link to the handbook page is included in the description of the YouTube video
- Post a link to the YouTube video in the #sales slack channel
