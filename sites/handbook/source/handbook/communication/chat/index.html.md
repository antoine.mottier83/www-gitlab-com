---
layout: handbook-page-toc
title: "GitLab Communication Chat"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction
At GitLab, Slack is critical to our communication with each other.  While it enables real-time communication, we also are careful to remain true to our asynchronous mindset, suggesting that GitLab team-members set "do not disturb" and not expect real-time answers from others all the time.

There are groups of channels that can help with various areas of GitLab.  This page speaks to a few subsets of those channel groups.

## Channels

### General Channels

There are a lot of general channels, just to name a few:

* **[`#whats-happening-at-gitlab`](https://gitlab.slack.com/archives/general)**: General announcements and company-wide broadcasts
* **[`#questions`](https://gitlab.slack.com/archives/questions)**: Ask all of GitLab for help with any question you have... and then go document it after you find the answer :)
* **[`#thanks`](https://gitlab.slack.com/archives/thanks)**: Where GitLab team-members can [say thanks](/handbook/communication/#say-thanks) to those GitLab team-members who did a great job, helped a customer, or helped you!
* **[`#random`](https://gitlab.slack.com/archives/random)**:  Socialize and share random snippets with your fellow GitLab team-members.

### Team Channels

To raise an issue with a specific team, please use below most commonly used channels;

* **`#peopleops`**: For general People Operations tasks such us onboarding, offboarding, letter of employment, Team calender events management etc
* **`#it-help`**: For IT related queries, Okta, issues with hardware, software, system access requests and user permissions etc
* **`#payroll_expenses`**: For Expensify issues, delayed reimbursements and salaries, payslips, etc
* **`#recruiting`**: For referrals based questions, candidate alignments, general recruiting etc
* **`#total-rewards`**: For general questions on [Total Rewards](/handbook/total-rewards/) including compensation, benefits, and equity


### Channel Categories

#### Account Channels (a_)
These channels (prefixed with a `a_`) are for team members to collaborate and communicate regarding the account for a customer organization. In some cases, they are marked as private or we are connected to customer's own Slack workspace using Shared Channels. To request a new channel or to make changes to existing channels, please use the [access request process](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#slack-google-groups-1password-vaults-or-groups-access-requests).

Many customers that require coordination across GitLab teams have dedicated channels in slack to discuss that customer's needs internally. Those channels are all postfixed with `-internal`.

Commercial (Mid-Market and SMB) accounts have specific [guidelines](/handbook/customer-success/comm-sales/sales-term-glossary-engagements) around having Slack channels with customers.

#### Feature Channels (f_)

These channels are for teams and GitLab team-members interested in a specific feature that exists in GitLab or is being built today!

**Examples**

* **[`#f_web_ide`](https://gitlab.slack.com/archives/f_web_ide)**: Share ideas and feedback about our [Web IDE](/blog/2018/06/15/introducing-gitlab-s-integrated-development-environment/)
* **[`#f_ldap`](https://gitlab.slack.com/archives/f_ldap)**: Talk about our integration with [LDAP providers](https://docs.gitlab.com/ee/administration/auth/ldap.html).

#### Group Channels (g_)

Group channels (prefixed with a `g_`) correspond to a [DevOps Stage group](/handbook/product/categories/#hierarchy) and other [engineering departments and teams](/handbook/engineering/#engineering-departments--teams).

**Example**

* **[`#g_create`](https://gitlab.slack.com/archives/g_create)**: Channel for the [Create](/handbook/product/categories/#create-stage) development group.
* **[`#g_create_source-code`](https://gitlab.slack.com/archives/g_create_source-code)**: Channel for the [Source Code](/handbook/product/categories/#source-code-group) development group.
* **[`#g_geo`](https://gitlab.slack.com/archives/g_geo)**: Dedicated to the [Geo group](/handbook/engineering/development/enablement/geo/).

#### Category Channels (c_)

Category channels (prefixed with a `c_`) correspond to a [DevOps Stage group category](/handbook/product/categories/#hierarchy).

**Example**

* **[`#c_foundations`](https://gitlab.slack.com/archives/c_foundations)**: Channel for the [FE/UX Foundations](/direction/create/ecosystem/frontend-ux-foundations/) category within Ecosystem.

#### Location Channels (loc_)

These channels are used to help GitLab team-members who are in the same general region of the world to talk about get-togethers and other location-specific items. They are also instrumental if you're going to be visiting somewhere else in the world - there might be a GitLab team-member or two nearby! Feel free to join the channel temporarily to chat with GitLab team-members who live there, and see if there is an opportunity to leverage the [visiting grant](/handbook/incentives/#visiting-grant) to add value to your trip.

**Examples**

* **[`#loc_nashville`](https://gitlab.slack.com/archives/loc_nashville)**: Chat with the GitLab team-members in Nashville, Tennessee, USA
* **[`#loc_portugal`](https://gitlab.slack.com/archives/loc_portugal)**: A channel for GitLab team-members in the entire country of Portugal!

#### Project Channels (proj_)

These channels are temporary and used to focus communication around a specific project. As an ephemeral channel they are archived once a project is complete and can be found in Slack by searching for `proj_`

#### Working Group Channels (wg_)

These channels are temporary and used to focus communication around a specific working group. As an ephemeral channel they are archived once the working group's exit criteria is met and can be found in Slack by searching for `wg_`

#### Stage Channels (s_)

Stage channels (prefixed with `s_`) correspond to stages within sub-departments.

**Examples**

* **[`#s_enablement`](https://gitlab.slack.com/archives/s_enablement)**: For the [Enablement stage](/handbook/product/categories/#enablement-stage).
* **[`#s_create`](https://gitlab.slack.com/archives/s_create)**: For the [Create stage](/handbook/product/categories/#create-stage).

#### Sub-Department Channels (sd_)

Sub-department channels (prefixed with `sd_`) correspond to sub-departments within departments.

**Example**

* **[`#sd_threat_mgmt`](https://gitlab.slack.com/archives/sd_threat_mgmt)**: For the [Threat Management sub-department](/handbook/engineering/development/threat-management/).


#### Values Feed Channels (values-feed-)

These channels are automated to show all messages from throughout our Slack channels that are reacted to with one of our values emojis:

<img src="/images/valuesemojis.png" alt="GitLab Values emojis">

The channels are:
* **[`#values-feed-collaboration`](https://gitlab.slack.com/archives/values-feed-collaboration)**
* **[`#values-feed-results`](https://gitlab.slack.com/archives/values-feed-results)**
* **[`#values-feed-efficiency`](https://gitlab.slack.com/archives/values-feed-efficiency)**
* **[`#values-feed-diversity-and-inclusion`](https://gitlab.slack.com/archives/values-feed-diversity-and-inclusion)**
* **[`#values-feed-iteration`](https://gitlab.slack.com/archives/values-feed-iteration)**
* **[`#values-feed-transparency`](https://gitlab.slack.com/archives/values-feed-transparency)**

#### Social Groups

In addition to weekly company group calls that bring us together, GitLab has Social Slack Groups. Social Groups are Slack channels that bring team members together around common interests, hobbies, and lifestyles. Think tennis club, gaming, shell collectors, movie buffs, faith groups, football. You can see a list of Social Slack Groups and their tags below. Feel free to join any that resonate with you. This is a non-exhaustive list, but if you don't see a group you'd love to see on the list, reach out to our diversity, inclusion and belonging team and we'd love to explore your idea.

**Examples:**

* **[`#cats`](https://gitlab.slack.com/archives/cats), [`#dog`](https://gitlab.slack.com/archives/dog), [`#cute-animal-photos`](https://gitlab.slack.com/archives/cute-animal-photos)**: Show us your cutest pics of your pets! We love all pets, but we made special channels for cats and dog(s).
* **[`#cooking`](https://gitlab.slack.com/archives/cooking)**: Share your favorite recipes, and brag about your homemade dishes.
* **[`#dad_jokes`](https://gitlab.slack.com/archives/dad_jokes)**: Self-explanatory really, give us your best dad jokes!
* **[`emilys`](https://gitlab.slack.com/archives/emilys)**: For all the Emily/ie/ia/ees at GitLab.
* **[`#fitlab`](https://gitlab.slack.com/archives/fitlab)**: Channel to discuss fitness and related topics.
* **[`#gaming`](https://gitlab.slack.com/archives/gaming)**: Discuss gaming (both tabletop and video). We also have a Discord linked in the topic.
* **[`#office-today`](https://gitlab.slack.com/archives/office-today)**: Where’s your office today? Share a photo or use words to describe it.
* **[`#lego`](https://gitlab.slack.com/archives/lego)**: Build, inspire, play. Share your builds and invite your children.
* **[`#lgbtq`](https://gitlab.slack.com/archives/lgbtq)**: Space for LGBTQ people and allies in GitLab to chat and support each other.
* **[`#managers`](https://gitlab.slack.com/archives/managers)**: Share ideas, thoughts, and issues on all things to do with being a manager at GitLab.
* **[`#mental_health_aware`](https://gitlab.slack.com/archives/mental_health_aware)**: Mental health is [super important](/company/culture/all-remote/mental-health/) and we realize that. This is a place to discuss concerns you might be having or find someone to talk to if you're feeling alone. We aim to be supportive and available for all who need it.
* **[`#music`](https://gitlab.slack.com/archives/music)**: Share your current playlist or any interesting music you come across.
* **[`#pizza_lovers`](https://gitlab.slack.com/archives/)**: Pizza lovers unite!
