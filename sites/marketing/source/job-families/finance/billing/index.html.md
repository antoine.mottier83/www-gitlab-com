---
layout: job_family_page
title: "Billing"
---

The Billing & Collections Team at Gitlab form part of the Revenue Accounting Group. The Billing & Collections Team are responsible for the Worldwide Billing & Collections process. 

### Billing & Collections Team Levels

### Associate Billing Specialist

The Associate Billing Specialist reports to the Manager, Billing or above.

#### Associate Billing Specialist Job Grade

The Associate Billing Specialist is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Associate Billing Specialist Responsibilities

* Review and evaluation of billing data for accuracy 
* Manage the sync of quote/opportunity information between Salesforce and Zuora
* Responsible for updating any billing errors as they occur prior to invoice submission
* Support auto-renewal billing process, payment, and related data entry
* Post credit card payments in customer accounts as needed
* Escalation of disputes related to billing functions
* Maintenance of billing team Zendesk ticketing system

#### Associate Billing Specialist Requirements

* 1-2 Years experience billing in a high-volume environment
* Understanding of subscription billing preferred
* Superior attention to detail 
* Excellent computer skills, self starter in picking up new and complex systems
* Ability to work independently to meet deadlines and objectives
* System knowledge
   * Zuora Billing
   * Salesforce CRM
   * Google Suite (Gmail, Google Docs, Sheets, etc.)
   * Slack (preferred, not required)

### Billing Specialist 

The Billing Specialist reports to the Manager, Billing or above.

#### Billing Specialist Job Grade

The Billing Specialist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Billing Specialist Responsibilities

* Support training of new/inbound team members
* Posting of external customer cash receipts in Zuora
* Preparation of refund issues in GitLab
* Responsible for monthly reconciliation of customer cash receipts
* Update existing billing team documentation as needed
* Review and evaluation of billing data for accuracy 
* Manage the sync of quote/opportunity information between Salesforce and Zuora
* Responsible for updating any billing errors as they occur prior to invoice submission
* Support auto-renewal billing process, payment, and related data entry
* Post credit card payments in customer accounts as needed
* Escalation of disputes related to billing functions
* Maintenance of billing team Zendesk ticketing system

#### Billing Specialist Requirements

* 3+ Years experience billing in a high-volume environment
* Experience in high volume subscription based billing environments
* Superior attention to detail 
* Excellent computer skills, self starter in picking up new and complex systems
* Ability to work independently to meet deadlines and objectives
* System knowledge
   * Zuora Billing
   * Salesforce CRM
   * Google Suite (Gmail, Google Docs, Sheets, etc.)
   * Slack (preferred, not required)

## Senior Billing Specialist

The Senior Billing Specialist reports to the Manager, Billing or above.

#### Senior Billing Specialist Job Grade

The Senior Billing Specialist is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Billing Specialist Responsibilities

* Maintenance and build of weekly aging and forecast files
* Prepare German VAT sales reports for Accounting Group
* Support foreign withholding tax reporting for billing group
* Support additional month-end ad-hoc reporting/tasks as needed
* Working knowledge of all billing requirements by deal type, entity, and channel
* Support team with inquiries related to billing
* Review and evaluation of billing data for accuracy 
* Manage the sync of quote/opportunity information between Salesforce and Zuora
* Responsible for updating any billing errors as they occur prior to invoice submission
* Support auto-renewal billing process, payment, and related data entry
* Post credit card payments in customer accounts as needed
* Escalation of disputes related to billing functions
* Maintenance of billing team Zendesk ticketing system

#### Senior Billing Specialist Requirements

* Experience in high volume subscription based billing environments
* Intermediate Excel/Google Sheets
* Understanding of billing and revenue
* Superior attention to detail 
* Excellent computer skills, self starter in picking up new and complex systems
* Ability to work independently to meet deadlines and objectives
* System knowledge
   * Zuora Billing
   * Salesforce CRM
   * Google Suite (Gmail, Google Docs, Sheets, etc.)
   * Slack (preferred, not required)

### Manager, Billing

The Manager, Billing Reports to the Sr. Manager, Billing or above.

#### Manager, Billing Job Grade

The Manager, Billing is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Billing Responsibilities

* Manage team billing functions and staff
* Works with the management team to understand the billing, collections, the billing team, the billing culture, and the area of strengths/weaknesses, to develop short-term and long-term plans to support the company’s strategic goals
* Assess current billing operations, offer recommendations for improvement, and implement new processes
* Assists revenue cycle staff with any day to day issues
* Assists in the monthly closing process for billing, collections, and revenue as needed
* Assist with the annual financial statement audit
* Create and distribute periodic billing metrics and reports; prepare special reports, as requested
* Manages assessment and hiring needs of the department 
* Assessment and distribution of staff training and responsibilities
* Collaborate with team members to assist with ad-hoc projects and miscellaneous tasks

#### Manager, Billing Requirements

* Bachelor’s degree in Accounting/Finance or equivalent experience
* 5+ Years of experience in billing and collections
* 3+ Years experience with Zuora and Salesforce from quote to cash 
* Proven ability to perform strategic planning and set priorities for a billing department
* Proven track record for improving processes and problem- solving
* Strong leadership skills with an ability to coach and motivate
* Solid understanding of billing and financial concepts
* Strong analytical and problem-solving skills
* Excellent verbal and written communication skills and ability to collaborate with cross-functional teams. Able to work in stressful situations with firm deadlines
* Ability to use GitLab
* System knowledge
   * Zuora Billing
   * Salesforce CRM
   * Google Suite (Gmail, Google Docs, Sheets, etc.)
   * Slack (preferred, not required)

### Senior Manager, Billing

The Senior Manager, Billing Reports into the Director, Billing & Collections.

#### Senior Manager, Billing Job Grade

The Senior Manager, Billing is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager Billing Responsibilities

* Manage the Billing/Collections organizations
* Manage month-end tasks and requirements for Billing/Collections team
* Ensure audit compliance in all related functions
* Establish and manage effective business systems and direct effective processes, procedure and practices.
* Motivates staff to achieve peak performance and productivity
* Proactively manage and ensure compliance with control objectives
* Must be competent in troubleshooting complex billing challenges, identifying and prioritizing gaps in processes.
* Collaborate with Revenue, IT, Vendors and internal stakeholders, to resolve roadblocks across various internal organizations
* Partner to resolve system discrepancies and to develop systemic enhancements to gain efficiencies. 
* Approves employee schedules, absences, overtime, and vacation. 
* Oversees the hiring and termination process with support from the People Team
* Writes and conducts performance evaluations

#### Senior Manager, Billing Requirements

* 5+ Years of management of Billing/Collections Team
* 5+ Years of experience in billing and collections
* 3+ Years experience with Zuora and Salesforce from quote to cash collection
* Bachelor's Degree in Finance/Accounting or related field  preferred 
* Proven ability to perform strategic planning and set priorities for a billing department
* Proven track record for improving processes and problem- solving
* Strong leadership skills with an ability to coach and motivate
* Solid understanding of billing and financial concepts
* Strong analytical and problem-solving skills
* Strong working knowledge of business systems
* Excellent verbal and written communication skills and ability to collaborate with cross-functional teams. 
* Able to work in stressful situations with firm deadlines
* Ability to use GitLab
* System knowledge
   * Zuora Billing
   * Salesforce CRM
   * Google Suite (Gmail, Google Docs, Sheets, etc.)
   * Slack (preferred, not required)

### Director, Billing and Collections

The Director, Billing & Collections position provides the strategic vision for billing & collections at GitLab. The Director is responsible for the performance and effectiveness of each department. Responsibilities include, but are not limited to; managing the daily work-flow of the department, monitoring processes and performance to identify trending issues and develop training solutions or procedures to address these issues, and will lead a Billing/Collections management teams to ensure key performance goals are attained and exceeded.

The Director, Billing & Collections reports to the Sr. Director, Revenue Accouting. 

#### Director, Billing and Collections Job Grade

The Director, Billing and Collections is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Billing and Collections Responsibilities

- Leads the Billing and Collections teams and effectively manages the risk of credit extensions while ensuring the DSO and dilution on accounts is kept within acceptable limits as set forth by Finance Leadership
- Deploy interim solutions for business-critical needs; must have ability to quickly assess unfamiliar situations and provide remediation, including modifying standard processes to address longer term issues
- Drive transformation, improve productivity and streamline business operations
- Manage review and analysis of business systems and upgrades
- Define and management performance metrics of Billing and AR Teams
- Build, optimize and automate performance reporting
- Partner with Revenue Director and Sales Operations team to drive process improvement and implementation
- Manages high level billing escalations and makes related business decisions
- Analyzes departmental performance and advises the teams on high risk decisions while maintaining established credit and underwriting controls to prevent loss exposure
- Educates, trains, monitors, problem-solves, and ensures company policies are followed across the Billing/Collection departments and provides strategic direction to management and their teams 
- Ensures all team members are effectively trained and have the tools available to carry out their tasks in an accurate and efficient manner
- Responsible for oversight of the monthly accounts close process (billing/collections), resulting in timely delivery of accurate data for management reporting
- Responsible to participate in external and internal audits as they relate to billing functions
- Responsible to supervise and direct activities of billing and collections staff to ensure consistent application of best practice, productivity, and adherence to internal controls for billing compliance

#### Director, Billing and Collections Requirements

- Bachelor's Degree in Accounting, Finance, or Business Administration required
- 7+ years in Billing/Collections management
- 3+ Years experience with Zuora and Salesforce from quote to cash collection
- Proven ability to perform strategic planning and set priorities for a billing department
- Proven track record for improving processes and problem- solving
- Strong leadership skills with an ability to coach and motivate
- Solid understanding of billing and financial concepts
- Strong analytical and problem-solving skills
- Excellent verbal and written communication skills and ability to collaborate with cross-functional teams. Able to work in stressful situations with firm deadlines
- Ability to use GitLab

## Performance Indicators

- [Average days of sales outstanding](/handbook/finance/accounting/#accounts-receivable-performance-indicators)
- [Time for Invoices to be generated when a deal is closed won in Salesforce](/handbook/finance/accounting/#accounts-receivable-performance-indicators)
- [Percentage of ineffective Sox Controls](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Career Ladder

The next step in the Billing & Collections job family is to move to the [Revenue Accounting](/job-families/finance/revenue-accounting/) job family.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/)

- Qualified candidates will be invited to schedule a 30 minute screening call with a member of the Recruitment team
- Then, candidates will be invited to schedule a 45 minute interviews with the Hiring Manager and a 30 minute interview with a Peer
- Finally, candidates will be invited to a 45 minute interview with a member of the Executive team.

As always, the interviews and screening call will be conducted via a [video call](h/handbook/communication/#video-calls). See more details about our interview process [here](/handbook/hiring/interviewing/).
