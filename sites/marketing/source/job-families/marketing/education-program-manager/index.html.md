---
layout: job_family_page
title: "GitLab for Education"
---

## GitLab for Education
The GitLab for Education team’s mission is to facilitate and drive the adoption of GitLab at educational institutions around the globe. We aim to build an engaged community of GitLab evangelists and contributors in the next generation of the workforce and tell the story of how GitLab and DevOps as a  discipline, are advancing and transforming teaching, learning, and scientific research.


### Responsibilities
- Own the story around GitLab’s role in Education and how integrating DevOps into teaching, learning, and scientific research is transformational.
- Create and execute a content strategy and awareness platform to tell GitLab’s story about DevOps in Education.
- Be a thought leader for teaching and learning DevOps in related academic disciplines.
- Act as a strategic influence in curriculum design, essential bodies of knowledge, and competencies models.   
- Develop, manage, and grow partnerships with key institutions, centers, organizations, and foundations.
- Use such tactics as online publishing, social, events, public relations, and partnerships to demonstrate GitLab’s influence on DevOps in Education.
- Maintain and evolve a standalone digital publication focused on the role DevOps has in Education (teaching, learning, and research). Manage content production from start to finish.
- Work with students, faculty, and staff around the globe to highlight unique and transformational use cases.
- Engage with academic associations through presentations, articles, and conferences with the goal of spreading awareness of GitLab’s offering and how DevOps can transform learning.
- Engage with scientific advancement associations, foundations, and governmental organizations to evangelize and enable DevOps in scientific research.
- Create engaging content that enables students, faculty, and staff to teach, learn and conduct research with GitLab.
- Assemble and curate existing GitLab content in a way that is accessible to students, faculty, and staff.
- Work cross-functionally to develop and share key insights that can be applied across teams and programs: Product Management and Design, Community Relations, Marketing, Diversity Inclusivity, and Belonging.  


### Requirements
- An understanding of the education market segment with a focus on higher education.
- A passion for learning, teaching, and research
- Analytical and data-driven in your approach to building and nurturing communities.
- Experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
- Familiarity with developer tools, Git, Continuous Integration, Containers, and Kubernetes.
- Ability to use GitLab’s Capability to gracefully handle day-to-day tasks, showcasing compassion and empathy.
- Capability to coordinate across many teams and perform in a fast-moving startup environment.
- Aptitude to be a [manager of one](/company/culture/all-remote/self-service/) and work with minimal supervision.
- Outstanding written and verbal communication skills.
- You embrace our [values](/handbook/values/), and work in accordance with those values.


## Levels

## Education Evangelist
Education Evangelist reports to the Education Program Manager.

### Education Evangelist Job grade
The Education Evangelist is a [grade 6](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).


### Education Evangelist Responsibilities
- Connect, engage with and gather feedback from faculty, students, and staff.
- Execute GitLab for Education Program initiatives and campaigns.
- Curate content from GitLab for Education Program Members, the wider scientific community,  academic literature, as well existing GitLab content.
- Create and curate original content to enable faculty, students, and staff to adopt and be successful with GitLab. This includes blog posts, webinars, videos, eLearning, courses, and best practices.
- Serve as a spokesperson for GitLab’s Education Program.
- Update and document processes in the GitLab for Education handbook.
- Activate the GitLab team in sharing GitLab for Education content with their networks, increasing our reach to media, academia, analysts, etc.
- Collaborate with GitLab’s PR agency partners to facilitate and secure media coverage for GitLab’s campaigns.
- Compile quarterly metrics reports to measure our GitLab in Education successes in relation to awareness and impact.
- Manage projects from start to finish.


### Education Evangelist Requirements
- 3-5 years experience in technical evangelism, program management, or education.
- An understanding of GitLab’s and DevOp’s role in Education.
- Experience in publishing, journalism, content marketing, social, and events. At least 1 year of experience giving talks and developing technical content.
- Self-directed and work with minimal supervision.
- Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
- You share our values and work in accordance with those values.
- Ability to use GitLab.


## Education Program Manager


### Job Grade

The Education Program Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Run and develop the [GitLab for Education](/solutions/education/) program. Increase operational efficiencies and streamline processes.  
- Establish relationships with educational institutions to produce inspirational case studies of their use of GitLab. Showcase Program Member’s success through Case Studies, webinars, blog posts, spotlights, and media campaigns.
- Build the foundations of a GitLab for Education community and engage with program members.
- Expand the GitLab for Education program with a learning package to facilitate and incentivize the use of GitLab for Educational purposes.
- Conduct and publish a survey on the GitLab in Education Program.
- Grow the number of institutions and increase the renewal percentage quarter over quarter.


### Requirements

- You have 5-7 years of experience running developer relations or community advocacy programs, preferably open source in nature.
- Analytical and data driven in your approach to building and nurturing communities.
- You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
- Excellent spoken and written English.
- Familiarity with developer tools, Git, Continuous Integration, Containers, and Kubernetes.
- A background and relationships in the academia and research spaces are a plus.
- Ability to use GitLab
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.

## Senior Education Program Manager

### Job Grade

The Senior Education Program Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
The Senior Education Program Manager shares the same responsibilities and requirements as the Education Program Manager listed above,  but also carries the following:

### Requirements

- You have 7-10 years of experience running developer relations or community advocacy programs, preferably open source in nature.
- Prior experience developing relationships with educational institutions.
- Substantial experience in academia and research spaces.

## Director, Education

The Director, (Head of Education) reports to the Director of Community Relations.

### Director, Education (Head of Education) Job Grade
The Director, Education is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Director, Education (Head of Education) Responsibilities
- Develop an overall strategy and vision for GitLab and DevOps in Education.
- Develop and maintain a content roadmap for enabling GitLab in Education Program Members to teach, learn and conduct research with GitLab.
- Demonstrate thought leadership in Education.
- Maintain and evolve a standalone digital publication focused on the role DevOps has in Education. Champion GitLab’s prominence therewithin.
- Work strategically to build a foundation for DevOps as a discipline by increasing the number of courses, curriculum, and offerings on DevOps.   
- Share and promote scientific research and publications using GitLab into mainstream media through press, articles, podcasts, with the goal of translating science to industry and highlighting GitLab’s capabilities.
- Champion the importance of DevOps workforce development in higher education.
- Collaboratively work across Corporate Marketing, Strategic Marketing, Learning and Development, Sales, and Product to tell GitLab in Education’s story.
- Responsible for ideation of all campaign activities, initiatives, OKRs, and reporting on results.


### Director, Education (Head of Education) Requirements
- 5+ years experience in Education strategy, curriculum development, pedagogy, and technical enablement.
- Proven experience leading the adoption of technology in education.
- Experience overseeing campaigns that include public relations, events, social, publishing, and content.
- Proven experience building strategic relationships with Universities, associations, government agencies, foundations, and centers.


### Performance Indicators
- Number of educational institutions and users adopting GitLab per quarter.
- Number of educational institutions renewing GitLab year over year.
- Published articles, podcasts, and other content by media and external sources.
- Views (website traffic) to content related to GitLab for Education on `about.gitlab.com` and impressions attributed to brand awareness efforts.


### Hiring Process
Candidates for positions within the GitLab for Education team can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on [our team page](https://about.gitlab.com/company/team/).

- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters.
- Candidates may then be offered a 30 minute interview with one of our Senior Community Programs Managers.
- Next, candidates will be invited to schedule an interview with our Director of Community Relations.
- Candidates will then be invited to schedule interviews with key team members from relevant company departments that the role will work with closely.
- Finally, our Chief Marketing Office or Sr. Marketing Director(s) may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via video, phone, or email.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring).
