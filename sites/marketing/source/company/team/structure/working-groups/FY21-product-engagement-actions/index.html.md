---
layout: markdown_page
title: "FY21 Product Engagement Actions"
canonical_path: "/company/team/structure/working-groups/FY21-product-engagement-actions/"
description: "Take action on the FY 21 product engagement survey results."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value             |
|-----------------|-------------------|
| Date Created    | February 23, 2021 |
| Target End Date | April 30, 2021    |
| Slack           | [#wg_fy21_product_engagement_actions](https://gitlab.slack.com/archives/C01P9FJRDHS) (only accessible by GitLab team members) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1_5Rf3z1uH0ieIvUJIK0CDVeG9H-EG__1B-OLxARZOxA/edit#) (only accessible by GitLab team members) |
| Docs            | TBD |
| Epics/Issues    | [Engagement Survey Issue](https://gitlab.com/gitlab-com/Product/-/issues/1986) / [Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1453895?scope=all&utf8=%E2%9C%93&label_name[]=Product%20Operations&label_name[]=wg-fy21-engagement-actions) / Engagement action item epics: [1386](https://gitlab.com/groups/gitlab-com/-/epics/1386)  [1387](https://gitlab.com/groups/gitlab-com/-/epics/1387)  [1388](https://gitlab.com/groups/gitlab-com/-/epics/1388) |
| Label           | `~wg-fy21-product-engagement-actions` |
| Associated KPIs/OKRs | TBD |

## Problems To Solve

For more details please reference the [Product FY'21 Engagement Survey Results](https://gitlab.com/gitlab-com/Product/-/issues/1986).

1. Define a competency for managers to conduct effective 1-2 year career development conversations. Update the product handbook with guidance, and develop a template so we have a consistent approach across the division. Each team member should then feel welcome to fill out the document and schedule time with their manager to discuss. We very much want to be having regular career conversations on a cadence the works for each person.
1. Invest in a more robust Learning & Development curriculum for Product team members for at least three key competencies. Leverage content from LinkedIn, ProductTalk, and homegrown sources. Link content to competency development handbook descriptions, the Career Development Framework, and onboarding instructions.
1. Develop a product handbook page outlining the required monthly release tasks for PM's. Make it clear what's required, and what's optional and/or there for best practice guidance. Endeavor to reduce the monthly release task list for PM's so there is more time available for high value activities like customer, market, and competitive research.


## Business Goals

- Create focus on value, outcomes over outputs, for product managers
- Boost product team job satisfaction and retention 
- Provide a clear path to success at GitLab for product managers of all levels
- Attract, grow and support talented product managers 
- Establish a robust Learning and development system for Gitlab product managers


## Protocols and Processes

**How we'll collaborate**

In order to stay focused and move quickly on actions, we need to stay _lean_.  Therefore, the working group will be limited to 9 functional leads, 1 facilitator and 1 executive sponsor. The 9 functional leads will then be split into three groups of 3 collaborators, who will each own of of the [problems to solve](#problems-to-solve), as noted above. 

**Creating Epics & Issues for the working group board**

**Reviewing and sharing work**


## Exit Criteria


## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Scott Williamson (Product) | Chief Product Officer |
| Facilitator           | Farnoosh Seifoddini (Product) | Product Operations |
| Functional Lead     | Kevin Chu (Product) | Group Manager |
| Functional Lead       | Mike Karampalas (Product - Growth) | Principal Product Manager |
| Functional Lead       | Mark Wood (Product - Create / Plan) | Senior Product Manager |
| Functional Lead       | Josh Zimmerman (People)) | Learning & Development Manager |
| Functional Lead       | Viktor Nagy (Product - Configure) | Senior Product Manager |
| Functional Lead       | Derek Ferguson (Product - Secure) | Senior Product Manager |
| Functional Lead       | Jacki Bauer (UX) | UX Manager |

## Meetings

Meetings are recorded and available on
YouTube in the [Working Group - FY21 Product Engagement Actions](TBD) playlist. Due to the subject matter of this working group and the high probability that every synchronous meeting will discuss sensitive customer information, the playlist is private and accessible by GitLab team members only.

