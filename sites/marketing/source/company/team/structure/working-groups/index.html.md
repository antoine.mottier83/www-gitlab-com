---
layout: markdown_page
title: "Working Groups"
description: "Like all groups at GitLab, a working group is an arrangement of people from different functions. Learn more!"
canonical_path: "/company/team/structure/working-groups/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's a Working Group?

Like all groups at GitLab, a [working group](https://en.wikipedia.org/wiki/Working_group) is an arrangement of people from different functions. What makes a working group unique is that it has defined roles and responsibilities, and is tasked with achieving a high-impact business goal. A working group disbands when the goal is achieved (defined by exit criteria) so that GitLab doesn't accrue bureaucracy.

## Roles and Responsibilities

### Required Roles

**Facilitator**

Assembles the working group, runs the meeting, assigns action items to Functional Leads, and communicates results      
* Meeting agendas should be organized to stay on topic. Follow up on goals from the previous meeting, and align back to the exit criteria in each meeting. 
* After a long discussion about a topic, try to summarize it and result in an action item - Determine if this topic should be pursued further, or if it changes the exit criteria.
* Assign any actions, initiatives, or outstanding questions to a [DRI](/handbook/people-group/directly-responsible-individuals/) to investigate further. This ensures accountability and prevents overwhelming any single member.
* Consider using an [Issue Board](https://docs.gitlab.com/ee/user/project/issue_board.html) to track working group tasks by using the ~"WorkingGroup::" scoped label on each issue. This can be done separately from any other development related issues that the working group needs to track.

**Executive Stakeholder**

An Executive or [Senior Leader](/company/team/structure/#senior-leaders) interested in the results, or responsible for the outcome

**Functional Lead**

Someone who represents their entire function to the working group, regularly monitors the Working Group Slack Channel, creates issues for action items, serves as DRI for issues created, actively participates in meetings, volunteers for opportunities to further the Working Groups goals, regularly attends meetings either synchronously or asynchronously, shares information learned from the Working Group with their Functional teams, volunteers to take on action items , gathers feedback from Functional teams and brings that feedback back to the Working Group

### Optional Roles

**Member**

Any subject matter expert, attends meetings synchronously or asynchronously on a regular basis, regularly monitors the Working Group Slack Channel, shares information learned from the Working Group with their peers, and gathers feedback from their peers and brings that feedback back to the Working Group

## Guidelines 

* An executive sponsor is required, in part, to prevent proliferation of working groups
* A person should not facilitate more than one concurrent working group
* Generally, a person should not be a part of more than two concurrent working groups in any role
* It is highly recommended that anyone in the working group with OKRs aligns them to the effort


## Process

* Preparation
  * Create a working group page
  * Assemble a team from required functions
    * Share in appropriate Slack channel(s) to encourage a diverse group of participants
  * Create an agenda doc public to the company
  * Create a Slack channel (with `#wg_` prefix) that is public to the company
  * Schedule a recurring Zoom meeting
* Define a goal and exit criteria
* Gather metrics that will tell you when the goal is met
* Organize activities that should provide incremental progress
* Ship iterations and track the metrics
* Communicate the results
  * Consider regular updates to the [#whats-happening-at-gitlab](https://gitlab.slack.com/archives/C0259241C)  slack channel as progress is made towards goal.
  * Communicate outcomes using [multi modal communication](/handbook/communication/#multimodal-communication).
  * Notify widely of exit outcomes via channels such as the [engineering week in review](/handbook/engineering/#communication).
* Disband the working group
  * Celebrate. Being able to close a working group is a thing to be celebrated!
  * Move the working group to the "Past Working Groups" section on this page
  * Update the working group's page with the close date and any relevant artifacts for prosperity
  * Archive the slack channel
  * Delete the recurring calendar meeting

## Participating in a Working Group

If you are interested in participating in an active working group, it is generally recommended that you first communicate with your manager and the facilitator and/or lead of the working group. After that, you can add yourself to the working group member list by creating a MR against the specific working group handbook page.

## Active Working Groups (alphabetic order)

* [Architecture Kickoff](/company/team/structure/working-groups/architecture-kickoff/)
* [Experimentation](/company/team/structure/working-groups/experimentation/)
* [GitLab.com Disaster Recovery](/company/team/structure/working-groups/disaster-recovery/)
* [GTM Product Analytics](/company/team/structure/working-groups/product-analytics-gtm/)
* [IACV - Delta ARR](/company/team/structure/working-groups/iacv-delta-arr/)
* [IC Gearing](/company/team/structure/working-groups/ic-gearing)
* [Internal Feature Flag usage](/company/team/structure/working-groups/feature-flag-usage)
* [Issue Prioritization Framework](/company/team/structure/working-groups/issue-prioritization-framework/)
* [Multi-Large](/company/team/structure/working-groups/multi-large/)
* [Real-Time](/company/team/structure/working-groups/real-time/)
* [Recruiting SSOT](/company/team/structure/working-groups/recruiting-ssot/)
* [S-1 Data Quality Process](/company/team/structure/working-groups/s1-dqp/)
* [SOX PMO](/company/team/structure/working-groups/sox/)
* [Webpack (Frontend build tooling)](/company/team/structure/working-groups/webpack/)
* [Database Scalability](/company/team/structure/working-groups/database-scalability/)

## Past Working Groups (alphabetic order)

* [CI Queue Time Stabilization](/company/team/structure/working-groups/ci-queue-stability/)
* [Commercial & Licensing](/company/team/structure/working-groups/commercial-licensing/)
* [Development Metrics](/company/team/structure/working-groups/development-metrics/)
* [Dogfood Plan](/company/team/structure/working-groups/dogfood-plan/)
* [Engineering Career Matrices](/company/team/structure/working-groups/engineering-career-matrices/)
* [Experimentation](/company/team/structure/working-groups/experimentation/)
* [Githost Migration](/company/team/structure/working-groups/githost-migration/)
* [GitLab.com Cost](/company/team/structure/working-groups/gitlab-com-cost/)
* [GitLab.com Revenue](/company/team/structure/working-groups/gitlab-com-revenue)
* [gitlab-ui (CSS and Components)](/company/team/structure/working-groups/gitlab-ui/)
* [Isolation](/company/team/structure/working-groups/isolation/)
* [Licensing and Transactions Improvements](/company/team/structure/working-groups/licensing-transactions-improvements/)
* [Log Aggregation](/company/team/structure/working-groups/log-aggregation/)
* [Logging](/company/team/structure/working-groups/logging/)
* [Minorities in Tech (MIT) Mentoring Program](/company/team/structure/working-groups/mit-mentoring/)
* [Performance Indicators](/company/team/structure/working-groups/performance-indicators/)
* [Product Analytics](/company/team/structure/working-groups/product-analytics/)
* [Product Development Flow](/company/team/structure/working-groups/product-development-flow/)
* [Secure Offline Environment Working Group](/company/team/structure/working-groups/secure-offline-environment/)
* [Self-Managed Scalability](/company/team/structure/working-groups/self-managed-scalability/)
* [Sharding](/company/team/structure/working-groups/sharding)
* [Simplify Groups & Projects](/company/team/structure/working-groups/simplify-groups-and-projects/)
* [Single Codebase](/company/team/structure/working-groups/single-codebase/)
* [Tiering](/company/team/structure/working-groups/tiering/)
* [Transient Bugs](/company/team/structure/working-groups/transient-bugs/)
* [Upstream Diversity](/company/team/structure/working-groups/upstream-diversity/)

