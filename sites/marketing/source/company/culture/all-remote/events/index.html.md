---
layout: handbook-page-toc
title: "Remote work conferences, summits, and events"
canonical_path: "/company/culture/all-remote/events/"
description: The importance of events in a remote company
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

![GitLab transport illustration](/images/all-remote/gitlab-transport.jpg){: .medium.center}

On this page, we're spotlighting conferences, summits, and events that focus on remote work. This page provides an overview of how to engage and extract maximum value from a virtual event, which may not be immediately intuitive to those who have not yet attended one.

## GitLab remote events

Looking for remote-focused events that GitLab is hosting or participating in? For a running schedule, please bookmark the main [GitLab Events](/events/) page.

## Remote work conferences and summits

Particularly for those who are [seeking a new role with an all-remote or remote-first company](/company/culture/all-remote/jobs/), events can be a great place to meet others who have experience and connections in the space. Below are several growing events that you may consider attending. 

### The Remote Future Summit

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/JV-bV6WZqg0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

> The [Remote Future Summit](https://remote-future.com/) is an annual virtual conference focused on remote work. You can join it from anywhere, as it is a 100% online experience. Explore the tracks of this year’s edition of the Remote Future Summit, from becoming a remote friendly organization to designing processes for distributed teams. You can still access all the contents from the second edition which took place from 15th to 17th of May and was joined by thousands of attendees from all around the world.

### Running Remote

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xaItbbLXh4E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

> [Running Remote](https://runningremote.com/) is carefully curated to teach you next-level, actionable strategies, and tactics you can utilize the very next day to manage and grow your distributed team. Our mission is to provide the education and tools that founders and professionals need to succeed in the future of work. This is a gathering of leaders who will share everything they’ve learned running a remote first organisation.

### The Remote Work Summit

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/3iFCbBT7Xt4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

> [The Remote Work Summit](https://www.theremoteworksummit.com/) is an initiative to bring together 14+ industry leaders from multiple Fortune 500 companies, startups, and agencies and talk about the challenges & frameworks to build effective remote teams, organizations & careers.
>
> In the coming years, most jobs & organizations will go remote. Hiring, on-boarding, company culture & entire operations will be a part of the virtual world. As we reach closer to the future of work, it is essential for all of us to understand the remote work environment in depth, not just as an organization but also as employees seeking flexible opportunities.

### Flex Summit

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/H0KovHDg268" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

> [Flex](https://flexsummit.com/) is the destination summit for leaders engaging in the future of flexible work.
>
> The concept of work is no longer about a place but about being as productive as possible. Flex helps today’s leaders prepare their workplace and workforce for a new way of working across multiple offices, remote locations, and coworking spaces, with blended teams made up of freelancers, contractors, robots, and remote workers.
>
> [Click here](https://www.fuze.com/flex-summit) to watch videos from the sessions and get the presentation slides.

### RemoteCon

> [RemoteCon](https://www.remotecon.org/index.html) is a remote conference on remote work. It will take place on April 7, 2020, using the Zoom video conferencing system. RemoteCon is organized by [Marcel Salathé](https://twitter.com/marcelsalathe) and a group of volunteers at EPFL who believe that remote is not only changing how we work, but also how society organizes itself. Getting remote work right is thus one of the biggest challenges of the 21st century. It’s time to talk about it.

## The power of networking

Networking takes additional consideration in the all-remote space, given that people congregate in the same physical space less often. This creates opportunity for conferences and summits to bring remote workers from all over the globe together, serving as a catalyst for collaboration, [communication](/company/culture/all-remote/informal-communication/), and inspiration. 

Those [seeking a remote role](/company/culture/all-remote/hiring/) can learn about emerging and established firms in the all-remote and remote-first space, channeling their energy to [apply at companies that they are aligned with](/company/culture/all-remote/jobs/). 

If you're a founder or hiring manager, these events offer a platform to promote your company to potential candidates who understand and appreciate the [benefits](/company/culture/all-remote/benefits/) and [drawbacks](/company/culture/all-remote/drawbacks/) of all-remote.

## Purpose and mission

As remote work becomes more popular, a number of conferences and summits are emerging to serve a few key purposes.

1. Educate the world on the [power](/company/culture/all-remote/benefits/) and [efficiency](/handbook/values/#efficiency) of working remotely, including those who have not yet embraced remote work.
1. Bring remote workers and companies together to fuel [collaboration](/handbook/values/#collaboration) and share learnings.
1. Provide a platform for experts to share knowledge with the [broader remote work community](/company/culture/all-remote/jobs/).
1. Create a tighter, more unified network of all-remote and remote-first advocates to evangelize for the creation of [more remote roles](/company/culture/all-remote/hiring/).

## Experiencing a remote work conference

![GitLab collaboration illustration](/images/all-remote/gitlab-collaboration.jpg){: .medium.center}

By and large, conventional conferences and summits require your physical presence for maximum impact. Networking, learning, and discussions all take place face-to-face, in a shared physical space. If you can't make it, you miss out. 

Select in-person conferences record snippets of panels and keynote speeches for viewing after the conference has ended, but do not offer a seamless way for dialog to continue. If you tune in after the event, the flow of information is unidirectional, and it's difficult to engage in a meaningful way. 

Remote work summits do not shun or look down on those who work in colocated organizations. The beauty of remote work events is that everyone is welcome, even those who may be skeptical of remote work or have negative experiences. 

All-remote and remote-first workflows change rapidly. Technology, tools, mobile network infrastructure, internet ubiquity, and a cultural understanding of virtual communication all contribute to making remote work [increasingly feasible](/company/culture/all-remote/benefits/). Those who have a [negative](/company/culture/all-remote/drawbacks/) view of remote work, but are curious to see how the space is [evolving](/company/culture/all-remote/part-remote/), stand to gain much from attending remote work conferences and summits. 

### Virtual attendance

Conferences and summits focused on remote work, however, are typically structured differently. While some may have an in-person element (such as [Running Remote](https://runningremote.com/)), there's also a virtual attendance option. 

Given that remote work summits are engineered to be open to anyone, anywhere in the world, livestreams are a vital part of ensuring that a global community can engage with content, speakers, panelists, and keynotes.

### Benefits for speakers and presenters

Speakers and presenters can present from anywhere. They may be on a stage with others in a shared physical space, or they may be presenting from anywhere in the world via computer and webcam. By removing the need to travel and carve out large chunks of one's schedule, the virtual nature of a remote work conference creates a significant opportunity for summit hosts to capture a [more diverse](/company/culture/inclusion/) array of speakers.

### Asynchronous engagement

A major boon of remote work summits is the ability to engage [asynchronously](/handbook/communication/). Even if you're unavailable for the entirety of the live event, all-remote and remote-first events are typically structured to be experienced at your leisure. For example, the prior year of [The Remote Future Summit](https://remote-future.com/) can still be experienced now, as all sessions were captured and archived for future viewing. 

### Building a community

Moreover, the remote community typically welcomes outreach from others working remotely. If you view a session that contributed to your knowledge and understanding of an issue, it's not out of line to reach out to the speaker(s) who presented via email, Twitter, etc. and begin a dialog — even months after the live event concludes. 

----

Return to the main [all-remote page](/company/culture/all-remote/).
