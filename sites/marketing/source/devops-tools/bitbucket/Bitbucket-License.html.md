---
layout: markdown_page
title: "Bitbucket License Overview"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## Bitbucket vs. GitLab Pricing (Cloud)

|                                                                   | <br>Free                    | <br>Bitbucket<br>Standard   | <br>Bitbucket<br>Premium    | GitLab<br>Bronze  |
|-------------------------------------------------------------------|-----------------------------|-----------------------------|-----------------------------|-------------------|
| <br>Cost                                                          | $0 <br>user/month           | $3 <br>user/month           | $6 <br>user/month           | $4 <br>user/month |
| <br>Users                                                         | <br>5 or less               | <br>Unlimited               | <br>Unlimited               | <br>Unlimited     |
| <br>Build minutes/month                                           | <br>50                      | <br>2500                    | <br>3500                    | <br>2000          |
| <br>CI/CD                                                         | <br>Yes                     | <br>Yes                     | <br>Yes                     | <br>Yes           |
| <br>Unlimited Private Repos                                       | <br>Yes                     | <br>Yes                     | <br>Yes                     | <br>Yes           |
| Issue Management/<br>Team Collaboration                           | Jira and Trello Integration | Jira and Trello Integration | Jira and Trello Integration | Yes <br>Native    |
| Code Insights <br>(Visibility into insights from 3rd party tools) | 3 <br>integrations          | <br>Unlimited               | <br>Unlimited               | <br>Unlimited     |
| <br>Smart Mirroring                                               | No                          | No                          | Yes                         | Yes               |
| <br>Deployment Permissions                                        | No                          | No                          | Yes                         | No                |
| Merge Checks/<br>Enforced Merge Checks                            | <br>Yes/No                  | <br>Yes/No                  | <br>Yes/Yes                 | <br>Yes/Yes       |

* GitLab Bronze/Starter offers
    * Native Issue Management
    * Native Team Collaboration Features 
    * Remote repository mirroring 
    * Built in Container Registry
    * Native Analytics
    * Forced Merge Approvals



## Bitbucket Pricing (Self-Managed)

|                 | Server    | Data Center |
|-----------------|-----------|-------------|    
| License Type    | Perpetual | Annual Term |     
| 25 user Cost    | $2,900    | $1,980      |
| 50 user Cost    | $5,200    | $3,630      |
| 100 user Cost   | $9,500    | $6,600      |
| 250 user Cost   | $19,000   | $13,200     |
| 500 user Cost   | $25,300   | $17,600     |
| 1,000 user Cost | $35,000   | $26,400     |
| 2,000 user Cost | $69,800   | $52,800     |

* Perpetual license
    * Includes 1 year of maintenance
    * Additional maintenance cost must be considered after year 1
    * Includes 1 server only
* Data Center
    * Includes Smart Mirroring
    * Includes Disaster Recovery
    * Gets very costly as user count reduces


