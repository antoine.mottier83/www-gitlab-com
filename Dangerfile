# frozen_string_literal: true

require_relative './lib/team/yaml'

# Only gitlab-bot is allowed to edit data/team.yml
bot_user = ENV['TEAM_YML_UPDATE_BOT_USER']

# Using the API, as git.modified_files doesn't work well with shallow clones
files = gitlab.api.merge_request_changes(gitlab.mr_json['project_id'], gitlab.mr_json['iid'])
modified_files = files.to_h["changes"].select { |file| !file['deleted_file'] } .map { |file| file['new_path'] } # rubocop:disable Style/InverseMethods

if modified_files.include?('data/team.yml') && gitlab.mr_author != bot_user
  msg = <<~MSG
    Merge requests are not allowed to contain changes to data/team.yml

    Our team database has been split into individual files. For more
    information please see: doc/team_database.md

    Instead please edit the individual files under data/team_members
  MSG

  failure msg
end

begin
  yaml = Gitlab::Homepage::Team::Yaml.new
  yaml.verify_members!
rescue Gitlab::Homepage::Team::InconsistentTeamError => e
  failure(e.message)
end

# vim: filetype=ruby
