<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process. Unless you are just suggesting a blog post idea, you will need to create the formatted merge request for your blog post in order for it to be reviewed and published. -->

## Triage (REQUIRED)

The Inbound Marketing team prioritizes requests that drive results and meet our [goals of generating organic traffic and inquiries](https://about.gitlab.com/handbook/marketing/inbound-marketing/#goals). 

Please note that there is a different process for when you want to announce something via the blog. Please see [announcement requests](https://about.gitlab.com/handbook/marketing/corporate-marketing/#requests-for-announcements) in the handbook and open an announcement request issue instead of a blog post issue. 

Generally speaking, engineering blog posts that are tutorials/how-tos or which share how we built or debugged something, are popular with our audience. **If your proposed blog post is aligned with our [Attributes of a successful blog post guidelines](https://about.gitlab.com/handbook/marketing/blog/#attributes-of-a-successful-blog-post), you can skip straight to your [proposal](#proposal).** 

If you are pitching something outside of those guidelines, please fill in the below to help us prioritize. You can check out [examples of high- and low-performing blog posts to help with your rationale](https://about.gitlab.com/handbook/marketing/inbound-marketing/content/editorial-team/#top-performing-blog-posts). 

**This issue fulfills one of these goals:** <!--(Requestor check one)-->
- [ ] Drive traffic to our website
- [ ] Convert traffic into leads
- [ ] Thought leadership/share expertise
- [ ] Build relationships with potential customers
- [ ] Drive long-term results (please explain below in your proposal)
- [ ] Announcement 
  - `Link to announcement request issue (required, see https://about.gitlab.com/handbook/marketing/corporate-marketing/#requests-for-announcements)`
- [ ] Cross-functional support 
   - `Link to OKR (required if this box is checked)` 
   
## Proposal

<!-- What do you want to blog about? Add your description here. If you are making an announcement, please open an `announcement` request issue in the Corporate Marketing project instead: https://about.gitlab.com/handbook/marketing/corporate-marketing/#requests-for-announcements -->

## Roles and Responsibilities 
| Person | Role |
| ------ | ------ |
| `@person` | requestor | 
| `@person`| editor |
| `@person` | approver (optional) | 

### Checklist

- [ ] If you have a specific publish date in mind (please allow 3 weeks' lead time)
  - [ ] Include it in the issue title and apply the appropriate weekly milestone (e.g. `Fri: Mar. 5, 2021`)
  - [ ] Give the issue a due date of a minimum of 2 working days prior
  - [ ] If your post is likely to be >2,000 words, give a due date of a minimum of 4 working days prior
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#process-for-time-sensitive-posts)
  - [ ] Add ~"Blog: Priority" label and supplied rationale in description
- [ ] If wide-spread customer impacting or sensitive, mention `@nwoods` to give her a heads up ASAP, apply the ~"sensitive" label, and check the [PR handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/#requests-for-announcements) in case you need to open an announcement request instead of a blog post issue
- [ ] If the post is about one of GitLab's Technology Partners, including integration partners, mention `@arashidi`,  apply the ~"Partner Marketing" label, and see the [blog handbook for more on third-party posts](https://about.gitlab.com/handbook/marketing/blog/index.html#third-party-posts)
- [ ] If the post is about one of GitLab's customers, mention `@FionaOKeeffe`, apply the ~"Customer Reference Program" label, and see the [blog handbook for more on third-party posts](https://about.gitlab.com/handbook/marketing/blog/index.html#third-party-posts)
- [ ] Indicate if supporting an event or campaign
- [ ] Indicate if this post requires additional approval from internal or external parties before publishing (please provide details in a comment)

## Production 
- [ ] Requestor to complete issue template (Triage, Proposal, Roles and Responsibilities, Checklist )
- [ ] Issue sent through triage for consideration (approve, backlog, deny) 
- [ ] Issue assigned to requestor to draft blog post and open MR
- [ ] MR created and linked to issue - issue is now deprecated in favor of MR and will close once MR is complete

/label ~"blog post" ~"Blog::Pitch" ~"mktg-inbound" ~"mktg-content" ~"IM-Support" ~"mktg-status::triage"
