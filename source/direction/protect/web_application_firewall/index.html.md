---
layout: secure_and_protect_direction
title: "Category Direction - Web Application Firewall"
description: "By being able to see what is being sent to your systems, GitLab wants to empower you to either block malicious traffic or to otherwise act on it."
canonical_path: "/direction/protect/web_application_firewall/"
---

- TOC
{:toc}


| | |
| --- | --- |
| Stage | [Protect](/direction/protect) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2021-02-25` |

### Overview
A web application firewall (WAF) filters, monitors, and blocks web traffic to and from a web application. A WAF is differentiated from a regular firewall in that a WAF is able to filter the content of specific web applications, while regular firewalls serve as a safety gate between servers. By inspecting the contents of web traffic, it can prevent attacks stemming from web application security flaws, such as SQL injection, cross-site scripting (XSS), file inclusion, and security misconfigurations.

GitLab's WAF comes with a default out-of-the-box OWASP ruleset in detection only mode.

### Where we are Headed

GitLab's WAF is now [deprecated](https://gitlab.com/gitlab-org/gitlab/-/issues/271276) as of our %13.6 release and will be [removed](https://gitlab.com/gitlab-org/gitlab/-/issues/271349) from the product in our %14.0 release.  Please reference [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/271276) for additional details.
