---
layout: markdown_page
title: Product Direction - Fulfillment
description: "The Fulfillment Team at GitLab focus create and support the enablement of our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Last reviewed: 2020-12-22

## Fulfillment Section Overview

### Composition

The Fulfillment Section is comprised of the [Purchase](#Purchase), [License](#License) and [Utilization](#Utilization) groups. These groups are organized to create clear DRIs for key systems internally, and for clear boundaries between customer experiences. We've [only recently established](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/67179) these groups and will be firming up their individual direction pages, along with all other handbook artifacts and metadata after the new year. 

Additional pages of interest:

- ##### [Fulfillment internal Release post - check out what we've recently shipped and what's on deck](https://about.gitlab.com/direction/fulfillment/releases/)
- ##### [Purchase group direction](https://about.gitlab.com/direction/fulfillment/purchase/)
- ##### [License group direction](https://about.gitlab.com/direction/fulfillment/provision/)
- ##### [Utilization group direction](#) (coming soon)

## Overview

The Fulfillment section focuses on creating and supporting experiences that enable our customers to purchase, upgrade, downgrade, and renew licenses and subscriptions. Additionally we support account and subscription management (adding and removing licenses), along with consumption purchases (currently CI minutes and storage). We are constantly striving to make the billing and payment processes simple, intuitive, and stress-free for our customers and GitLab team members. We believe that engaging in business transactions with GitLab should be minimally invasive and when noticed, should be a positive, empowering, and user-friendly experience.

As we mature this section over the coming quarters we aim to reduce friction for customers by enabling them to self-service as much as possible all while reducing manual intervention from our sales and support functions empowering them to spend more time working directly with our customers on more strategic activities. 

We also partner with the [Growth Groups](https://about.gitlab.com/direction/#growth-groups) to enable experimentation and product improvements focused on making it easy for people to try, buy, grow, and stay with GitLab.

## Groups

#### Purchase
The Purchase group is responsible for all purchase experiences. Trials, purchase flows, and account management (invoices, contact, billing info). This includes payments for new customers along with trials. This group is also responsible for the purchase transitions and communication between our storefront and Zuora. Systems: [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot), [GitLab](/handbook/engineering/development/fulfillment/architecture/#gitlab). Integrations: [Zuora](/handbook/engineering/development/fulfillment/architecture/#zuora), [Stripe](/handbook/engineering/development/fulfillment/architecture/#stripe).

#### License
The Provision group is responsible for all of the post-purchase activities that occur behind the scenes when a customer purchases new seats or modifies existing licenses. This includes retrieving and managing licenses along with integrations to back of house tools. Systems: [LicenseDot](/handbook/engineering/development/fulfillment/architecture/#licensedot), Salesforce.

#### Utilization
The Utilization group is responsible for all consumables management, usage reporting, and usage notifications (excluding license utilization). [GitLab](/handbook/engineering/development/fulfillment/architecture/#gitlab).

## Performance Indicators

_we'll be iterating on the organization of these PIs as we further establish our groups, for now they are all tracked uniformly across the entire section_

**[Percentage of transactions through self-service purchasing](https://about.gitlab.com/handbook/product/performance-indicators/#percentage-of-transactions-through-self-service-purchasing)**

**[Percentage of SMB transactions through self-service purchasing](https://app.periscopedata.com/app/gitlab/779889/WIP:-Percentage-of-SMB-transactions-through-self-service-purchasing?widget=10313311&udv=0)**

**[CI Minutes Transactions and Revenue](https://app.periscopedata.com/app/gitlab/744221/CI-minutes-Pricing-Initiative)**

**[Storage Consumption](https://app.periscopedata.com/app/gitlab/768903/GitLab-Storage)**

## Recent Accomplishments

1. Launched consumption offerings (CI Minutes & Storage)
1. [Renewals now process at list price (vs the original discount price)](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/790)
1. [All invoices are now available at customers.gitlab.com](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/503)
1. [Ability to purchase and automatically allocate storage](https://gitlab.com/groups/gitlab-org/-/epics/4663)
1. [Allow for user growth while processing an order](https://gitlab.com/gitlab-org/license-gitlab-com/-/issues/184)

## Upcoming Work

#### [Tiering Project](https://about.gitlab.com/company/team/structure/working-groups/tiering/)
You can learn more about this project [here](https://gitlab.com/groups/gitlab-org/-/epics/4838) through our slack channel [here](https://gitlab.slack.com/archives/C01DQ3BUYFN).

#### [Cloud Licensing](https://gitlab.com/groups/gitlab-org/-/epics/1878)
Today's process for managing licenses of a self-managed instance is less than optimal. You need to login to customers.gitlab.com to purchase, renew and access your license key - then manually configure your instance to accept that new key. We want to make this process as painless and seamless as possible. 

Our plan is to enable a license sync system that will allow customers to regularly sync their license and user counts with GitLab to streamline purchases and renewals as well as enable us to provide more flexible billing options for our customers.

At the same time, we understand that not all of our customers are running instances that can connect to the internet or have requirements against sharing any kind of information. In those cases, we need to ensure customers have the option to continue to purchase and apply licenses as they do today, but with as minimal friction as possible.

#### [Quarterly Co-Terms](https://gitlab.com/groups/gitlab-org/-/epics/2747)
We've heard feedback from our customers and sales team members that GitLab's current [true up](https://about.gitlab.com/handbook/ceo/pricing/#true-up-pricing) process is not ideal. To improve we're looking to move to "co-term" seat growth on a quarterly basis. This means that instead of being billed for add on users at the end of your term, for the entire term we'd reconcile the add on users once a quarter. You can find more details in the linked epic, we're optimistic that this improvement is a great first step as we iterate on making our billing and add-on user process more transparent to our customers. 

#### [Support for self service transactions and reduction in manual intervention](https://gitlab.com/gitlab-org/growth/product/-/issues/1624)
Today many actions related to a customers account require manual intervention. This could be as simple as configuring a trial, or as complex as handling add on seats mid term. We aim to reduce the tasks the require manual intervention as much as possible. Doing so enables customers to help themselves quickly, without engaging with sales. It also enables our sales and support teams to free themselves up to use their time on higher value activities and more complicated deals or customer issues. This work is comprised of many individual tasks. You can find the work tracked against the linked issue. 

## High Level Themes

_The below themes are groupings of work the team has been focused on over the past few quarters. We'll likely divide this out as we further establish our groups and onboard new PMs._

### Making our billing more transparent

We don't want our customers to be surprised about billing and user management. Customers should be fully aware when they're taking an action that will require additional payment.

* [Send a monthly user digest to subscription management contacts monthly to monitor seat utilization at a glance.](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/1344)
* [Create an in-app banner to notify admin when the subscribed seat count is approaching the limit.](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/1210)
* [Display an in-app banner to notify admin when the subscribed seat count has exceeded the limit.](https://gitlab.com/gitlab-org/gitlab/-/issues/215959)

### Improving user information for admins and group owners

We want customers to easily understand what (and who) they're paying for. Over the next two quarters we will provide instance admins and group owners with tools to find the users they are being billed for.


### Improving the user experience of our CustomersDot

The CustomersDot should be easy-to-use and provide a consistently delightful experience that matches our other offerings.

[The CustomersDot should use our Pajamas design system](https://gitlab.com/groups/gitlab-org/-/epics/1788)


## Principles

* Make it easy and simple as possible to purchase, upgrade, and renew a GitLab license or subscription
* Build and support a robust billing and licensing experience that is flexible and allows us to continue to iterate on our pricing, packaging, and product and service offerings
* Provide transparent, clear, and easy-to-understand information to admins and group owners about their users, usage, and corresponding costs
* Automate as much of the billing process as possible so our GitLab team members can focus their time on providing excellent service and support to our customers
* Upholding our compliance, privacy, and security standards


## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/product-processes/#how-we-prioritize-work). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=section%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!
