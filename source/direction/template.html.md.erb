---
layout: markdown_page
title: "GitLab Direction"
description: "Our vision is to replace disparate DevOps toolchains with a single application that is pre-configured to work by default across the entire DevOps lifecycle."
canonical_path: "/direction/"
extra_css:
  - maturity.css
  - direction.css
---

## On this page
{:.no_toc}

- TOC
{:toc}

This page introduces the GitLab product vision, where we're headed over the next few years, and our plan to deliver over the next year.

## Vision

Our vision is to replace DevOps toolchains with a
[single](/handbook/product/single-application/) [DevOps platform](/topics/devops-platform/) that is
pre-configured to work by default across the entire DevOps lifecycle. We believe our vision is shared by market analysts who have coined it the [Value Stream Delivery Platform market](/analysts/gartner-vsdp20/).

Many organizations are in the midst of evolving from classic development
paradigms to DevOps. They want faster cycle time, improved efficiency, and
reduced risk. GitLab’s purpose is to help those organizations accelerate their
DevOps journey.  Examples of how GitLab does this include:
* Streamlining workflows for individuals and teams through a [single application experience](/handbook/product/single-application/) that spans the entire DevOps lifecycle
* Shifting security testing left so issues are found earlier
* Enabling toolchain consolidation and simplifications by including all ten stages of the DevOps lifecycle
* Enabling digital transformation through Infrastructure as Code, native Kubernetes support, and cloud portability
* Accelerating deployment times through tightly integrated source code management, continuous integration, and continuous delivery
* Improving the completeness of product feedback loops through progressive delivery and integrated incident management  

GitLab facilitates remote work transformations as highlighted in this customer's [story](https://medium.com/tangram-visions/making-remote-work-work-with-gitlab-ffab5b0b6697), enabling people to commute less, spend more time with family and friends, and get great jobs from anywhere.  Using GitLab as a single DevOps platform reduces the toil of working across multiple systems, which can otherwise lead to silos and lack of transparency. GitLab also improves working relationships between teammates by improving collaboration, and enables higher quality software development projects which increases job satisfaction.  As evidence of this, one of our customers told us they use GitLab as a recruiting tool, suggesting that while the recruit may make more money elsewhere, their professional quality of life will be higher in a software development environment leveraging GitLab.

We execute on our vision rapidly and efficiently by leveraging the best practices of 100,000 organizations co-developing the DevOps platform of their dreams. We take a
[seed then nurture](/company/strategy/#seed-then-nurture) approach to [maturing our product surface area over time](/direction/maturity/), all the while,
focusing on customer results. We leverage [sensing mechanisms](/handbook/product/product-processes/#sensing-mechanisms) and [product usage data](/handbook/product/performance-indicators/) to make decisions about where to increase or decrease [investment](/handbook/product/investment/). You can read more about the principles that guide our prioritization and product thinking in
our [product handbook](/handbook/product/product-principles/)

### Trend Towards Consolidation

Continuing apace after Microsoft's 2018 [acquisition of GitHub](https://blogs.microsoft.com/blog/2018/10/26/microsoft-completes-github-acquisition/), the trend to consolidate DevOps companies seems here to stay. In January 2019, [Travis CI was acquired by Idera](https://techcrunch.com/2019/01/23/idera-acquires-travis-ci/), and in February 2019 we saw [Shippable acquired by JFrog](https://techcrunch.com/2019/02/21/jfrog-acquires-shippable-adding-continuous-integration-and-delivery-to-its-devops-platform/). Atlassian and GitHub now both bundle CI/CD with SCM, alongside their ever-growing related suite of products. In January 2020, [CollabNet acquired XebiaLabs to build out their version of a comprehensive DevOps solution](https://xebialabs.com/company/press/collabnet-versionone-and-xebialabs-combine-to-create-integrated-agile-devops-platform/).

It's natural for technology markets go through stages as they mature: when a young technology is first becoming popular, there is an explosion of tools to support it. New technologies have rough edges that make them difficult to use, and early tools tend to center around adoption of the new paradigm. Once the technology matures, consolidation is a natural part of the lifecycle. GitLab is in a fantastic position to be ahead of the curve on consolidation, as we're already considered a representative vendor in this [newly defined market](/analysts/gartner-vsdp20/), but it's a position we need to actively defend as more competitors start to bring legitimately integrated products to market.

### DevOps Stages

<%= devops_diagram(["All"]) %>

<!-- ![Product Categories](categories.png) -->

DevOps is a broad space with a lot of complexity. To manage this within GitLab,
we break down the [DevOps lifecycle](https://en.wikipedia.org/wiki/DevOps_toolchain)
into a few different [sections](/handbook/product/categories/#hierarchy), each with its own direction page you can review.

<% data.sections.each do |sectionKey, section| %>
  <% line = "* **[#{section.name} Section Direction](#{section.direction})** - Includes the " %>
  <% stages = data.stages.stages.select{|stageKey, stage| stage.section==sectionKey} %>
  <% stageLinks = stages.map { |stageKey,stage| "[#{stage.display_name}](#{stage.direction})" } %>
  <% line += stageLinks.to_sentence + " " + "stage".pluralize(stages.count) %>
  <% if stages.count == 1 then %>
    <% stages.each do |stageKey, stage| %>
      <% groups = stage.groups.map { |groupKey, group| "[#{group.name}](#{group.direction})" } %>
      <% line += ", which includes the " + groups.to_sentence + ' ' + "group".pluralize(groups.count) %>
    <% end %>
  <% end %>
<%= line %>
<% end %>

We are investing [in the following manner](/handbook/product/product-processes/#rd-investment-allocation) across each stage.

## 3-year Strategy

### Situation
GitLab competes in a large market space, with a [TAM](/handbook/product/investment/#total-and-service-addressable-markets) estimated at ~$18B in 2024. GitLab has recently surpassed the $150M ARR milestone, with unusually high revenue growth and retention rates.  GitLab is uniquely positioned in the market with a vision to offer a single application for the entire DevOps lifecycle.  GitLab competes across numerous market segments and aims to deliver value in [80+ market categories](/direction/maturity/#category-maturity). GitLab’s product vision is uniquely ambitious, as we were the first DevOps player to take a [single application approach](/single-application/). From idea to production, GitLab helps teams improve cycle time from weeks to minutes, reduce development process costs, and enable a faster time to market while increasing developer productivity. With software “eating the world,” this is widely viewed as a mission-critical value proposition for customers.  We also have a number of [tailwinds](/handbook/leadership/biggest-tailwinds/) in the form of cloud adoption, Kubernetes adoption, and DevOps tool consolidation, which are helping fuel our rapid growth.  Finally, GitLab has an open source community and distribution model, which has exposed the value of GitLab to millions of developers and has sped up the maturation of our product through [more than 200 monthly improvements](https://gitlab.biterg.io/goto/937475d38035f496df3501c9b30af5ef) to the GitLab codebase from our users.

### Strategic Challenges
1. **Tension between Breadth and Depth:** GitLab’s ambitious single application product vision means we need to build out feature function value across a very large surface area.  Our challenge is to drive the right balance between breadth and depth in our product experience.  In recent years, we have optimized for breadth. To win and retain more sophisticated enterprise customers, we need to effectively [seed then nurture](/strategy/#seed-then-nurture) in areas of the product that generate usage and revenue.  With so much product surface area to deliver in a single application experience, it is a big UX challenge to keep the experience simple, consistent, and seamless between DevOps phases.  
1. **GitLab.com and Self-Managed:** Another challenge we face is the balance between our self-managed and GitLab.com offerings.  GitLab's early paying customers were more interested in self-managed, and the majority of our customers use this offering today.  As a result, we focused heavily on delivering a great self-managed customer experience.  However, as the market shifts toward cloud adoption, we are seeing an increasing demand for our GitLab.com offering.  We now need to rapidly meet the same enterprise-grade security, reliability, and performance expectations our paying customers have come to expect from self-managed in our SaaS (.com offering).  
1. **Wide Customer Profile:** We also serve a wide range of customers, from individual contributor developers to large enterprises, across all vertical markets.  This range of deployment options and customer sizes makes our business complex and makes it hard to optimize the customer experience for all customer sizes.  Over the past few years, we have prioritized enabling our direct sales channel, but in the process have not focused enough on great customer experiences around self-service purchase workflows, onboarding, and cross-stage adoption.  
1. **Competition:** Finally, we have formidable competition from much larger companies, including Microsoft, Atlassian, and Synopsys to name a few.  Microsoft is starting to mimic our single application positioning, and while behind us in the journey, have substantial resources to dedicate to competing with GitLab.

### Strategic Response
1. **Focus on increasing Combined Monthly Active Users [(CMAU)](/handbook/product/performance-indicators/#combined-monthly-active-users):** There is a strong correlation between the number of stages customers use and their propensity to upgrade to a paid package.  In fact, **adding a stage triples conversion!**  Each product group should be laser focused on driving adoption and regular usage of their respective stages, as it should lead to higher [IACV](/handbook/sales/#incremental-annual-contract-value-iacv), reduced churn, and higher customer satisfaction.  See [this graph in Sisense, which shows the correlation increasing Stages per Namespace has with paid conversion](https://app.periscopedata.com/app/gitlab/608522/Conversion-Dashboard?widget=7985190&udv=0).
    <embed width="100%" height="400" src="<%= signed_periscope_url(chart: 7985190, dashboard: 608522, embed: 'v0') %>">
    As outlined in this [user journey](/handbook/product/product-principles/#multi-feature-usage-adoption-journey), the most important additional stages for customers to adopt are Create to Verify and Verify to Release, as each of these adoption steps open up three additional stages to users.  
1. **Deliver cross-stage value:**  GitLab’s primary point of differentiation is our single application approach.  As we continue to drive value in any given stage or category, our first instinct should be to connect that feature or product experience to other parts of the GitLab product.  These cross-stage connections will drive differentiated customer value and will be impossible for point product competitors to imitate.  Recognizing this opportunity, we have grown our R&D organization significantly over the past two years, and plan to invest an outsized amount on R&D for the next 2-3 years to extend our lead in executing against the single application product vision.  
1. **Leverage open source to rapidly achieve multi-stage product adoption.**  Our vision is to deliver a single application for the entire DevOps lifecycle.  To achieve this ambitious vision more quickly, we will leverage our powerful open source community.  Each stage should have a clear strategy for tiering the value of the stage.  When stages are early in maturity, we will bias toward including as much functionality in our Core open source version as possible, to drive more rapid adoption and greater community contributions, which will help us mature new stages faster.  Once stage adoption is achieved, we can then layer on additional value in paid tiers to encourage upgrades.
1. **Shift to depth in core product areas:**  We want to ensure the core product usage experience is great, which will lead to more paying customers and improved customer retention.  We intend to maintain our market-leading depth in stages with lovable categories, which currently are Verify (Continuous Integration) and Create (Source Code Management and Code Review).  Beyond that, we will endeavor to rapidly mature our offering to lovable in Plan (3rd most used stage), Release (4th most used stage), and Secure (important element of our Ultimate tier).  Our [goal](/company/strategy/#sequence) is to have 50% of our categories at lovable maturity by the end of 2023.
1. **Optimize for SaaS and self-service:**  We have millions of overall users, hundreds of thousands of paying users, and tens of thousands of paying organizations.  Given this volume of adoption, it’s essential that we assume users will need to serve themselves to buy and use GitLab.  This will enable us to shift a higher percentage of our customer base to a lower-cost, self-service buying channel, reducing sales and support costs, and improving our customer acquisition costs.  It should also lead to higher customer satisfaction overall, as assuming customers will buy and use GitLab without assistance from sales and support will force us to keep our UX quality bar very high.  We should also assume that over time a majority of our customers will prefer a SaaS delivery model, so our SaaS offering needs to have enterprise-grade security, availability, and performance. We must also ensure feature parity between self-managed and GitLab.com and that customers have an easy migration path from self-managed to GitLab.com.

## Personas

[Personas](/handbook/marketing/strategic-marketing/roles-personas/) are the people we design for. We’ve started down the path of having developers, security professionals, and operations professionals as first-class citizens; letting each person have a unique experience tailored to their needs. We want GitLab to be the main interface for all of these people. Show up at work, start your day, and load up GitLab. And that’s already happening.

But there are a lot of other roles involved with the development and delivery of software. That is the ultimate GitLab goal - where everyone involved with software development and delivery uses a single application so they are on the same page with the rest of their team. We are rapidly expanding our user experience for [Designers](/handbook/marketing/strategic-marketing/roles-personas/#presley-product-designer), [Compliance Managers](/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager), [Product Managers](/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager), and [Release Managers](/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager). We’ll also be expanding to the business side, with Executive visibility and reporting. While we’re still calling it DevOps, we’re really expanding the definition of DevOps and delivering it all as a single application.

## FY22 Product Investment Themes

Every year at GitLab, we choose some specific areas of emphasis to help guide the teams on the directional themes that we want to accentuate in our product. This section is used to highlight that emphasis. It is not a comprehensive list of everything we plan to do this year. Direction for each stage and category can be found at the respective [direction pages](https://about.gitlab.com/handbook/product/categories/). We are not asking the teams to deviate from their core mission.

Many teams will see themselves contributing to these areas of emphasis directly. The other teams will continue to execute on their mission - that is also important.

The themes are to help facilitate cross-team collaboration when invariably teams working on the 1-year themes may need to collaborate with others. Our guidance is: if any team approaches you to prioritize something that is thematic for this year, consider that as a higher priority than you would normally - as it is in service of the broader product-wide goal that we, as a company, have deemed important to accomplish this year.

For FY22, the 3 key product themes we are focused on are:

### Application Security Testing leadership

_Performance Indicator: Total user growth for Ultimate_

As part of our land-and-expand strategy, we consider Application Security Testing (AST) a critical land play after SCM and CI. We will take a [leadership position within the AST market](https://about.gitlab.com/direction/security/#what-we-are-currently-working-on) as we build on our FY21 success and extend the AST offering to meet the most demanding customer requirements. 


#####  Execution Plan for _AST Leadership_

We will establish a leadership position within the Application Security Testing (AST) market by moving Static Analysis (SAST), Dynamic Analysis (DAST), and Dependency Scanning categories to Complete maturity, as well as moving Vulnerability Management to Viable maturity. In addition, we will introduce a Fuzz Testing category and bring it to Viable maturity.

##### AST Leadership benefits for our customers and why we will win

Our single application for the DevSecOps lifecycle allows Developers and Security professionals to collaborate in ways natural to them, but with unprecedented effectiveness. Developers can find vulnerabilities at the time of code commit, making it easier to fix them without context switching. This leads to acceptance and adoption by developers. In addition, Security professionals gain visibility into vulnerabilities at the time the code is committed and when modifications, approvals, and exceptions are made. They can also enforce security policies in the merge request flow. 

The end result is that organizations that adopt GitLab benefit by: 

* [Lower cost of remediation](https://about.gitlab.com/direction/security/#lowering-the-cost-of-remediation)
* Naturally shift security left
* Broader and deeper testing than before with new scan types
* Support for more languages and frameworks 
* Ability to find unknown vulnerabilities in the projects
* A systematic method towards better security and risk visibility across their organization
* Ability to protect Kubernetes deployments 


### Adoption through usability

_Performance Indicator: Increase in [Predicted TMAU](https://about.gitlab.com/handbook/product/performance-indicators/#estimated-combined-monthly-active-users)_

The key to unlocking the power of a single application is to have customers experience the value of multiple stages working together. We believe that improving usability is our primary path to unlocking multi-stage adoption.  

##### Execution Plan for _Adoption through Usability_ 

We will focus on making features more discoverable, working by default, and usable. If we can do this successfully, new users will be more likely to buy, current customers will be more likely to add seats and uptier, and the customers will maximize the benefit they can extract from using GitLab.

Key areas of focus:

* [Convention over Configuration](https://about.gitlab.com/handbook/product/product-principles/#convention-over-configuration)
* [Configuration Principles](https://about.gitlab.com/handbook/product/product-principles/#configuration-principles), when required
* [Simplifying Projects and Groups](https://about.gitlab.com/direction/plan/#unify-projects-and-groups)
* [Principled adherence to our established knowledge architecture](https://about.gitlab.com/handbook/product/product-principles/#principled-adherence-to-the-established-knowledge-architecture)
* [Researching and improving key cross-stage moments](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10466#note_499059124)
* [Quarterly OKRs to drive improvements in the key usability themes](https://gitlab.com/gitlab-com/Product/-/issues/2099)
* [Redesigning Auto DevOps to enable easy stage onboardings](https://gitlab.com/gitlab-org/gitlab/-/issues/301065)
* [Integrating Incidents into common workflows](https://gitlab.com/groups/gitlab-org/-/epics/5355)

##### Usability benefits for our customers and why we will win

A single application for the DevOps vision isn’t new. What is new is our approach to it. Others who attempt this try to stitch together point products after the fact (either through M&A or a “solution kit” exercise). 

The key difference between stitching point products together vs a single application comes down to seamless interconnections between the stages, so that the product effortlessly moves from one job to be done to the next. All personas feel like they have an ability to connect and collaborate with each other in the same space and don’t have to maintain separate integrations and data systems simply to do their job.

##### Benefits to GitLab by focusing on usability

McKinsey conducted a five-year study on the [business value of design](https://www.mckinsey.com/business-functions/mckinsey-design/our-insights/the-business-value-of-design) which showed top-quartile companies increased their revenues and total returns to shareholders substantially faster than their industry counterparts did over a five-year period. McKinsey found that top-quartile companies experienced 32 percentage points higher revenue growth and 56 percentage points higher TRS growth for the period as a whole.

#####  Appeal of usability to us as product builders (Product, UX, Eng, Infra)

Many users of GitLab start and end their working days with GitLab. They spend a lot of time with our tools to get their work done.  If we make the experience delightful, we have a meaningful opportunity to enhance their quality of life.
It's a win, win, win. Win for our customers, win for GitLab, and a sense of pride/win for us as builders.
 

### SaaS First

_Performance Indicator: Increase in [Paid GMAU for gitlab.com](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/#enablementinfrastructure---paid-gmau---number-of-unique-paid-users-that-perform-an-action-on-gitlabcom)_


**SaaS First does not mean SaaS only**. We are committed to our Self-managed as well as SaaS customers. Our software architecture has a single unified codebase that allows us to serve both SaaS and Self-managed deployment without the additional burden that other vendors may have that have chosen to create different software stacks for SaaS and Self-managed. 

This year, we place emphasis on strengthening our SaaS offering by focusing on ensuring feature parity with our market leading self-managed offering. We've made some updates to clarify that the product team should ensure feature parity for SaaS going forward.

#### Why is this important?

We have north of [700K SaaS MAU](https://about.gitlab.com/handbook/product/performance-indicators/#unique-monthly-active-users-umau). 

IDC projects that SaaS-based delivery of DevOps tools (26.4% CAGR) will overtake self-managed solutions (7.1% CAGR) by 2022.  This represents a large and urgent opportunity for GitLab, which has an estimated 9% usage-share of the cloud hosted Git repository market, based on [Bitrise repository counts](https://blog.bitrise.io/state-of-app-development-in-2019), compared to our 
 [67% usage-share of self-managed Git solutions](https://about.gitlab.com/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market) 

Additionally, IDC predicts that the market for SaaS-based DevOps tools (currently 38% of the market) will overtake the self-managed market (62%) by 2022 and account for $10.3B in revenue (or 58% of DevOps software tools market) by 2024. As a result, over this time period we expect to see an increase in existing self-managed users migrating to GitLab.com, as well as the majority of prospective customers preferring SaaS over self-managed deployment.

We have two-thirds of the usage-share of self-managed Git solutions. We will provide a compelling onboarding experience to ensure that we are able to serve our customers on SaaS just like we do on Self-managed. 


##### Our Execution Plan for SaaS first

SaaS First doesn’t mean SaaS only. When developing features and capabilities, we will provide them on SaaS as well as Self-managed. If faced with a choice to deliver it on one deployment method first - for example, Self-managed first, SaaS next or vice versa - we will choose SaaS First and Self-managed next.

We have also added a new [SaaS parity product principle](https://about.gitlab.com/handbook/product/product-principles/#parity-between-saas-and-self-managed-deployments) to help clarify this.

In addition we will focus on 

* [SaaS parity](https://about.gitlab.com/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/#all-differences-between-gitlabcom-and-self-managed)

* [Improving usability for admins](https://about.gitlab.com/direction/enablement/dotcom/#improved-usability-for-admins)

* [Availability and performance](https://about.gitlab.com/direction/enablement/dotcom/#availability-and-performance)

* [Improve our product intelligence capabilities](https://about.gitlab.com/direction/product-intelligence/)

* [Enterprise-grade security and compliance](https://about.gitlab.com/direction/enablement/dotcom/#enterprise-grade-security-and-compliance)

* [Simplify Projects and Groups](https://about.gitlab.com/direction/plan/#unify-projects-and-groups)


## Mitigating low-end disruption

GitLab is not immune to disruption. In some ways, it is a sign of success that GitLab is now at a scale where we have to think about low-end disruption. Arguably, a few years ago, GitLab was a low-end disruptor.

Clayton Christensen defines [low-end-disruption](https://online.hbs.edu/blog/post/4-keys-to-understanding-clayton-christensens-theory-of-disruptive-innovation) as follows:

_Low-end disruption refers to businesses that come in at the bottom of the market and serve customers in a way that is "good enough." These are generally the lower profit markets for the incumbent and thus, when these new businesses enter, the incumbents move further "upstream." In other words, they put their focus on where the greater profit margins are._

Our perspective is that low-end disruption is an additional and critical sensing mechanism. This is especially true for the DevOps market. We look at the following attributes to figure out if a low-end disruption has anything close to potential product-market resonance. This list is an adaptation of the [Product Zietgest Fit](https://a16z.com/2019/12/09/product-zeitgeist-fit/).

   - **Builder Enthusiasm**: Are the most talented, hardest working, or most-in-demand people - the engineers, the developers - so intrigued by the approach that they are working on it, excited by it, and trying to make it a thing?  If that is the case, then there is a good chance that they will eventually make it happen, moving beyond the fringes to the mainstream. Number of stars, forks, and contributions in the repo are some metrics to look for here.
   - **Despite Test**:  When people are using a product despite the fact that it’s not the best thing out there, or, in some cases, that it’s straight-up terrible, it’s a great sign. It shows that the product has a line into something emotional, not solely functional. Wanted, not just needed. In the early days, these products can often feel misunderstood or controversial. At first blush, the conceit may even raise a few eyebrows. But to the people who have been working on those products, they’re so clearly elegant, if temporarily imperfect, solutions to big and important problems that they seem almost obvious once they recognize it. Google Trends and posts on Hacker News are some things to monitor here.
   - **T-shirt test**:  If people with no connection to the company are wearing their t-shirts or putting their stickers on their laptops or wearing their socks, that  desire to associate with the idea indicates as much a movement as a product.

A reason low-end disruptors are able to enter the market is that the feature-absorption by users is lower than the feature-velocity of the established vendor. To address this we  are focused on a [working-by-default configuration principle](https://about.gitlab.com/handbook/product/product-principles/#configuration-principles). 

## Maturity

As we add new categories and stages to GitLab, some areas of the product will be deeper and more mature than others. We publish a
list of the categories, what we think their maturity levels are, and our plans to improve on our [maturity page](/direction/maturity/).

We strive to be the best product in the market and to be truly lovable. As the market, customer needs, competitive landscape, and technology change, we should expect our maturities to also change, including changing to a lower maturity rating. By embracing a [focus on improvement](/handbook/values/#focus-on-improvement) and [low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame), we encourage moving a maturity down. This is a strong indicator that we are realists about our product with an eye on achieving the best results for our customers. 

## Scope

We try to prevent maintaining functionality that is language or platform
specific, because they slow down our ability to get results. Examples of how we
handle it instead are:

1. We don't make native mobile clients. Instead, we make sure our mobile web pages are great.
1. We don't make native clients for desktop operating systems. People can use [Tower](https://www.git-tower.com/) and, for example, GitLab was the first to have merge conflict resolution in our web applications.
1. For language translations, we [rely on the wider community](https://docs.gitlab.com/ee/development/i18n/translation.html).
1. For Static Application Security Testing we rely on [open source security scanners](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks).
1. For code navigation, we're hesitant to introduce navigation improvements that only work for a subset of languages.
1. For [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality_diff.html), we reuse Code Climate Engines.
1. For building and testing with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/), we use Heroku Buildpacks.

Outside our scope are Kubernetes and everything it depends on:

1. **Network** (fabric) [Flannel](https://github.com/coreos/flannel/), Openflow, VMware NSX, Cisco ACI
1. **Proxy** (layer 7) [Envoy](https://envoyproxy.github.io/), [nginx](https://nginx.org/en/), [HAProxy](http://www.haproxy.org/), [traefik](https://traefik.io/)
1. **Ingress** [(north/south)](https://networkengineering.stackexchange.com/a/18877) [Contour](https://github.com/heptio/contour), [Ambassador](https://www.getambassador.io/),
1. **Service mesh** [(east/west)](https://networkengineering.stackexchange.com/a/18877) [Istio](https://istio.io/), [Linkerd](https://linkerd.io/)
1. **Container Scheduler** we mainly focus on Kubernetes, other container schedulers are: CloudFoundry, OpenStack, OpenShift, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Package manager** [Helm](https://github.com/kubernetes/helm), [ksonnet](http://ksonnet.heptio.com/)
1. **Operating System** Ubuntu, CentOS, [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [CoreOS](https://coreos.com/), [Alpine Linux](https://alpinelinux.org/about/)

During a presentation of Kubernetes, Brendan Burns talks about the four Ops layers
at [the 2:00 mark](https://youtu.be/WwBdNXt6wO4?t=120):

1. Application Ops
1. Cluster Ops
1. Kernel/OS Ops
1. Hardware Ops

GitLab helps you mainly with application ops. And where needed, we also allow you
to monitor clusters and link them to application environments. But we intend to
use vanilla Kubernetes, instead of something specific to GitLab.

Also outside our scope are products that are not specific to developing,
securing, or operating applications and digital products.

1. Identity management: Okta and Duo, you use this mainly with SaaS applications
   you don't develop, secure, or operate.
1. SaaS integration: Zapier and IFTTT
1. Ecommerce: Shopify

In scope are things that are not mainly for SaaS applications:

1. Network security, since it overlaps with application security to some extent.
1. Security information and event management (SIEM), since that measures
   applications and network.
1. Office productivity applications, since
   ["We believe that all digital products should be open to contributions, from legal documents to movie scripts and from websites to chip designs"](/company/strategy/#why)

We expect GitLab to continue to grow, and we have several ideas for [possible future stages](/handbook/product/categories/#possible-future-stages)

## Quarterly Objectives and Key Results (OKRs)

To make sure our goals are clearly defined and aligned throughout the
organization, we make use of OKRs (Objectives and Key Results). Our [quarterly Objectives and Key Results](/company/okrs/)
are publicly viewable.

## Your contributions

GitLab's direction is determined by GitLab the company and the code that is
sent by our [contributors](http://contributors.gitlab.com/). We continually
merge code to be released in the next version. Contributing is the best way to
get a feature you want included.

On [our issue tracker for CE][ce-issues] and [EE][ee-issues], many requests are
made for features and changes to GitLab. Issues with the [Accepting Merge
Requests] label are pre-approved as something we're willing to add to GitLab. Of
course, before any code is merged it still has to meet our [contribution
acceptance criteria].

[ce-issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
[ee-issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues
[Accepting merge requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues?state=opened&label_name=Accepting+merge+requests
[contribution acceptance criteria]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria

## How we plan releases

At GitLab, we strive to be [ambitious](/handbook/product/product-principles/#how-this-impacts-planning),
maintain a strong sense of urgency, and set aspirational targets with every
release. The direction items we highlight in our [kickoff](/handbook/product/product-processes/#kickoff-meetings)
are a reflection of this ambitious planning. When it comes to execution, we aim
for
[velocity over predictability](/handbook/engineering/#velocity-over-predictability).
This way, we optimize our planning time to focus on the top of the queue and
deliver things fast. We schedule 100% of what we can accomplish based on past [throughput](/handbook/engineering/management/throughput/)
and availability factors (vacation, contribute, etc.).

See our [product handbook on how we prioritize](/handbook/product/product-processes/#how-we-prioritize-work).

## Previous releases

On our [releases page](/releases/), you can find an overview of the most
important features of recent releases and links to the blog posts for each
release.

## Upcoming releases

GitLab releases a new version [every single month on the 22nd](/blog/2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab). You can find the
major planned features for upcoming releases on our [upcoming releases page](/upcoming-releases/) or see the [upcoming features for paid tiers](/direction/paid_tiers).

Note that we often move things around, do things that are not listed, and cancel
things that _are_ listed.

## Mobile strategy

Developing and delivering mobile apps with GitLab is a critical capability. Many technology companies are now managing a fleet of mobile applications, and being able to effectively build, package, test, and deploy this code in an efficient, effective way is a competitive advantage that cannot be understated. GitLab is taking improvements in this area seriously, with a unified vision across several of our DevOps stages.

### Mobile focus areas

There are several stages involved in delivering a comprehensive, quality mobile development experience at GitLab. These include, but are not necessarily limited to the following:

- Manage: Comprehensive templates to get started quickly
- Create: Web IDE features that allow you to easily manage the kinds of code and artifacts you work with during mobile development
- Verify: Runners for macOS, Linux-based builds for iOS
- Package: Build archives for mobile applications
- Release: Review Apps for mobile development, code signing, and publishing workflows to TestFlight or other distribution models
- Secure: Security scanning built directly into CI/CD pipelines, supporting a variety of mobile coding languages

### Mobile direction

We have more details about our plans on the [Mobile direction page](/direction/mobile/mobile-devops/). Some of our older thoughts are collected in the original
[epic]](https://gitlab.com/groups/gitlab-org/-/epics/769).

## ML/AI at GitLab

Right now, GitLab doesn't use any ML/AI technologies in production features, but we expect to use them [in the near future](/direction/learn/applied_ml/) for several types of problems. This is the focus of our new [Learn stage](/direction/learn/).

<%= partial("direction/singlegroup", :locals => { :label => "SingleEngineerGroups" }) %>

